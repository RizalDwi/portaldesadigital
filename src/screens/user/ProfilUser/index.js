import React, {useState, useEffect} from 'react'
import { View, Modal, StyleSheet, TouchableHighlight, Text, ToastAndroid } from 'react-native'
import Header from './../../../container/molecules/header/HeaderProfilUser'
import Konten from './../../../container/molecules/konten/KontenProfilUser'
import Asynstorage from '@react-native-community/async-storage'
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import LoadingScreen from './../../LoadingScreen'
import Axios from 'axios'

export default function index() {
    const [modalVisible, setModalVisible] = useState(false);
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [foto, setFoto] = useState('')
    const [loading, setLoading] = useState(false)

    const SelectImageFromGaleery = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true
          }).then(image => {
            setModalVisible(!modalVisible);
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    const selectImageFromCamera = () => {
        setModalVisible(!modalVisible);
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
          }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    async function updateFoto() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    foto: foto,
                    id_users: id_user
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/updateFotoUser', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setLoading(false)
                    Asynstorage.setItem('foto_user', foto)
                    showToast()
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Foto profil telah diubah", ToastAndroid.SHORT);
      };

    const getData = async() => {
        setName(await Asynstorage.getItem('username'))
        setEmail(await Asynstorage.getItem('email'))
        setFoto(await Asynstorage.getItem('foto_user'))
    }

    useEffect(() => {
        getData()
     }, [])

     if(loading){
         return <LoadingScreen />
     }

    return (
        <View style={{flex: 1}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centeredView}>
                <View style={styles.modalView}>

                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            selectImageFromCamera();
                        }}>
                            <Text style={styles.textStyle}>Ambil Dari Camera</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            SelectImageFromGaleery();
                        }}>
                            <Text style={styles.textStyle}>Ambil Dari Galeri</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            setModalVisible(!modalVisible);
                        }}
                        >
                        <Text style={styles.textStyle}>Batal</Text>
                    </TouchableHighlight>
                </View>
                </View>
            </Modal>
            <Header name={name} foto={foto} modal={setModalVisible} />
            <Konten name={name} email={email} updateFoto={updateFoto} />
        </View>
    )
}

const styles = StyleSheet.create({
    
      centeredView: {
        flex: 1,
        marginTop: 22
      },
      modalView: {
        width: '100%',
        height: 200,
        position: 'absolute',
        bottom: 0,
        backgroundColor: "white",
        padding: 35,
        alignItems: "center",
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        textAlign: "center",
        fontFamily: 'Poppins-Bold'
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
})
