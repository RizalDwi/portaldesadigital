import React, { useState } from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index( {navigation} ) {

    const [isi, setIsi] = useState('')

    const onPressAdd = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    isi: isi
                }

                console.log(data)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tambah_berita', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    if(res.data.succes){
                        alert("Tambah berita berhasil")

                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Home' }]
                        })
                    } else {
                        alert('Tambah Berita gagal')
                    }
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

    return (
        <View style={{flex: 1}}>
            <TextInput value={isi} onChangeText={(isi) => setIsi(isi)} placeholder="Isi Berita" />
            <TouchableOpacity onPress={() => onPressAdd()}>
                <View style={{width: '100%', height: 40, backgroundColor: 'pink'}}>
                    <Text>Add Berita</Text>
                </View>
                
            </TouchableOpacity>
        </View>
    )
}
