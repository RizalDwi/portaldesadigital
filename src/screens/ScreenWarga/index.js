import React from 'react'
import { View, Text } from 'react-native'

export default function index() {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20}}>
            <Text style={{fontFamily: 'Rubik-Regular', fontSize: 17, textAlign: 'center'}}>Anda bukan warga desa ini, silahkan melakukan pengajuan menjadi warga desa agar bisa menggunakan fitur ini</Text>
        </View>
    )
}
