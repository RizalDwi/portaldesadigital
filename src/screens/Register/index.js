import React from 'react'
import { View, Text } from 'react-native'
import HeaderRegister from './../../container/molecules/header/HeaderRegister'
import FormRegister from './../../container/molecules/FormRegister'

export default function index({navigation}) {
    return (
        <View style={{flex: 1 , alignItems: 'center', justifyContent: 'center', padding: 20}}>
            <HeaderRegister />
            <FormRegister navigation={navigation} />
        </View>
    )
}
