import React from 'react'
import { View, Text, Image } from 'react-native'

export default function index() {
    return (
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <Image source={require('./../../assets/gif/loading.gif')} style={{width: 100, height: 100}} />
        </View>
    )
}
