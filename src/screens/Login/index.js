import React from 'react'
import { View, Text } from 'react-native'
import FormLogin from './../../container/molecules/FormLogin'
import HeaderLogin from './../../container/molecules/header/HeaderLogin'

export default function index( {navigation} ) {
    return (
        <View style={{flex: 1 , alignItems: 'center', justifyContent: 'center', padding: 20}}>
            <HeaderLogin />
            <FormLogin navigation={navigation} />
        </View>
    )
}
