import React, {useEffect} from 'react'
import { View, Text, Image } from 'react-native'
import LoadingScreen from './../LoadingScreen'
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'

export default function index({navigation}) {

    async function Logout() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/logout', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    Asynstorage.clear()
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }]
                    })
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
            Logout()
      }, [])

    return (
        <LoadingScreen />
    )
}
