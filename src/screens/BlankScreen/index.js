import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'

export default function index({ navigation }) {

    useEffect(() => {
        Asynstorage.getItem('api_token', (error, result) =>{
            
            if (result){

                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }]
                })
                
            } else {
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }]
                })
            }
          })
    }, [])
    return (
        <View>
            <Text></Text>
        </View>
    )
}
