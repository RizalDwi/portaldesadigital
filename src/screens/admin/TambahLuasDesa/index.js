import React,{useState, createContext} from 'react'
import { View, ToastAndroid } from 'react-native'
import Form from './../../../container/molecules/form/FormTambahWilayah'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index({route}) {

    const [namaWilayah, setNamaWilayah] = useState('')
    const [luas, setLuas] = useState('')
    const [loading, setLoading] = useState(false)

    const handleChangeInputNamaWilayah = (value) => {
        setNamaWilayah(value)
    }

    const handleChangeInputLuas = (value) => {
        setLuas(value)
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    const addLuasDesa = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    nama_tempat: namaWilayah,
                    luas: luas
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_wilayah_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setNamaWilayah('')
                    setLuas('')
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            namaWilayah,
            luas,
            handleChangeInputNamaWilayah,
            handleChangeInputLuas,
            addLuasDesa
        }}>
            <View style={{flex: 1}}>
                <Form />
            </View>
        </RootContext.Provider>
    )
}
