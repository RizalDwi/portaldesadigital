import React, {useState, useEffect} from 'react'
import { StyleSheet, InteractionManager, View, ScrollView } from 'react-native'
import Header from './../../../container/molecules/header/HeaderPesanWarga'
import Konten from './../../../container/molecules/konten/admin/KontenPesanWarga'
import Chat from './../../../container/molecules/Chat'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({route}) {

    const [dataBalasan, setDataBalasan] = useState([])
    const [loading, setLoading] = useState(true)
    const [perihal, setPerihal] = useState('')
    const [judul, setJudul] = useState('')
    const [isi, setIsi] = useState('')
    const [nama, setNama] = useState('')
    const [namaDesa, setNamaDesa] = useState('')
    const [balasan, setBalasan] = useState('')
    const [email, setEmail] = useState('')

    const handleBalasan = (value) => {
        setBalasan(value)
    }

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_laporan_satuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataBalasan(res.data.balasan)
                    setPerihal(res.data.data[0].perihal)
                    setJudul(res.data.data[0].judul_laporan)
                    setIsi(res.data.data[0].isi_laporan)
                    setNama(res.data.data[0].name)
                    setNamaDesa(res.data.data[0].nama_desa)
                    setEmail(res.data.data[0].email)
                    console.log(res)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function getDataBalasan() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_balasan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataBalasan(res.data.data)
                    console.log(res)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function addBalasan() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_laporan: route.params.id,
                    isi_balasan: balasan
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_balasan_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Data berhasil ditambahkan")
                    console.log(data)
                    getDataBalasan()
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    console.log(data)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function sendMail() {
        let data = {
            service_id: 'service_portal',
            template_id: 'template_xsfexbv',
            user_id: 'user_WX30R5JyYbSAwupDLWj1g',
            accessToken: '527f75c9a0f7d675c79a45041fb87c53',
            template_params: {
                'from_name': namaDesa,
                'perihal' : perihal,
                'to_name' : nama,
                'pesan_warga' : isi,
                'tujuan_email' : email,
                'pesan_desa' : balasan
            }
        }

        Axios.post('https://api.emailjs.com/api/v1.0/email/send', data, {
            timeout: 20000
        })
        .then((res) => {
            alert('Your mail is sent!');
            addBalasan()
            console.log(res)
            
        })
        .catch((err) => {
            console.log("Error ->", err)
            alert('Oops... ' + JSON.stringify(err));
        })
    }

    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => {
          getData()
        });
      }, [])

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <Header perihal={perihal} judul={judul} nama={nama} />
                <Konten isi={isi} />
                <Chat sendMail={sendMail} namaDesa={namaDesa} handleBalasan={handleBalasan} dataBalasan={dataBalasan} />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({})
