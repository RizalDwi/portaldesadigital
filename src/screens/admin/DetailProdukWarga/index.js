import React, {useState, useEffect} from 'react'
import { View, Text, Image, ScrollView, Modal, StyleSheet, TouchableOpacity, ToastAndroid} from 'react-native'
import Header from './../../../container/molecules/header/HeaderAjuanProduk'
import Artikel from './../../../container/molecules/Artikel'
import Textarea from 'react-native-textarea';
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({route, navigation}) {

    const [loading, setLoading] = useState(true)
    const [data, setData] = useState([])
    const [modalVisible, setModalVisible] = useState(false);
    const [alasan, setAlasan] = useState('')

    async function getKonten() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/detail_ajuan_produk', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setData(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function produkDiterima() {
      var token = await Asynstorage.getItem('api_token')
      try {
          await Asynstorage.getItem('api_token', (error, result) =>{

            if (result){

              let data = {
                  id: route.params.id,
              }
              setLoading(true)
              Axios.post('https://shielded-eyrie-91877.herokuapp.com/produk_diterima', data, {
                  timeout: 20000,
                  headers: {
                      'Authorization' : 'Bearer ' + token
                  }
              })
              .then((res) => {
                  showToast()
                  navigation.goBack();
                  
              })
              .catch((err) => {
                  console.log("Error ->", err)
                  alert(err)
              })
            }
          })

      } catch(err) {
        console.log("Logout -> error : ",err)
      }
  }

  async function produkDitolak() {
    var token = await Asynstorage.getItem('api_token')
    try {
        await Asynstorage.getItem('api_token', (error, result) =>{

          if (result){

            let data = {
                id: route.params.id,
                alasan: alasan
            }
            setLoading(true)
            Axios.post('https://shielded-eyrie-91877.herokuapp.com/produk_ditolak', data, {
                timeout: 20000,
                headers: {
                    'Authorization' : 'Bearer ' + token
                }
            })
            .then((res) => {
                showToast()
                navigation.goBack();
                
            })
            .catch((err) => {
                console.log("Error ->", err)
                alert(err)
            })
          }
        })

    } catch(err) {
      console.log("Logout -> error : ",err)
    }
}

  const showToast = () => {
    ToastAndroid.show("Data telah disimpan", ToastAndroid.SHORT);
  };

    useEffect(() => {
        getKonten()
      }, [])

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{flex: 1, padding: 20}}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                        <View>
                            <Text style={styles.label}>Alasan Penolakan</Text>
                            <View style={styles.container}>
                                <Textarea
                                    containerStyle={styles.textareaContainer}
                                    style={styles.textarea}
                                    placeholder={'Isi Pesan'}
                                    placeholderTextColor={'#c7c7c7'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText = {(value) => setAlasan(value)}
                                />
                            </View>
                        </View>
                        <TouchableOpacity style={{width: '95%'}} onPress={() => produkDitolak()}>
                          <View style={{marginTop: 10,height: 40, backgroundColor: '#2286FF', justifyContent: 'center', alignItems: 'center'}}>
                              <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Kirim Pesan</Text>
                          </View>
                        </TouchableOpacity>
                        
                        <TouchableOpacity style={{width: '95%'}} onPress={() => {
                            setModalVisible(!modalVisible);
                        }}>
                            <View style={{marginTop: 10,height: 40, backgroundColor: '#BB2424', justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Batal</Text>
                            </View>
                        </TouchableOpacity>
                        
                        </View>
                    </View>
                </Modal>
            <ScrollView>
                <Header name={data[0].name} foto={data[0].foto} />
                <View style={{marginTop: 10}}>
                    <Image source={{uri: `data:image/png;base64,${data[0].gambar_produk}`}} style={{height: 220, width: '100%'}} />
                </View>
                <Artikel judul={data[0].judul_produk} isi={data[0].deskripsi} />
            
            </ScrollView>
            <View style={{marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity style={{width: '40%'}} onPress={() => produkDiterima()}>
                <View style={{height: 40, backgroundColor: '#26A718', justifyContent: 'center', alignItems: 'center'}}>
                      <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Setujui</Text>
                  </View>
              </TouchableOpacity>
                <TouchableOpacity style={{width: '40%'}} onPress={() => {
                        setModalVisible(true);
                    }}>
                    <View style={{height: 40, backgroundColor: '#BB2424', justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Tolak</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
  

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 22
  },
  modalView: {
    backgroundColor: 'white',
    height: 320,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  container: {
    width: 350,
    height: 170,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textareaContainer: {
    height: 150,
    width: '100%',
    padding: 5,
    backgroundColor: '#F7F9FC',
    borderWidth: 1,
    borderColor: '#A8C4E5'
  },
  textarea: {
    textAlignVertical: 'top',  // hack android
    height: 140,
    fontSize: 14,
    color: '#333',
  },
  label : {
    fontFamily: 'Poppins-Bold',
    color: '#394761',
    fontSize: 18
}
});
