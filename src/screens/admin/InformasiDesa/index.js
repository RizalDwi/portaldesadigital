import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid, Alert } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelInfomasiDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [dataInformasi, setDataInformasi] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)
    const [warna, setWarna] = useState('')

    async function getDataInformasi() {
      setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_informasi_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataInformasi(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataInformasi()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getDataInformasi()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const deleteInformasi = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_informasi_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataInformasi()
                    setLoading(false)
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    setLoading(false)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const ubahStatus = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_status_informasi_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataInformasi()
                    setLoading(false)
                    showToast("Data berhasil diubah")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus Informasi Desa",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteInformasi(id) }
        ],
        { cancelable: false }
        );
    
        if(loading){
            return <LoadingScreen />
        }

    return (
        <View style={{flex: 1, padding: 20}}>
            
            <TouchableOpacity onPress={() => navigation.navigate('FormTambahInformasi', {idDesa: route.params.idDesa})}>
                <View style={[styles.button,{ backgroundColor: `#${warna}`}]}>
                    <Text style={styles.textButton}>Tambah Informasi</Text>
                </View>
            </TouchableOpacity>
            <Tabel warna={warna} navigation={navigation} dataInformasi={dataInformasi} ubahStatus={ubahStatus} alertDelete={alertDelete} />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    label: {fontFamily: 'Poppins-Bold', fontSize: 18, color: 'black'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    button: {backgroundColor: '#35BEE0', width: '45%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
})
