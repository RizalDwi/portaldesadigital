import React,{useState, createContext} from 'react'
import { View, Text, ToastAndroid } from 'react-native'
import Form from './../../../container/molecules/form/FormTambahMisi'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {

    const [misi, setMisi] = useState('')
    const [loading, setLoading] = useState(false)

    const handleInputMisi = (value) => {
        setMisi(value)
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    const addMisi = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    misi: misi,
                    id_visi: route.params.idVisi
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_misi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setMisi('')
                    setLoading(false)
                    showToast()
                    console.log(res)
                    console.log("Data 2 -> ", data)
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            misi,
            handleInputMisi,
            addMisi
        }}>
            <View style={{flex: 1, padding: 20}}>
                <Form />
            </View>
        </RootContext.Provider>
    )
}
