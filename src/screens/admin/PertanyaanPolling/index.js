import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid, Alert, InteractionManager } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelPernyataanPolling'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {
    const [dataPolling, setDataPolling] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)
    const [max, setMax] = useState(4)
    const [jumlah, setJumlah] = useState(0)
    const [warna, setWarna] = useState('')

    async function getDataPolling() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_polling: route.params.idPolling
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_pilihan_polling', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataPolling(res.data.data)
                    setJumlah(res.data.jumlah)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getDataPolling()
        });
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            InteractionManager.runAfterInteractions(() => {
                getDataPolling()
            });
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const deletePolling = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_pilihan_polling', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataPolling()
                    setLoading(false)
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    setLoading(false)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const ubahStatus = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_pilihan_polling_status', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataPolling()
                    setLoading(false)
                    showToast("Data berhasil diubah")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus Pilihan Polling",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deletePolling(id) }
        ],
        { cancelable: false }
        );
    
        if(loading){
            return <LoadingScreen />
        }
    return (
        
        <View style={{flex: 1, padding: 20}}>
            <View style={{marginBottom: 15}}>
                <Text style={styles.label}>Pertanyaan : {route.params.pertanyaan}</Text>
            </View>
            <View style={{marginBottom: 15}}>
                <Text style={styles.labelMax}>Max : {max - jumlah} </Text>
            </View>
            {(() => {
                if (jumlah < 4){
                    return (
                        <TouchableOpacity onPress={() => navigation.navigate('FormPilihanPolling', {idPolling: route.params.idPolling})}>
                            <View style={styles.button}>
                                <Text style={styles.textButton}>Tambah Pilihan Polling</Text>
                            </View>
                        </TouchableOpacity>
                    )
                }
            })()}
            <Tabel warna={warna} navigation={navigation} dataPolling={dataPolling} alertDelete={alertDelete} ubahStatus={ubahStatus} />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    label: {fontFamily: 'Poppins-Bold', fontSize: 18, color: 'black'},
    labelMax: {fontFamily: 'Poppins-Bold', fontSize: 13, color: 'black'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    button: {backgroundColor: '#35BEE0', width: '55%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
})
