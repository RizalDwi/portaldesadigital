import React, {useState, createContext, useEffect} from 'react'
import { View, Text, ScrollView, ToastAndroid } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenEditAparat'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../screens/LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {

    const [nama, setNama] = useState('')
    const [nik, setNik] = useState('')
    const [niap, setNiap] = useState('')
    const [nip, setNip] = useState('')
    const [tempatLahir, setTempatLahir] = useState('')
    const [tanggalLahir, setTanggalLahir] = useState(new Date())
    const [jenisKelamin, setJenisKelamin] = useState('')
    const [pendidikan, setPendidikan] = useState('')
    const [agama, setAgama] = useState('')
    const [pangkat, setPangkat] = useState('')
    const [nomorPengangkatan, setNomorPengangkatan] = useState('')
    const [tanggalPengangkatan, setTanggalPengangkatan] = useState(new Date())
    const [nomorPemberhentian, setNomorPemberhentian] = useState('')
    const [tanggalPemberhentian, setTanggalPemberhentian] = useState(new Date())
    const [masaJabatan, setMasaJabatan] = useState('')
    const [jabatan, setJabatan] = useState('')
    const [status, setStatus] = useState('')
    const [tupoksi, setTupoksi] = useState('')
    const [photo, setPhoto] = useState('')
    const [loading, setLoading] = useState(true)

    const handleInputNama = (value) => {
        setNama(value)
    }
    const handleInputNik = (value) => {
        setNik(value)
    }
    const handleInputNiap = (value) => {
        setNiap(value)
    }
    const handleInputTempatLahir = (value) => {
        setTempatLahir(value)
    }
    const handleInputTanggalLahir = (value) => {
        setTanggalLahir(value)
    }
    const handleInputJenisKelamin = (value) => {
        setJenisKelamin(value)
    }
    const handleInputPendidikan = (value) => {
        setPendidikan(value)
    }
    const handleInputAgama = (value) => {
        setAgama(value)
    }
    const handleInputPangkat = (value) => {
        setPangkat(value)
    }
    const handleInputNomorPengangkatan = (value) => {
        setNomorPengangkatan(value)
    }
    const handleInputTanggalPengangkatan = (value) => {
        setTanggalPengangkatan(value)
    }
    const handleInputNomorPemberhentian = (value) => {
        setNomorPemberhentian(value)
    }
    const handleInputTanggalPemberhentian = (value) => {
        setTanggalPemberhentian(value)
    }
    const handleInputMasaJabatan = (value) => {
        setMasaJabatan(value)
    }
    const handleInputJabatan = (value) => {
        setJabatan(value)
    }
    const handleInputStatus = (value) => {
        setStatus(value)
    }
    const handleInputTupoksi = (value) => {
        setTupoksi(value)
    }
    const handleInputPhoto = (value) => {
        setPhoto(value)
    }
    const handleInputNip = (value) => {
        setNip(value)
    }
    
    useEffect(() => {
        async function getDataAparat() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.idAparat
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_aparat', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        setNama(res.data.data[0].nama)
                        setNik(res.data.data[0].nik)
                        setNiap(res.data.data[0].niap)
                        setNip(res.data.data[0].nip)
                        setTempatLahir(res.data.data[0].tempat_lahir)
                        setTanggalLahir(new Date(res.data.data[0].tanggal_lahir))
                        setJenisKelamin(res.data.data[0].jenis_kelamin)
                        setPendidikan(res.data.data[0].pendidikan)
                        setAgama(res.data.data[0].agama)
                        setPangkat(res.data.data[0].pangkat_atau_golongan)
                        setNomorPengangkatan(res.data.data[0].nomor_sk_pengangkatan)
                        setTanggalPengangkatan(new Date(res.data.data[0].tanggal_sk_pengangkatan))
                        setNomorPemberhentian(res.data.data[0].nomor_sk_pemberhentian)
                        setTanggalPemberhentian(new Date(res.data.data[0].tanggal_sk_pemberhentian))
                        setMasaJabatan(res.data.data[0].masa_jabatan)
                        setJabatan(res.data.data[0].jabatan)
                        setStatus(res.data.data[0].status)
                        setTupoksi(res.data.data[0].tupoksi)
                        setPhoto(res.data.data[0].foto)
                        setLoading(false)
                        console.log("Id Apatat->",route.params.idAparat)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataAparat()
      
      }, [])

      const showToast = () => {
        ToastAndroid.show("Data berhasi diperbaharui", ToastAndroid.SHORT);
      };

    const editAparat = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data2 = {
                    id: route.params.idAparat,
                    nama: nama,
                    nik: nik,
                    nip: nip,
                    niap: niap,
                    tempat_lahir: tempatLahir,
                    tanggal_lahir: tanggalLahir,
                    jenis_kelamin: jenisKelamin,
                    pendidikan: pendidikan,
                    agama: agama,
                    pangkat_atau_golongan: pangkat,
                    nomor_sk_pengangkatan: nomorPengangkatan,
                    tanggal_sk_pengangkatan: tanggalPengangkatan,
                    nomor_sk_pemberhentian: nomorPemberhentian,
                    tanggal_sk_pemberhentian: tanggalPemberhentian,
                    masa_jabatan: masaJabatan,
                    jabatan: jabatan,
                    status: status,
                    tupoksi: tupoksi,
                    foto: photo
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_aparat', data2, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    
                    setLoading(false)
                    showToast()
                    console.log(res)
                    console.log("Data 2 -> ", data2)
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data2)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            nama,
            nik,
            niap,
            tempatLahir,
            tanggalLahir,
            jenisKelamin,
            pendidikan,
            agama,
            pangkat,
            nomorPengangkatan,
            tanggalPengangkatan,
            nomorPemberhentian,
            tanggalPemberhentian,
            masaJabatan,
            jabatan,
            status,
            tupoksi,
            photo,
            nip,
            handleInputNama,
            handleInputNik,
            handleInputNiap,
            handleInputTempatLahir,
            handleInputTanggalLahir,
            handleInputJenisKelamin,
            handleInputPendidikan,
            handleInputAgama,
            handleInputPangkat,
            handleInputNomorPengangkatan,
            handleInputTanggalPengangkatan,
            handleInputNomorPemberhentian,
            handleInputTanggalPemberhentian,
            handleInputMasaJabatan,
            handleInputJabatan,
            handleInputStatus,
            handleInputTupoksi,
            handleInputPhoto,
            handleInputNip,
            editAparat
        }}>
            <View style={{flex: 1, padding: 20}}>
                <ScrollView>
                    <Konten />
                </ScrollView>
            </View>
        </RootContext.Provider>
    )
}
