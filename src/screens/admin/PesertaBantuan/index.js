import React, {useEffect, useState} from 'react'
import { View, Text, Alert, ToastAndroid } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenPesertaBantuan'
import Tabel from './../../../container/molecules/Tabel/TabelPesertaBantuan'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index( {navigation, route} ) {

    const [count, setCount] = useState(0);
    const [listPeserta, setListPeserta] = useState([])
    const [loading, setLoading] = useState(true)

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_program: route.params.id,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_peserta_bantuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                   
                    setListPeserta(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const deletePeserta = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_warga: id,
                    id_program: route.params.id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/delete_peserta_bantuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getData()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus peserta",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deletePeserta(id) }
        ],
        { cancelable: false }
        );

        const showToast = (value) => {
            ToastAndroid.show(value, ToastAndroid.SHORT);
          };

    useEffect(() => {
        getData()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getData()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <Konten navigation={navigation} id={route.params.id} idDesa={route.params.idDesa} warna={route.params.warna} />
            <Tabel navigation={navigation} listPeserta={listPeserta} alertDelete={alertDelete} warna={route.params.warna} />
        </View>
    )
}
