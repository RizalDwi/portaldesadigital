import React, {useEffect, useState} from 'react'
import { View, Text, processColor, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { BarChart } from 'react-native-charts-wrapper'
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'
import LoadingScreen from './../../../screens/LoadingScreen'

export default function index({route, navigation}) {

  const [dataPolling, setDataPolling] = useState([])
    
  useEffect(() => {
    async function getDataChart() {
      var token = await Asynstorage.getItem('api_token')
      try {
          await Asynstorage.getItem('api_token', (error, result) =>{

            if (result){

              let data = {
                  id_polling: route.params.idPolling,
              }

              Axios.post('https://shielded-eyrie-91877.herokuapp.com/chart_polling', data, {
                  timeout: 20000,
                  headers: {
                      'Authorization' : 'Bearer ' + token
                  }
              })
              .then((res) => {
                  setDataPolling(res.data.data)
                  console.log("Polling ->", res)
                  res.data.data.forEach(setIsiLabel)
                  setIsLoading(false)
                  
              })
              .catch((err) => {
                setIsLoading(false)
                alert('Belum ada pertanyaan')
              })
            }
          })

      } catch(err) {
        console.log("Logout -> error : ",err)
      }
  }

    getDataChart()
  }, [])

  const [tanggal, setTanggal] = useState('')
  const [isLoading, setIsLoading ] = useState(true)
  let label = []

  const setIsiLabel = (item, index) => {
      label.push(item.pilihan)
  }


  const [legend, setLegend] = useState({
    enabled: false,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5
})

  // const [xAxis, setXAxis] = useState({
  //     valueFormatter: label,
  //     textColor: processColor('#394761'),
  //     position: 'BOTTOM',
  //     drawAxisLine: true,
  //     drawGridLines: false,
  //     axisMinimum: -0.5,
  //     granularityEnabled: true,
  //     granularity: 1,
  //     color: '#394761',
  //     spaceBetweenLabels: 0,
  //     labelRotationAngle: -45.0,
  //     limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
  // })
  const [xAxis, setXAxis] = useState({
    valueFormatter: label,
    granularityEnabled: true,
    granularity : 1,
})
  const [yAxis, setYAxis] = useState({
      left: {
          axisMinimum: 0,
          labelCountForce: true,
          granularity: 5,
          granularityEnabled: true,
          drawGridLines: false,
          textColor: processColor('#394761')
      },
      right: {
          textColor: processColor('#394761'),
          axisMinimum: 0,
          labelCountForce: true,
          granularity: 5,
          granularityEnabled: true,
          enabled: false
      }
  })
  // DetailHasilPolling
    if(isLoading) {
        return <LoadingScreen />
    } else {
      return (
        <View style={{flex: 1}}>
          <View style={{width: '100%', paddingHorizontal: 30, paddingTop: 30}}>
            <Text style={{color: '#394761', fontSize: 20, fontWeight: 'bold', fontFamily: 'muli', marginBottom: 10}}>{route.params.pertanyaan}</Text>
          </View>
          <View style={{paddingLeft: 30}}>
          <TouchableOpacity onPress={() => navigation.navigate('DetailHasilPolling', {idPolling: route.params.idPolling})}>
              <View style={styles.button}>
                  <Text style={styles.textButton}>Detail Jawaban</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, padding: 30}}>
            <BarChart
                style={{ flex: 1}}
                data={{dataSets: [{
                  values: dataPolling,
                  label: 'Data Poling',
                  config: {
                    colors: [processColor("#FCCD00"), processColor("#FFA700"),processColor('#7a0012'),processColor('#25F8BD')],
                    drawFilled: false,
                    drawValues: false,
                    
                }
                }],
                  config: {
                    barWidth: 0.9,
                  }
                }}
                
                xAxis={xAxis}
                legend={legend}
                marker={{
                  enabled: true,
                  markerColor: processColor('#FFFFFF'),
                  textColor: processColor('#000000'),
                  textSize: 14
                }}
                animation={{durationX: 2000}}
                gridBackgroundColor={processColor('#ffffff')}
                visibleRange={{x: { min: 4, max: 4 }}}
                drawBarShadow={false}
                drawValueAboveBar={true}
                drawHighlightArrow={true}
              />
              {/* <BarChart
                style={{ flex: 1}}
                data= {{
                  dataSets: [{
                    values: dataPolling,
                    label: 'Jumlah Pengunjung',
                    config: {
                      color: [processColor("#FCCD00"), processColor("#FFA700"),processColor('#7a0012'),processColor('#25F8BD')],
                      drawFilled: false,
                      drawValues: false,
                    }
                  }],
                  config: {
                    barWidth: 0.7,
                  }
                }}
                xAxis={xAxis}
                legend={legend}
                marker={{
                  enabled: true,
                  markerColor: processColor('#FFFFFF'),
                  textColor: processColor('#000000'),
                  textSize: 14
                }}
              animation={{durationX: 2000}}
              gridBackgroundColor={processColor('#ffffff')}
              visibleRange={{x: { min: 4, max: 4 }}}
              drawBarShadow={false}
              drawValueAboveBar={true}
              drawHighlightArrow={true}
              /> */}
          </View>
        </View>
          
      )
    }

  }

  const styles = StyleSheet.create({
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    button: {backgroundColor: '#35BEE0', width: '40%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
})
