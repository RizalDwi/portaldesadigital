import React, {useEffect} from 'react'
import { View, InteractionManager } from 'react-native'
import { useState } from 'react/cjs/react.development'
import HeaderDesa from '../../../container/molecules/header/HeaderKelolaDesa'
import MenuDesa from '../../../container/molecules/konten/admin/KontenMenuKelolaDesa'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation}) {

    const [idDesa, setIdDesa] = useState(0)
    const [loading, setLoading] = useState(true)
    const [warna, setWarna] = useState('')
    const [foto, setFoto] = useState('')
    const [namaDesa, setNamaDesa] = useState('')

    async function getDataDesa() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setLoading(false)
                    setIdDesa(res.data.data[0].id)
                    setNamaDesa(res.data.data[0].nama_desa)
                    setFoto(res.data.data[0].foto)
                    Asynstorage.setItem('alamat_desa_admin', 'Kab. '+res.data.data[0].kabupaten+", "+res.data.data[0].provinsi)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

      useEffect(() => {
        getDataDesa()
        
      }, [])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1}}>
            <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
                <HeaderDesa warna={warna} namaDesa={namaDesa} foto={foto} />
                <MenuDesa navigation={navigation} id_desa={idDesa} warna={warna} />
            </View>
        </View>
    )
}
