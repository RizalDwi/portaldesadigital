import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, TextInput } from 'react-native'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation, route}) {

    const [dataWarga, setDataWarga] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_warga_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataWarga(res.data.data)
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function TambahAdmin(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_user: id
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tambah_admin', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataWarga(res.data.data)
                    getData()
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }


      useEffect(() => {
        // getData()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getData()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      if(loading){
          return <LoadingScreen />
      }

    const renderItem = ({item, index}) => {
        return (
            <View style={{borderColor: '#DFDFDF', borderBottomWidth: 1, marginVertical: 5}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                        <Image source={require('./../../../assets/images/rijal.jpg')} style={{height: 70, width: 70, borderRadius: 70/2}} />
                        <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.nama_lengkap}</Text>
                                <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>{item.nik}</Text>
                                <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.alamat}</Text>
                        </View>
                        <View style={{flex:1, alignItems: 'flex-end'}}>
                            <TouchableOpacity onPress={() => TambahAdmin(item.id_users)}>
                                <View style={styles.button}>
                                    <Text style={styles.textButoon}>Jadikan Admin</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
            </View>
        )
    }

    return (
        <View style={{flex:1, padding: 20}}>
          
            <FlatList data={dataWarga} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </View>
    )
}

const styles = StyleSheet.create({
    textButoon: {fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 12},
    button: {backgroundColor: '#4CC9F0', width: 120, height: 40, marginTop: 10, justifyContent: 'center', alignItems: 'center'}
})


