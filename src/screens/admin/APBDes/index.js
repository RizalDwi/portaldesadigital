import React,{useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid, Alert, InteractionManager } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelAPBDes'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [dataKegiatan, setDataKegiatan] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)
    const [warna, setWarna] = useState('')

    async function getDataKegiatan() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataKegiatan(res.data.data)
                    setLoading(false)
                    console.log(data)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    setLoading(false)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getDataKegiatan()
          });
      }, [])

      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const deleteKegiatan = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataKegiatan()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    setLoading(false)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }


      const alertDelete = (id) =>
        Alert.alert(
        "Hapus Kegiatan ini?",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteKegiatan(id) }
        ],
        { cancelable: false }
        );
    
        if(loading){
            return <LoadingScreen />
        }

    return (
        <View style={{flex: 1, padding: 20}}>
            
            <View style={{width: '100%'}}>
                
                    <View style={{backgroundColor: `#${warna}`,width: '45%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}}>
                        
                            <TouchableOpacity onPress={() => navigation.navigate('FormTambahAnggaran', {idDesa: route.params.idDesa})} style={{width: '90%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}}>
                                <Text style={styles.textButton}>Tambah Anggaran</Text>
                            </TouchableOpacity>
                        
                    </View>
            </View>
            <Tabel warna={warna} navigation={navigation} dataKegiatan={dataKegiatan} alertDelete={alertDelete} idDesa={route.params.idDesa} />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'}
})
