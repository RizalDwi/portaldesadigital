import React,{useState, createContext, useEffect} from 'react'
import { View, Text } from 'react-native'
import Form from './../../../container/molecules/form/FormEditMisi'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {

    const [misi, setMisi] = useState('')
    const [loading, setLoading] = useState(true)

    const handleInputMisi = (value) => {
        setMisi(value)
    }

    const updateMisi = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    misi: misi,
                    id: route.params.idMisi
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_misi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    
                    setLoading(false)
                    // showToast()
                    console.log(res)
                    console.log("Data 2 -> ", data)
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        async function getDataMisi() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.idMisi
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_misi', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        setMisi(res.data.data[0].misi)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataMisi()
      
      }, [])

    

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            misi,
            handleInputMisi,
            updateMisi
        }}>
            <View style={{flex: 1, padding: 20}}>
                <Form />
            </View>
        </RootContext.Provider>
    )
}
