import React, {useEffect, useState} from 'react'
import { View, Text, processColor, Image, StyleSheet, ScrollView } from 'react-native'
import { BarChart } from 'react-native-charts-wrapper'
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'
import LoadingScreen from '../../LoadingScreen'

export default function index({route, navigation}) {

  const [dataPolling, setDataPolling] = useState([])
    
  useEffect(() => {
    async function getDataChart() {
      var token = await Asynstorage.getItem('api_token')
      try {
          await Asynstorage.getItem('api_token', (error, result) =>{

            if (result){

              let data = {
                  id_desa: route.params.idDesa,
              }

              Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_pengunjung', data, {
                  timeout: 20000,
                  headers: {
                      'Authorization' : 'Bearer ' + token
                  }
              })
              .then((res) => {
                  setDataPolling(res.data.data)
                  res.data.data.forEach(setIsiLabel)
                  setIsLoading(false)
                  
              })
              .catch((err) => {
                setIsLoading(false)
              })
            }
          })

      } catch(err) {
        console.log("Logout -> error : ",err)
      }
  }

    getDataChart()
  }, [])

  const [tanggal, setTanggal] = useState('')
  const [isLoading, setIsLoading ] = useState(true)
  let label = []

  const setIsiLabel = (item, index) => {
    var month = new Array();
      month[0] = "Jan";
      month[1] = "Feb";
      month[2] = "Mar";
      month[3] = "Apr";
      month[4] = "May";
      month[5] = "Jun";
      month[6] = "Jul";
      month[7] = "Aug";
      month[8] = "Sep";
      month[9] = "Oct";
      month[10] = "Nov";
      month[11] = "Dec";

    var mydate = new Date(item.tanggal);
    var str =mydate.getDate() +" "+ month[mydate.getMonth()-1] + " " + mydate.getFullYear();
    label.push(str)
}

  const [legend, setLegend] = useState({
      enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
})

  const [xAxis, setXAxis] = useState({
      valueFormatter: label,
      granularityEnabled: true,
      granularity : 1,
  })
  const [yAxis, setYAxis] = useState({
      left: {
          axisMinimum: 0,
          labelCountForce: true,
          granularity: 5,
          granularityEnabled: true,
          drawGridLines: false,
          textColor: processColor('#394761')
      },
      right: {
          textColor: processColor('#394761'),
          axisMinimum: 0,
          labelCountForce: true,
          granularity: 5,
          granularityEnabled: true,
          enabled: false
      }
  })
  // DetailHasilPolling
    if(isLoading) {
        return <LoadingScreen />
    } else {
      return (
        <View style={{flex: 1}}>
          <View style={{width: '100%', paddingHorizontal: 30, paddingTop: 30}}>
            <Text style={{color: '#394761', fontSize: 20, fontWeight: 'bold', fontFamily: 'muli', marginBottom: 10}}>{route.params.pertanyaan}</Text>
          </View>
          <View style={{ flex: 1, padding: 30}}>
            <BarChart
                style={{ flex: 1}}
                data= {{
                  dataSets: [{
                    values: dataPolling,
                    label: 'Jumlah Pengunjung',
                    config: {
                      color: processColor('teal'),
                      barShadowColor: processColor('lightgrey'),
                      highlightAlpha: 90,
                      highlightColor: processColor('red'),
                    }
                  }],
                  config: {
                    barWidth: 0.7,
                  }
                }}
                xAxis={xAxis}
                legend={legend}
                marker={{
                  enabled: true,
                  markerColor: processColor('#FFFFFF'),
                  textColor: processColor('#000000'),
                  textSize: 14
                }}
              animation={{durationX: 2000}}
              gridBackgroundColor={processColor('#ffffff')}
              visibleRange={{x: { min: 4, max: 4 }}}
              drawBarShadow={false}
              drawValueAboveBar={true}
              drawHighlightArrow={true}
              />
            
          </View>
        </View>
          
      )
    }

  }

  const styles = StyleSheet.create({
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    button: {backgroundColor: '#35BEE0', width: '40%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
})
