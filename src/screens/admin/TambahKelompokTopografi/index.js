import React, {useState, createContext} from 'react'
import { View, ToastAndroid } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import Form from './../../../container/molecules/form/FormTambahKelompokTopografi'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index({route}) {

    const [namaKelompok, setNamaKelompok] = useState('')
    const [jumlah, setJumlah] = useState('')
    const [loading, setLoading] = useState(false)

    const handleChangeInputNamaKelompok = (value) => {
        setNamaKelompok(value)
    }

    const handleChangeInputJumlah = (value) => {
        setJumlah(value)
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    const addKelompokTopografi = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_kategori: route.params.itemId,
                    nama_kelompok: namaKelompok,
                    jumlah: jumlah
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_kelompok_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setNamaKelompok('')
                    setJumlah('')
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            namaKelompok,
            jumlah,
            addKelompokTopografi,
            handleChangeInputJumlah,
            handleChangeInputNamaKelompok
        }}>
            <View style={{flex: 1}}>
                <Form />
            </View>
        </RootContext.Provider>
    )
}
