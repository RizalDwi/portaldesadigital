import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert, ToastAndroid, InteractionManager } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelSubkategoriAPBDes'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [dataSubkategori, setDataSubkategori] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)

    async function getDataSubkategori() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_kategori: route.params.idKategori
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/subkategori_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataSubkategori(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    useEffect(() => {
        
      InteractionManager.runAfterInteractions(() => {
        getDataSubkategori()
      });
    }, [])
      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const deleteKategori = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_subkategori_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataSubkategori()
                    setLoading(false)
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const ubahStatus = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_subkategori_apbdes_status', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataSubkategori()
                    setLoading(false)
                    showToast("Data berhasil diubah")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus Subkategori APBDes",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteKategori(id) }
        ],
        { cancelable: false }
        );

    if(loading){
        return <LoadingScreen />
    }
    return (
        
            <View style={{flex: 1, padding: 20}}>
                <View style={{marginBottom: 15}}>
                    <Text style={styles.label}>Kategori: {route.params.namaKategori}</Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('TambahSubkategori', {idKategori: route.params.idKategori})}>
                    <View style={[styles.button,{ backgroundColor: `#${route.params.warna}`}]}>
                        <Text style={styles.textButton}>Tambah SubKategori Baru</Text>
                    </View>
                </TouchableOpacity>
                <Tabel warna={route.params.warna} navigation={navigation} dataSubkategori={dataSubkategori} alertDelete={alertDelete} ubahStatus={ubahStatus} />
            </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    label: {fontFamily: 'Poppins-Bold', fontSize: 18, color: 'black'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    button: {width: '55%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
})
