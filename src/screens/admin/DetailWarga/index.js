import React, {useEffect} from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import { useState } from 'react/cjs/react.development'
import Header from './../../../container/molecules/header/HeaderWarga'
import Konten from './../../../container/molecules/konten/admin/KontenDetailWarga'
import LoadingScreen from './../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {

    const [nama, setNama] = useState('')
    const [nik, setNik] = useState('')
    const [kk, setKK] = useState('')
    const [tanggal, setTanggal] = useState('')
    const [photo, setPhoto] = useState('')
    const [alamat, setAlamat] = useState('')
    const [telp, setTelp] = useState('')
    const [loading, setLoading] = useState(true)

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/detail_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Warga->",res)
                    setNama(res.data.data[0].nama_lengkap)
                    setNik(res.data.data[0].nik)
                    setKK(res.data.data[0].no_kk)
                    setTanggal(res.data.data[0].tanggal_lahir)
                    setPhoto(res.data.data[0].foto_pengajuan)
                    setAlamat(res.data.data[0].alamat)
                    setTelp(res.data.data[0].no_telp)
                    setLoading(false)
                    
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    if(loading){
        return <LoadingScreen/>
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <Header nama={nama} />
                <Konten nik={nik} kk={kk} tanggal={tanggal} photo={photo} alamat={alamat} telp={telp}/>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({})
