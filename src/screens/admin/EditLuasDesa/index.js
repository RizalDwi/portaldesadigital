import React,{useState, createContext, useEffect} from 'react'
import { View, ToastAndroid } from 'react-native'
import Form from './../../../container/molecules/form/FormEditWilayahDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index({route}) {

    const [namaWilayah, setNamaWilayah] = useState('')
    const [luas, setLuas] = useState('')
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function getDataKondisiDesa() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.itemId
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_luas_desa', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        setNamaWilayah(res.data.data[0].nama_tempat)
                        setLuas(res.data.data[0].luas)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataKondisiDesa()
      
      }, [])

    const handleChangeInputNamaWilayah = (value) => {
        setNamaWilayah(value)
    }

    const handleChangeInputLuas = (value) => {
        setLuas(value)
    }

    const updateLuasDesa = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.itemId,
                    nama_tempat: namaWilayah,
                    luas: luas
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_wilayah_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil diperbaharui", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen />
    } else {
        return (
            <RootContext.Provider value={{
                namaWilayah,
                luas,
                handleChangeInputNamaWilayah,
                handleChangeInputLuas,
                updateLuasDesa
            }}>
                <View style={{flex: 1}}>
                    <Form />
                </View>
            </RootContext.Provider>
        )
    }
}
