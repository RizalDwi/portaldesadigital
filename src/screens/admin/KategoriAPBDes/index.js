import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert, ToastAndroid, InteractionManager } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelKategoriAPBDes'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [dataKategori, setDataKategori] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)
    const [warna, setWarna] = useState('')

    async function getDataKategori() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/kategori_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataKategori(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => {
          getDataKategori()
        });
      }, [])

      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const deleteKategori = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_kategori_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataKategori()
                    setLoading(false)
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    setLoading(false)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const ubahStatus = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_kategori_apbdes_status', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataKategori()
                    setLoading(false)
                    showToast("Data berhasil diubah")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus Kategori APBDes",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteKategori(id) }
        ],
        { cancelable: false }
        );
    
        if(loading){
            return <LoadingScreen />
        }

    return (
            <View style={{flex: 1, padding: 20}}>
                <TouchableOpacity onPress={() => navigation.navigate('TambahKategori', {idDesa: route.params.idDesa})}>
                    <View style={[styles.button,{ backgroundColor: `#${warna}`}]}>
                        <Text style={styles.textButton}>Tambah Kategori Baru</Text>
                    </View>
                </TouchableOpacity>
                <Tabel warna={warna} navigation={navigation} dataKategori={dataKategori} alertDelete={alertDelete} ubahStatus={ubahStatus} />
            </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    button: {width: '50%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
})
