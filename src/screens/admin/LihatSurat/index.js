import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, PermissionsAndroid, ToastAndroid, InteractionManager } from 'react-native'
import Pdf from 'react-native-pdf';
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'
import RNFetchBlob from 'rn-fetch-blob'

export default function index({route}) {
    
    const [loading, setLoading] = useState(true)
    const [file, setFile] = useState('')
    const [nomorSurat, setNomorSurat] = useState('')
    
    const downloadFromBase64 = async() => {
        const fs = RNFetchBlob.fs;
        let fPath = Platform.select({
            ios: fs.dirs.DocumentDir,
            android: fs.dirs.DownloadDir,
         });
         
         fPath = `${fPath}/${nomorSurat}.pdf`;
         try {
                   const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
                   if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    await fs.writeFile(fPath, file, "base64");
                    showToast()
                   } else {
                     Alert.alert('Permission Denied!', 'You need to give storage permission to download the file');
                   }
                 } catch (err) {
                   console.warn(err);
                 } 

    }
    
    const showToast = () => {
        ToastAndroid.show("Surat telah disimpan", ToastAndroid.SHORT);
      };

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/view_surat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                   
                    setFile(res.data.data[0].file_surat)
                    setNomorSurat(res.data.data[0].nomor_surat);
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getData()
            });
    }, [])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={styles.containerPdf}>
                <View>
                <TouchableOpacity onPress={() => downloadFromBase64()} style={{marginBottom: 10}}>
                                <View style={styles.buttonAdd}>
                                    <Text style={styles.textButton}>Download Surat</Text>
                                </View>
                            </TouchableOpacity>
                </View>
                <Pdf
                        source={{uri: `data:application/pdf;base64,${file}` }}
                        onLoadComplete={(numberOfPages,filePath)=>{
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page,numberOfPages)=>{
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error)=>{
                            console.log(error);
                        }}
                        onPressLink={(uri)=>{
                            console.log(`Link presse: ${uri}`)
                        }}
                        style={styles.pdf}/>
                </View> 
    )
}

const styles = StyleSheet.create({
      containerPdf: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,

    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    },
    buttonAdd: {backgroundColor: '#35BEE0', width: 130, height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
})
