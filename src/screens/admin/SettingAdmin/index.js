import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelAdmin'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation, route}) {

    const [dataWarga, setDataWarga] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [warna, setWarna] = useState('')
    const [rules, setRules] = useState('')

    async function getData() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        setRules(await Asynstorage.getItem('rules'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_admin', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataWarga(res.data.data)
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function hapusAdmin(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id:id
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_admin', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    getData()
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const alertDelete = (id) =>
        Alert.alert(
        "Hapus Admin",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => hapusAdmin(id) }
        ],
        { cancelable: false }
        );


      useEffect(() => {
        getData()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getData()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{flex: 1, padding: 20}}>
            
            <View style={{width: '100%'}}>
                        {(() => {
                            if (rules == "Superadmin"){
                                return (
                                    <View style={{backgroundColor: `#${warna}`,width: '45%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}}>
                        
                                    <TouchableOpacity onPress={() => navigation.navigate('TambahAdmin', {idDesa: route.params.idDesa})} >
                                        <Text style={styles.textButton}>Tambah admin</Text>
                                    </TouchableOpacity>
                                
                            </View>
                                )
                            }
                            
                        })()}
                   
            </View>
            <Tabel navigation={navigation} dataWarga={dataWarga} hapusAdmin={alertDelete} warna={warna} rules={rules} />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'}
})
