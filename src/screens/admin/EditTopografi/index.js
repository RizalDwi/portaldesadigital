import React,{useState, createContext, useEffect} from 'react'
import { View, Text, ToastAndroid } from 'react-native'
import Form from './../../../container/molecules/form/FormEditTopografi'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index( {route} ) {

    const [namaKategori, setNamaKategori] = useState('')
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function getDataTopografiDesa() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.itemId
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_kategori_topografi', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        setNamaKategori(res.data.data[0].nama_kategori)
                        setLoading(false)
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataTopografiDesa()
        console.log('itemId -> ', route.params.itemId)
      }, [])

      const showToast = () => {
        ToastAndroid.show("Data berhasil diperbaharui", ToastAndroid.SHORT);
      };

      const updateKategoriDesa = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.itemId,
                    nama_kategori: namaKategori,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/update_kategori_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

      const handleChangeInputNamaKategori = (value) => {
        setNamaKategori(value)
    }

    if (loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            handleChangeInputNamaKategori,
            namaKategori,
            updateKategoriDesa
        }}>
            <View style={{flex: 1}}>
                <Form />
            </View>
        </RootContext.Provider>
        
    )
}
