import React from 'react'
import { View, Text } from 'react-native'
import Form from './../../../container/molecules/form/FormTambahProgramBantuan'

export default function index({route}) {
    return (
        <View style={{flex: 1, padding: 20}}>
            <Form idDesa = {route.params.idDesa} />
        </View>
    )
}
