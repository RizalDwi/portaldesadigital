import React, {useEffect, useState} from 'react'
import { View, InteractionManager, ScrollView } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenKontakDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [dataLaporan, setDataLaporan] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_laporan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataLaporan(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => {
          getData()
        });
      }, [])
if(loading){
    return <LoadingScreen />
}
    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <Konten navigation={navigation} dataLaporan={dataLaporan}/>
            </ScrollView>
        </View>
    )
}
