import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, Image, Alert, ToastAndroid, Modal, Pressable, TextInput, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../screens/LoadingScreen'
import DropDownPicker from 'react-native-dropdown-picker';

export default function index( {navigation, route} ) {

    const [dataMarker, setDataMarker] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [judul, setJudul] = useState('')
    const [deskripsi, setDeskripsi] = useState('')
    const [idMarker, setIdMarker] = useState('')
    const [icon, setIcon] = useState('')
    const [modalVisible, setModalVisible] = useState(false);
    const [warna, setWarna] = useState('')

    async function getData() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_marker', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataMarker(res.data.data)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function updateMarker() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: idMarker,
                    judul: judul,
                    deskripsi: deskripsi,
                    icon: icon
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/update_marker', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    showToast("Data berhasil diubah")
                    setModalVisible(false)
                    getData()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showModal = (uJudul, uDeskripsi, uIcon, uId) => {
        setJudul(uJudul)
        setDeskripsi(uDeskripsi)
        setIcon(uIcon)
        setIdMarker(uId)
        setModalVisible(true)
      }
      const closeModal = () =>{
        setIcon('')
        setJudul('')
        setDeskripsi('')
        setModalVisible(false)
    }

    async function hapusMarker(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/delete_marker', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setModalVisible(false)
                    getData()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const alertDelete = (id) =>
        Alert.alert(
        "Hapus Marker",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => hapusMarker(id) }
        ],
        { cancelable: false }
        );
    
        const showToast = (value) => {
            ToastAndroid.show(value, ToastAndroid.SHORT);
          };

          useEffect(() => {
        
            InteractionManager.runAfterInteractions(() => {
              getData()
            });
          }, [])

    const renderItem = ({ item, index }) => {
        return(
            <View style={{flexDirection: 'row'}}>
                        <View style={styles.rowAksi}>
                            <View style={{flexDirection: 'row'}}>
                                <View style={styles.button}>
                                        <TouchableOpacity onPress={() => showModal(item.judul, item.deskripsi, item.icon, item.id) }>
                                            <Image source={require('./../../../assets/icons/pencil_edit.png')} style={{width: 40, height: 40}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.button}>
                                        <TouchableOpacity onPress={() => alertDelete(item.id)}>
                                            <Image source={require('./../../../assets/icons/cancel.png')} style={{width: 40, height: 40}} />
                                        </TouchableOpacity>
                                    </View>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelRow}>{item.judul}</Text>
                        </View>
                        <View style={styles.rowIcon}>
                        {(() => {
                            var source = ''
                            if (item.icon == 'balaidesa'){
                                source = require('./../../../assets/marker/balaidesa.png')
                            } else if(item.icon == 'gereja'){
                                source = require('./../../../assets/marker/gereja.png')
                            } else if(item.icon == 'masjid'){
                                source = require('./../../../assets/marker/masjid.png')
                            } else if(item.icon == 'monumen'){
                                source = require('./../../../assets/marker/monumen.png')
                            } else if(item.icon == 'parlemen'){
                                source = require('./../../../assets/marker/parlemen.png')
                            } else if(item.icon == 'rumah'){
                                source = require('./../../../assets/marker/rumah.png')
                            } else if(item.icon == 'sekolah'){
                                source = require('./../../../assets/marker/sekolah.png')
                            }
                            return <Image source={source} style={{width: 40, height: 40}} />                            
                        })()}
                           
                        </View>
                    </View>
        )
    }

    if(loading) {
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                <View>
                 <Text style={styles.label}>Pilih Marker</Text>
                            <DropDownPicker
                                items={[
                                {label: "Balai Desa", value: "balaidesa", icon: () => <Image source={require('../../../assets/marker/balaidesa.png')} style={{width: 20, height: 20}} />},
                                {label: "Gereja", value: "gereja", icon: () => <Image source={require('../../../assets/marker/gereja.png')} style={{width: 20, height: 20}} />},
                                {label: "Masjid", value: "masjid", icon: () => <Image source={require('../../../assets/marker/masjid.png')} style={{width: 20, height: 20}} />},
                                {label: "Monumen", value: "monumen", icon: () => <Image source={require('../../../assets/marker/monumen.png')} style={{width: 20, height: 20}} />},
                                {label: "Parlemen", value: "parlemen", icon: () => <Image source={require('../../../assets/marker/parlemen.png')} style={{width: 20, height: 20}} />},
                                {label: "Rumah", value: "rumah", icon: () => <Image source={require('../../../assets/marker/rumah.png')} style={{width: 20, height: 20}} />},
                                {label: "Sekolah", value: "sekolah", icon: () => <Image source={require('../../../assets/marker/sekolah.png')} style={{width: 20, height: 20}} />}
                                ]}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={120}
                                labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setIcon(value.value)}
                               
                            />
                            <View></View>
                 </View>
                  <View>
                    <Text style={styles.label}>Judul Marker</Text>
                    <TextInput placeholder="Misi Desa" style={styles.form} value={judul} onChangeText={(value) => setJudul(value)}/>
                 </View>
                 <View>
                    <Text style={styles.label}>Deskripsi Marker</Text>
                    <TextInput placeholder="Misi Desa" style={styles.form} value={deskripsi} onChangeText={(value) => setDeskripsi(value)}/>
                 </View>
                 <View style={{flexDirection: 'row'}}>
                    <Pressable
                        style={[styles.buttonModal, styles.buttonClose]}
                        onPress={() => updateMarker()}
                    >
                        <Text style={styles.textStyle}>Simpan</Text>
                    </Pressable>
                    <Pressable
                    style={[styles.buttonModal, styles.buttonClose, {marginLeft: 10}]}
                    onPress={() => closeModal()}
                  >
                    <Text style={styles.textStyle}>Batal</Text>
                  </Pressable>
                 </View>
                </View>
              </View>
            </Modal>
            <ScrollView>
                <View style={{width: '100%'}}>
                    <View style={{flexDirection: 'row', backgroundColor: `#${warna}`}}>
                        <View style={styles.rowAksi}>
                            <Text style={styles.labelHeader}>Act</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Nama</Text>
                        </View>
                        <View style={styles.rowIcon}>
                            <Text style={styles.labelHeader}>Icon</Text>
                        </View>
                        
                    </View>
                    <FlatList data={dataMarker} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
                    
                </View>
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center'
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 11,
        textAlign: 'center'
    },
    row: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 120, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowIcon: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 80, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowAksi: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 120, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    button: {paddingHorizontal: 5},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
      },
      modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '90%',
      },
      buttonModal: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width: 80
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "justify"
      },
      form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
})

