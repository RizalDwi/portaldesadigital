import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ToastAndroid, Button, Image } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../LoadingScreen'
import { ScrollView } from 'react-native-gesture-handler'
import { useForm, Controller } from "react-hook-form";
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import RNFetchBlob from 'rn-fetch-blob'
import DropDownPicker from 'react-native-dropdown-picker';

export default function index({route, navigation}) {

    const [listData, setListData] = useState([])
    const [loading, setLoading] = useState(true)
    const [nama, setNama] = useState('')
    const [listInput, setListInput] = useState([])
    const { control, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => tambah(data);
    const [listPersyaratan, setListPersyaratan] = useState([])
    const [namaLengkap, setNamaLengkap] = useState('')
    const [nik, setNik] = useState('')
    const [tanggalLahir, setTanggalLahir] = useState('')
    const [listAparat, setListAparat] = useState([])
    const [nomorSurat, setNomorSurat] = useState('')
    const [idTtd, setIdTtd] = useState('')
  

   

    const selectImageFromCamera = () => {
        setModalVisible(!modalVisible);
        ImagePicker.openCamera({
            width: 400,
            height: 300,
            cropping: true,
          }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    const tambah = async() => {
        console.log(route.params.id_desa)
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                
                let data = {
                    id_ajuan: route.params.id_ajuan,
                    nomor_surat: nomorSurat,
                    id_ttd: idTtd
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/arsip/store', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Request->",res)
                    // setNama('')
                    navigation.goBack();
                    // showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const tolakAjuan= async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                
                let data = {
                    id_ajuan: route.params.id_ajuan,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/ajuan-surat/update', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Request->",res)
                    // setNama('')
                    navigation.goBack();
                    // showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function Downnload() {
        const { config, fs } = RNFetchBlob;
        var token = await Asynstorage.getItem('api_token')
        console.log(listData)
        const downloads = fs.dirs.DownloadDir;
        return config({
            // add this option that makes response data to be stored as a file,
            // this is much more performant.
            fileCache : true,
            addAndroidDownloads : {
            useDownloadManager : true,
            notification : false,
            path:  downloads + '/fileNew.docx',
            }
        })
        .fetch('GET', 'https://shielded-eyrie-91877.herokuapp.com/ajuan/cetak', listData)
        .then((res) => {
            console.log(res)
        });
        
    }

    async function getData() {
        
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_ajuan: route.params.id_ajuan,
                    id_desa: route.params.id_desa
                }
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/ajuan-surat/detail-pengajuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setListInput(res.data.isi_ajuan)
                    setListPersyaratan(res.data.syarat_ajuan)
                    setNamaLengkap(res.data.data_warga[0].nama_lengkap)
                    setNik(res.data.data_warga[0].nik)
                    setTanggalLahir(res.data.data_warga[0].tanggal_lahir)
                    setListAparat(res.data.aparat)
                    setNomorSurat(res.data.nomor_surat)
                    console.log("Isian", res)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getData()
  }, [])

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen/>
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>

            <View style={{alignItems: 'center'}}>
                    <Text style={styles.labelDokumen}>Persyaratan</Text>
                </View>
                <View style={{marginBottom: 10}}>

                {listPersyaratan.map((prop, key) => {

                    return (
                        <View>
                            <Text style={styles.label}>{prop.nama}</Text>
                            <View style={{marginVertical: 10, paddingHorizontal: '10%', alignItems: 'center'}}>
                                <Image source={{uri: `data:image/png;base64,${prop.file}`}} style={{width: 400, height: 400}} />
                            </View>
                           
                        </View>
                    );
                })}
                </View>
                <View style={{marginBottom: 10}}>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.labelDokumen}>Tanda Tangan</Text>
                </View>
                <Text style={styles.label}>Ditanda tangani oleh :</Text>
                <View>
                                <DropDownPicker
                                items={listAparat}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={300}
                                labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setIdTtd(value.value)}
                            />
                            <View></View>
                            </View>
            </View>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.labelDokumen}>Data Surat</Text>
                </View>
            
            <View style={{marginBottom: 10}}>
                        <View>
                            <Text style={styles.label}>Nomor Surat</Text>
                            <TextInput
                                    style={styles.form}
                                    value={nomorSurat}
                                    onChangeText={(value) => setNomorSurat(value)}
                                />
                                
                        </View>
                        <View>
                            <Text style={styles.label}>Nama Lengkap</Text>
                            <TextInput
                                    style={styles.form}
                                    value={namaLengkap}
                                    editable={false}
                                />
                                
                        </View>
                        <View>
                            <Text style={styles.label}>No KTP</Text>
                            <TextInput
                                    style={styles.form}
                                    value={nik}
                                    editable={false}
                                />
                                
                        </View>
                        <View>
                            <Text style={styles.label}>Tanggal Lahir</Text>
                            <TextInput
                                    style={styles.form}
                                    value={tanggalLahir}
                                    editable={false}
                                />
                                
                        </View>
            
            {listInput.map((prop, key) => {
                    return (
                        // <Button style={{borderColor: prop[0]}}  key={key}>{prop[1]}</Button>
                        <View>
                            <Text style={styles.label}>{prop.nama.replace(/_/g, " ").trim().replace(/^\w/, (c) => c.toUpperCase())}</Text>
                            <TextInput
                                    style={styles.form}
                                    value={prop.isi}
                                    editable={false}
                                />
                                
                        </View>
                    );
                })}

            </View>
                
               {/* <TouchableOpacity style={{width: '100%'}} onPress={() => Downnload()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity> */}
               <View style={{marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <TouchableOpacity style={{width: '40%'}} onPress={() => tambah()}>
                        <View style={{height: 40, backgroundColor: '#26A718', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Setujui</Text>
                        </View>
                    </TouchableOpacity>
                        <TouchableOpacity style={{width: '40%'}} onPress={() => tolakAjuan()}>
                            <View style={{height: 40, backgroundColor: '#BB2424', justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Tolak</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
           
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        formDokumen: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelDokumen : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 25
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
