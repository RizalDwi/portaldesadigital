import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image, TouchableOpacity, Alert, ToastAndroid } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../LoadingScreen'
import RNFetchBlob from 'rn-fetch-blob'

export default function index({route, navigation}) {

    const [listArsip, setListArsip] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);

    async function getData() {
        
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.id_desa
                }
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/arsip/view-admin', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setListArsip(res.data.data)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    function Downnload(id, nomor_surat) {
        console.log(nomor_surat)
        const { config, fs } = RNFetchBlob;
        const downloads = fs.dirs.DownloadDir;
        setLoading(true)
        return config({
            fileCache : true,
            addAndroidDownloads : {
            useDownloadManager : true,
            notification : false,
            path:  downloads + '/result.docx',
            }
        })
        .fetch('GET', 'https://shielded-eyrie-91877.herokuapp.com/arsip/cetak/' + id)
        .then((res) => {
            showToast("Surat berhasil diunduh")
            setLoading(false)
        });
        
    }

          const showToast = (value) => {
            ToastAndroid.show(value, ToastAndroid.SHORT);
          };

    useEffect(() => {
        getData()
    const interval = setInterval(() => {
        setCount((count) => count + 1);
      }, 1000);
    const unsubscribe = navigation.addListener('focus', () => {
        setCount(0);
        getData()
      });
  
      return () => {
        // Clear setInterval in case of screen unmount
        clearTimeout(interval);
        // Unsubscribe for the focus Listener
        unsubscribe;
      };

    
  }, [navigation])

  const renderItem = ({ item, index }) => {
    return(
        <View style={styles.cardContainer}>
        <View style={{flex:1}}>
        <View style={styles.row}>
                <View style={styles.coloumnLeft}>
                    <Text style={styles.labelHeader}>Nama Pengaju</Text>
                </View>
                <View style={styles.coloumnRight}>
                    <Text style={styles.labelRow}>: {item.nama_lengkap}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.coloumnLeft}>
                    <Text style={styles.labelHeader}>NIK</Text>
                </View>
                <View style={styles.coloumnRight}>
                    <Text style={styles.labelRow}>: {item.nik}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.coloumnLeft}>
                    <Text style={styles.labelHeader}>Tanggal</Text>
                </View>
                <View style={styles.coloumnRight}>
                    <Text style={styles.labelRow}>: {item.tanggal}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.coloumnLeft}>
                    <Text style={styles.labelHeader}>Nomor Surat</Text>
                </View>
                <View style={styles.coloumnRight}>
                    <Text style={styles.labelRow}>: {item.nomor_surat}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.coloumnLeft}>
                    <Text style={styles.labelHeader}>Nama Layanan</Text>
                </View>
                <View style={styles.coloumnRight}>
                    <Text style={styles.labelRow}>: {item.nama_layanan}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.coloumnLeft}>
                    <Text style={styles.labelHeader}>Ditanda tangani oleh</Text>
                </View>
                <View style={styles.coloumnRight}>
                    <Text style={styles.labelRow}>: {item.aparat}</Text>
                </View>
            </View>
           
        </View>
            <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                <TouchableOpacity onPress={() => Downnload(item.id, item.nomor_surat) }>
                    <View style={{backgroundColor: '#F19000', width: 50, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                        <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Cetak</Text>
                    </View>
                </TouchableOpacity>
            </View>
    </View>

    )
}

    if(loading){
        return <LoadingScreen/>
     }

    return (
        <View style={{flex: 1, padding: 20}}>
           
            <ScrollView>
                    <FlatList data={listArsip} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    buttonAdd: {width: '50%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10},
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
})

