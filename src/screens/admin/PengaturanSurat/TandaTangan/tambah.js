import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid, Image } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import Axios from 'axios'
import LoadingScreen from './../../../LoadingScreen'
import DropDownPicker from 'react-native-dropdown-picker';

export default function index({route}) {

    const [loading, setLoading] = useState(false)
    const [idAparat, setIdAparat] = useState('')
    const [modalVisible, setModalVisible] = useState(false);
    const [foto, setFoto] = useState('')
    const [listAparat, setListAparat] = useState([])

    const SelectImageFromGaleery = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true
          }).then(image => {
            setModalVisible(!modalVisible);
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    const selectImageFromCamera = () => {
        setModalVisible(!modalVisible);
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
          }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    const getAparat = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    id_desa: route.params.id_desa,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tanda-tangan/aparat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Form Data ->", data)
                    console.log(res)
                    setListAparat(res.data.data)
                    setLoading(false)
                })
                .catch((err) => {
                    //alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const tambah = async() => {
        console.log(route.params.id_desa)
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    file_ttd: foto,
                    id_aparat: idAparat
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tanda-tangan/store', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setIdAparat('')
                    setFoto('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

      useEffect(() => {
        getAparat()
        }, [])

    if(loading){
        return <LoadingScreen/>
    }

    return (
        <View style={{flex: 1, padding: 20}}>
                <View>
                    <Text style={styles.label}>Nama Aparat</Text>
                    <DropDownPicker
                        items={listAparat}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setIdAparat(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <TouchableOpacity onPress={() => {
                                                        SelectImageFromGaleery();
                                                        }}>
                            <View style={{width: 190, height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 15, textAlign: 'center'}}>Upload Tanda Tangan</Text>
                            </View>
                    </TouchableOpacity>
                </View>
              
            <View style={{marginVertical: 10, paddingHorizontal: '10%', alignItems: 'center'}}>
                <Image source={foto === '' ? require('./../../../../assets/icons/thumnail.png') : {uri: `data:image/png;base64,${foto}`}} style={{width: 400, height: 400}} />
            </View>
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => tambah()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      centeredView: {
        flex: 1,
        marginTop: 22
      },
      modalView: {
        width: '100%',
        height: 200,
        position: 'absolute',
        bottom: 0,
        backgroundColor: "white",
        padding: 35,
        alignItems: "center",
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        textAlign: "center",
        fontFamily: 'Poppins-Bold'
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
})
