import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import LoadingScreen from './../../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {
    const [nama, setNama] = useState('')
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function getData() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.id
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/persyaratan-surat/view', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        setNama(res.data.data[0].nama)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
       getData()
      
      }, [])

    const updatePersyaratan = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    nama: nama,
                    id: route.params.id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/persyaratan-surat/update', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    //console.log(res)
                    //console.log("Data 2 -> ", data)
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil diubah", ToastAndroid.SHORT);
      };

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{flex: 1, padding: 20}}>
                <View>
                    <Text style={styles.label}>Nama</Text>
                    <TextInput placeholder="Nama Klasifikasi" style={styles.form} value={nama} onChangeText={(value) => setNama(value)} />
                </View>
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => updatePersyaratan()} >
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
