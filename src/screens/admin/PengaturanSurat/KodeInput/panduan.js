import React, {useState} from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default function index() {

    return (
        <View style={{flex: 1, padding: 20}}>
            <View>
                <Text style={styles.textTitle}>Apa itu kode input?</Text>
            </View>
            <View>
                <Text style={ styles.textKonten }>
                    Kode input merupakan daftar variabel pada template surat yang harus dimasukan ke dalam aplikasi. 
                    Tujuan mengisi kode input ke dalam aplikasi adalah agar aplikasi mengenali setiap variabel pada
                    template surat sehingga aplikasi bisa mengganti variabel tersebut dengan data yang dimasukan oleh pengguna.
                    Penulisan variabel pada template adalah {"${nama_variabel}"}
                </Text>
                <Text style={[styles.textKonten, {marginTop: 10}]}>
                    Berikut merupakan contoh variabel pada template surat :
                </Text>
                <View>
                    <Image source={require('../../../../assets/images/kode_input.png')} style={{ width: '100%', height: 100 }} />
                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
    textTitle: {
        fontFamily: 'Poppins-Bold',
        fontSize: 25,
        color: '#394761',
    },
    textKonten: {
        textAlign: 'justify',
        fontFamily: 'Poppins',
        color: '#394761',
    }
})
