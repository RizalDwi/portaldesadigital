import React, {useState} from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ToastAndroid, ScrollView } from 'react-native'
import RNFetchBlob from 'rn-fetch-blob'

export default function index() {

    function Downnload() {
        const { config, fs } = RNFetchBlob
        const downloads = fs.dirs.DownloadDir; // this is the pictures directory. You can check the available directories in the wiki.
        let options = {
        fileCache: true,
        addAndroidDownloads : {
            useDownloadManager : true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
            notification : false,
            path:  downloads + "/template_surat.docx", // this is the path where your downloaded file will live in
            description : 'Downloading Template.'
        }
        }
        config(options).fetch('GET', "https://shielded-eyrie-91877.herokuapp.com/cetak").then((res) => {
            showToast("Template sudah terunduh")
        })
        
    }

    const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

    return (
        <View style={{flex: 1, padding: 20}}>
            
            <ScrollView>
                <Text style={ styles.textKonten }>
                    Untuk membuat template surat, buatlah surat seperti biasa dengan format docx. Pada bagian data yang bisa diubah,
                    gunakanlah variabel dengan penulisan sebagai berikut {"${nama_variabel}"}. Penulisan variabel pada template gunakan huruf kecil dan ganti sepasi
                    dengan garis bawah.
                </Text>
                <Text style={styles.textKonten}>
                    Berikut merupakan contoh variabel pada template surat :
                </Text>
                <View>
                    <Image source={require('../../../../assets/images/kode_input.png')} style={{ width: '100%', height: 100, marginBottom: 10 }} />
                </View>
                <Text style={styles.textKonten}>
                    Untuk contoh file template surat, dapat diunduh di sini: 
                </Text>
                <View style={{alignItems: 'center', marginBottom: 10}}>
                    <TouchableOpacity onPress={() => Downnload()}>
                        <View style={{backgroundColor: '#4CC9F0', width: 130, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                             <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Download Template</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View>
                    <Text style={styles.textTitle}>TIPS membuat variabel template</Text>
                </View>
                <Text style={styles.textKonten}>
                    Agar data terisi otomatis dan mempermudah pengguna mengisi formulir pengajuan surat, gunakanlah variabel berikut :
                </Text>
                <View style={{ marginTop: 10 }}>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${nomor_surat}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi nomor surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${nama}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi nama pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${no_ktp}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi nomor KTP pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${no_kk}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi nomor KK pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${tanggal_lahir}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi tanggal lahir pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${nama_pamong}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi nama pamong yang akan memberi tanda tangan</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${jabatan}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi jabatan pamong</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${nip}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi NIP pamong</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${ttd}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk meletakan foto tanda tangan pamong</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${tanggal_surat}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi tanggal surat saat dibuat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${tanggal_mulai}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi tanggal mulai berlakunya surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${tanggal_akhir}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi tanggal akhir berlakunya surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${agama}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi agama pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${sex}}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi jenis kelamin pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${status}}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi status pernikahan pengaju surat</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>{"${kepentingan}}"}</Text>
                        </View>
                        <View style={styles.listTips}>
                            <Text style={styles.textKonten}>: Untuk mengisi kepentingan pengajuan surat</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
    textTitle: {
        fontFamily: 'Poppins-Bold',
        fontSize: 25,
        color: '#394761',
    },
    textKonten: {
        textAlign: 'justify',
        fontFamily: 'Poppins',
        color: '#394761',
    },
    row: {
        flexDirection: 'row',
    },
    listTips: {
        width: '50%'
    }
})
