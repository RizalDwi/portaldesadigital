import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ToastAndroid, Dimensions } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../LoadingScreen'
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob'
import FileViewer from 'react-native-file-viewer';
import DropDownPicker from 'react-native-dropdown-picker';

export default function index({route}) {

    const [loading, setLoading] = useState(true)
    const [kode, setKode] = useState('')
    const [nama, setNama] = useState('')
    const [path, setPath] = useState('')
    const [jumlah, setJumlah] = useState('')
    const [berlaku, setBerlaku] = useState('')
    const [listKlasifikasi, setListKlasifikasi] = useState([])

    const [file, setFile] = useState('')

    const getFile = async() => {
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.docx],
            });
            //await FileViewer.open(res.uri);
            convertFile(res.uri)
            setPath(res.uri)
            
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              // User cancelled the picker, exit any dialogs or menus and move on
            } else {
              throw err;
            }
          }
    }

    const convertFile = (exampleFilePath) => {
        const fs = RNFetchBlob.fs;
        fs.readFile(exampleFilePath, 'base64')
          .then(data => {
              setFile(data)
              console.log(data)
          })
      }
    
    useEffect(() => {
        async function getData() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.id
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/format-surat/view', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        setListKlasifikasi(res.data.aparat)
                        setNama(res.data.data[0].nama_layanan)
                        setFile(res.data.data[0].template_surat)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
       getData()
      
      }, [])

    const tambahFormat = async() => {
        console.log(route.params.id_desa)
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    nama_layanan: nama,
                    template_surat: file,
                    masa_berlaku: jumlah + " " + berlaku,
                    kode_klasifikasi: kode,
                    id_desa: route.params.id_desa,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/format-surat/store', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setKode('')
                    setNama('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen/>
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <Text style={styles.label}>Klasifikasi Surat</Text>
            <View>
                            <DropDownPicker
                            items={listKlasifikasi}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => setKode(value.value)}
                        />
                        <View></View>
                        </View>
                <View>
                    <Text style={styles.label}>Nama Layanan</Text>
                    <TextInput placeholder="Nama Layanan" style={styles.form} onChangeText={(value) => setNama(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Masa Berlaku</Text>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{width: '45%'}}>
                            <DropDownPicker
                            items={[
                                {label:'1',value:'1'},{label:'2',value:'2'},{label:'3',value:'3'},{label:'4',value:'4'},{label:'5',value:'5'},{label:'6',value:'6'},{label:'7',value:'7'},{label:'8',value:'8'},{label:'9',value:'9'},{label:'10',value:'10'},{label:'11',value:'11'},{label:'12',value:'12'},{label:'13',value:'13'},{label:'14',value:'14'},{label:'15',value:'15'},{label:'16',value:'16'},{label:'17',value:'17'},{label:'18',value:'18'},{label:'19',value:'19'},{label:'20',value:'20'},{label:'21',value:'21'},{label:'22',value:'22'},{label:'23',value:'23'},{label:'24',value:'24'},{label:'25',value:'25'},{label:'26',value:'26'},{label:'27',value:'27'},{label:'28',value:'28'},{label:'29',value:'29'},{label:'30',value:'30'},{label:'31',value:'31'}
                            ]}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => setJumlah(value.value)}
                        />
                        <View></View>
                        </View>
                        <View style={{width: '45%', marginLeft: 10}}>
                            <DropDownPicker
                            items={[
                                {label: 'Hari', value: 'Hari'},
                                {label: 'Minggu', value: 'Minggu'},
                                {label: 'Bulan', value: 'Bulan'}
                            ]}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => setBerlaku(value.value)}
                        />
                        <View></View>
                        </View>
                    </View>
                    
            
                </View>

                <TouchableOpacity style={{width: '100%'}} onPress={() => getFile()}>
                <View style={{width: 190, height: 30, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 14, textAlign: 'center'}}>Upload template surat</Text>
                    </View>
               </TouchableOpacity>
               {(() => {
                            if (path != ''){
                                return(
                                <View style={{marginTop: 10}}>
                                    <Text style={styles.labelDokument}>Template telah ditambahkan</Text>
                                    <TouchableOpacity onPress={() => FileViewer.open(path)}>
                                        <View style={{backgroundColor: '#4CC9F0', width: 110, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                            <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Lihat Template</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                )
                            }
                        })()}

               
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => tambahFormat()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelDokument : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 14
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
})
