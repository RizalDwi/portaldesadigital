import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'


export default function index({route, navigation}) {
    return (
        <View style={{flex: 1, padding: 10}}>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('KlasifikasiSurat', {warna: route.params.warna, id_desa: route.params.id_desa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Klasifikasi Surat</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('FormatSurat', {warna: route.params.warna, id_desa: route.params.id_desa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Format Surat</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('Persyaratan', {warna: route.params.warna, id_desa: route.params.id_desa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Persyaratan Surat</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('KelolaPengajuan', {warna: route.params.warna, id_desa: route.params.id_desa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Pengajuan Surat</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('TandaTangan', {warna: route.params.warna, id_desa: route.params.id_desa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Tanda Tangan</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('Arsip', {warna: route.params.warna, id_desa: route.params.id_desa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Arsip</Text>
                    </View>
                </TouchableOpacity>
            </View>
           
        </View>
    )
}
