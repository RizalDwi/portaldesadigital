import React,{useState, useEffect, createContext} from 'react'
import { View, ToastAndroid, Alert, StyleSheet, Modal, Pressable, Text, InteractionManager} from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenKelolaPemerintahan/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route, navigation}) {

    const [visi, setVisi] = useState([])
    const [misi, setMisi] = useState('')
    const [idVisi, setIdVisi] = useState(0)
    const [aparat, setAparat] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [idDesa, setIdDesa] = useState(route.params.idDesa)
    const [modalVisible, setModalVisible] = useState(false);
    const [tupoksi, setTupoksi] = useState('')
    const [warna, setWarna] = useState('')

    async function getDataPemerintahan() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/pemerintahan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setAparat(res.data.aparat)
                    setMisi(res.data.misi)
                    if(res.data.visi.length > 0){
                      setVisi(res.data.visi[0].visi)
                      setIdVisi(res.data.visi[0].id)
                    }
                    
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
      InteractionManager.runAfterInteractions(() => {
        getDataPemerintahan()
      });
        
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            InteractionManager.runAfterInteractions(() => {
              getDataPemerintahan()
            });
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      const ModalTupoksi = (value) => {
        setTupoksi(value)
        setModalVisible(true)
        console.log("test")
        
      }

      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const deleteAparat = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_aparat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataPemerintahan()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const deleteMisi = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_misi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataPemerintahan()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDeleteMisi = (id) =>
        Alert.alert(
        "Hapus misi desa",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteMisi(id) }
        ],
        { cancelable: false }
        );

        const alertDeleteAparat = (id) =>
        Alert.alert(
        "Hapus aparat desa",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteAparat(id) }
        ],
        { cancelable: false }
        );

        const updateVisi = async() => {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: idVisi,
                        visi: visi,
                        id_desa: idDesa
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/update_visi', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        showToast("Data berhasil diperbaharui")
                        
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
          }
    
        const handleChangeInputVisi = (value) => {
            setVisi(value)
        }


        if(loading){
            return <LoadingScreen />
        }

    return (
        <RootContext.Provider value={{
            visi,
            misi,
            idVisi,
            aparat,
            handleChangeInputVisi,
            idDesa,
            deleteAparat,
            updateVisi,
            deleteMisi,
            alertDeleteMisi,
            alertDeleteAparat,
            ModalTupoksi,
            warna
        }}>
            <View style={{flex:1}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>{tupoksi}</Text>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalVisible(!modalVisible)}
                  >
                    <Text style={styles.textStyle}>Tutup</Text>
                  </Pressable>
                </View>
              </View>
            </Modal>
                <Konten />
            </View>
        </RootContext.Provider>
    )
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '90%',
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    width: 80
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "justify"
  }
});
