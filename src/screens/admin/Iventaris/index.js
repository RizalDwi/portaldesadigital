import React, {useState, useEffect} from 'react'
import { View } from 'react-native'
import Header from './../../../container/molecules/header/HeaderProfileAdmin'
import Konten from '../../../container/molecules/konten/admin/KontenKelolaIventaris'
import Asynstorage from '@react-native-community/async-storage'

export default function index({navigation, route}) {

    const [warna, setWarna] = useState('')
    
    const getWarna = async() => {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
    }

    useEffect(() => {
        getWarna()
     }, [])

    return (
        <View style={{flex: 1}}>
            <Header />
            <Konten navigation={navigation} id_desa={route.params.idDesa} warna={warna} />
        </View>
    )
}
