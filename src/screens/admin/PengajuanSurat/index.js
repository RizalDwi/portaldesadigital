import React, {createContext, useEffect, useState} from 'react'
import { View, InteractionManager, ToastAndroid } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenPengajuanSurat/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({navigation, route}) {

    
    const [pengajuan, setPengajuan] = useState([])
    const [file, setFile] = useState('')
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [nomorSurat, setNomorSurat] = useState('')
    const [tandaTangan, setTandaTangan] = useState('')

    const handleFile = (value) => {
        setFile(value)
    }

    const handleTandaTangan = (value) => {
        setTandaTangan(value)
    }

    const handleNomorSurat = (value) => {
        setNomorSurat(value)
    }

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.idPengaju,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_pengajuan_satuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Pengajuan --->", res)
                    setPengajuan(res.data.data[0])
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function uploadSurat() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_pengajuan: route.params.idPengaju,
                     file_surat: file,
                     nomor_surat: nomorSurat,
                     tanda_tangan: tandaTangan
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_arsip_surat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                   setLoading(false)
                   showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function updateStatusTerukan() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.idPengaju,
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/surat_diteruskan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getData()
                   showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function updateStatusTolak() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.idPengaju,
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/surat_ditolak', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    navigation.goBack();
                   showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }


    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => {
          getData()
        });
      }, [])

      const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    if(loading) {
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            pengajuan,
            handleFile,
            handleNomorSurat,
            handleTandaTangan,
            uploadSurat,
            updateStatusTerukan,
            updateStatusTolak,
            file
        }}>
            <View style={{flex: 1}}>
                <Konten />
            </View>
        </RootContext.Provider>
    )
}
