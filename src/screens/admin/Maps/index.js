import React, {useState, useEffect} from 'react'
import { View, Text, TouchableOpacity, ScrollView, StyleSheet, ToastAndroid, Modal, Pressable, TextInput, Image, PermissionsAndroid} from 'react-native'
import MapView,{Marker, Polygon} from 'react-native-maps'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import DropDownPicker from 'react-native-dropdown-picker';
import LoadingScreen from './../../../screens/LoadingScreen'

export default function index({route, navigation}) {

    const [border, setBorder] = useState([])
    const [loading, setLoading] = useState(true)
    const [modalVisible, setModalVisible] = useState(false);
    const [latlng, setLatlng] = useState([])
    const [judul, setJudul] = useState('')
    const [deskripsi, setDeskripsi] = useState('')
    const [icon, setIcon] = useState('')
    const [listCoordinate, setListCoordinate] = useState([])
    const [count, setCount] = useState(0);
    const [latitudeDesa, setLatitudeDesa] = useState(0)
    const [longitudeDesa, setLongtitudeDesa] = useState(0)

    const [position, setPosition] = useState({
        latitude: -7.064469,
        longitude: 112.160808,
        latitudeDelta: 0.009,
        longitudeDelta: 0.009,
      });

      const [margin, setMargin] = useState(1)

      async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_border_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setBorder(res.data.data)
                    setLatitudeDesa(res.data.data[0].latitude)
                    setLongtitudeDesa(res.data.data[0].longitude)
                    setListCoordinate(res.data.marker)
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function addMarker() {
        setModalVisible(false)
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    judul: judul,
                    deskripsi: deskripsi,
                    latitude: latlng.latitude,
                    longitude: latlng.longitude,
                    icon: icon
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_marker', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setLoading(false)
                    setMargin(1)
                    setListCoordinate(res.data.marker)
                    showToast()
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const requestPermission = async() =>{
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (granted) {
        console.log( "You can use the ACCESS_FINE_LOCATION" )
      } 
      else {
        console.log( "ACCESS_FINE_LOCATION permission denied" )
      }
    }


      useEffect(() => {
        requestPermission()
        getData()
        const interval = setInterval(() => {
            setMargin(1)
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setMargin(1)
            setCount(0);
            getData()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };    
      }, [navigation])

      const showModal = (latlng) => {
        setLatlng(latlng)
        setModalVisible(true)
      }
      const closeModal = () =>{
          setLatlng([])
          setJudul('')
          setDeskripsi('')
          setModalVisible(false)
      }

      const showToast = () => {
        ToastAndroid.show("Marker tersimpan", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                <View>
                 <Text style={styles.label}>Pilih Marker</Text>
                            <DropDownPicker
                                items={[
                                {label: "Balai Desa", value: "balaidesa", icon: () => <Image source={require('../../../assets/marker/balaidesa.png')} style={{width: 20, height: 20}} />},
                                {label: "Gereja", value: "gereja", icon: () => <Image source={require('../../../assets/marker/gereja.png')} style={{width: 20, height: 20}} />},
                                {label: "Masjid", value: "masjid", icon: () => <Image source={require('../../../assets/marker/masjid.png')} style={{width: 20, height: 20}} />},
                                {label: "Monumen", value: "monumen", icon: () => <Image source={require('../../../assets/marker/monumen.png')} style={{width: 20, height: 20}} />},
                                {label: "Parlemen", value: "parlemen", icon: () => <Image source={require('../../../assets/marker/parlemen.png')} style={{width: 20, height: 20}} />},
                                {label: "Rumah", value: "rumah", icon: () => <Image source={require('../../../assets/marker/rumah.png')} style={{width: 20, height: 20}} />},
                                {label: "Sekolah", value: "sekolah", icon: () => <Image source={require('../../../assets/marker/sekolah.png')} style={{width: 20, height: 20}} />}
                                ]}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={120}
                                labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setIcon(value.value)}
                               
                            />
                            <View></View>
                 </View>
                  <View>
                    <Text style={styles.label}>Judul Marker</Text>
                    <TextInput placeholder="Misi Desa" style={styles.form} onChangeText={(value) => setJudul(value)}/>
                 </View>
                 <View>
                    <Text style={styles.label}>Deskripsi Marker</Text>
                    <TextInput placeholder="Misi Desa" style={styles.form} onChangeText={(value) => setDeskripsi(value)}/>
                 </View>
                 <View style={{flexDirection: 'row'}}>
                    <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => addMarker()}
                    >
                        <Text style={styles.textStyle}>Simpan</Text>
                    </Pressable>
                    <Pressable
                    style={[styles.button, styles.buttonClose, {marginLeft: 10}]}
                    onPress={() => closeModal()}
                  >
                    <Text style={styles.textStyle}>Batal</Text>
                  </Pressable>
                 </View>
                </View>
              </View>
            </Modal>
            <MapView style={{flex:1, marginBottom: margin}}
                initialRegion={{
                  latitude: latitudeDesa,
                    longitude: longitudeDesa,
                    latitudeDelta: 0.06,
                    longitudeDelta: 0.06
                }}
                onMapReady={() => setMargin(0)}
                showsUserLocation={true}
                showsMyLocationButton={true}
                onPress={(e) => showModal(e.nativeEvent.coordinate)}
                >
              
            <Polygon
                coordinates={border}
                strokeColor="#000" // fallback for when `strokeColors` is not supported by the map-provider
                strokeColors={[
                    '#7F0000',
                    '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                    '#B24112',
                    '#E5845C',
                    '#238C23',
                    '#7F0000'
                ]}
                strokeWidth={2}
        />
        {listCoordinate.map((item, index) => {
                    var source = ''
                    if (item.icon == 'balaidesa'){
                        source = require('./../../../assets/marker/balaidesa.png')
                    } else if(item.icon == 'gereja'){
                        source = require('./../../../assets/marker/gereja.png')
                    } else if(item.icon == 'masjid'){
                        source = require('./../../../assets/marker/masjid.png')
                    } else if(item.icon == 'monumen'){
                        source = require('./../../../assets/marker/monumen.png')
                    } else if(item.icon == 'parlemen'){
                        source = require('./../../../assets/marker/parlemen.png')
                    } else if(item.icon == 'rumah'){
                        source = require('./../../../assets/marker/rumah.png')
                    } else if(item.icon == 'sekolah'){
                        source = require('./../../../assets/marker/sekolah.png')
                    }
                    return (
                        <Marker key={item.id} coordinate={{
                            latitude: parseFloat(item.latitude),
                            longitude: parseFloat(item.longitude)
                        }}
                        icon={source}
                        title={item.judul}
                        description={item.deskripsi}
                        />
                    )
                })}
            </MapView>
            <View style={{position: 'absolute', width: 60, height: 60, padding: 10}}>
              <TouchableOpacity onPress={() => navigation.navigate('MarkerDesa', {idDesa: route.params.idDesa})}>
                  <Image source={require('../../../assets/icons/more_detail.png')} style={{width: 50, height: 50}} />   
              </TouchableOpacity> 
            </View>
            
        </View>
    )
}
const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
      },
      modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '90%',
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width: 80
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "justify"
      },
      form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 14
    }
})
