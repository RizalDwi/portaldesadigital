import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, InteractionManager } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelRealisasi'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation, route}) {

    const [dataRealisasi, setDataRealisasi] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [warna, setWarna] = useState('')

    async function getData() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_realisasi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataRealisasi(res.data.data)
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }


      useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getData()
          });
        
      }, [])

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{flex: 1, padding: 20}}>
            
            <View style={{width: '100%'}}>
                    <View style={{backgroundColor: `#${warna}`,width: '45%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}}>
                        
                            <TouchableOpacity onPress={() => navigation.navigate('FormTambahRealisasi', {idDesa: route.params.idDesa})} >
                                <Text style={styles.textButton}>Tambah Realisasi</Text>
                            </TouchableOpacity>
                        
                    </View>
            </View>
            <Tabel warna={warna} navigation={navigation} dataRealisasi={dataRealisasi} />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'}
})
