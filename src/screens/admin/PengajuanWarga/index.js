import React, {createContext, useEffect, useState} from 'react'
import { View, Text } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenKelolaPengajuanWarga/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'


export const RootContext = createContext();
export default function index({navigation, route}) {

    const [listPengajuan, setListPengajuan] = useState([])
    const [listWarga, setListWarga] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);

    async function getKonten() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/list_ajuan_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setListPengajuan(res.data.pengajuan)
                    setListWarga(res.data.warga)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getKonten()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getKonten()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      if(loading){
          return <LoadingScreen />
      }

    return (
        <RootContext.Provider value={{
            listPengajuan,
            listWarga
        }}>
            <View style={{flex: 1}}>
                <Konten/>
            </View>
        </RootContext.Provider>
    )
}
