import React, {useState, useEffect} from 'react'
import { View, Text, ScrollView } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenKelolaProdukWarga'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [listPengajuan, setListPengajuan] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [cekData, setCekData] = useState(false)

    async function getKonten() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/list_ajuan_produk', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    if (res.data.data.length == 0){
                        setCekData(true)
                    }
                    setListPengajuan(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getKonten()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getKonten()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      if(loading){
          return <LoadingScreen />
      }

      if(cekData){
          return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20}}>
                <Text style={{fontFamily: 'Rubik-Regular', fontSize: 17, textAlign: 'center'}}>Tidak ada pengajuan</Text>
            </View>
          )
      }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <Konten navigation={navigation} listPengajuan={listPengajuan} />
            </ScrollView>
        </View>
    )
}
