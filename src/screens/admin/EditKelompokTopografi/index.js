import React, {createContext, useState, useEffect} from 'react'
import { View, ToastAndroid } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import Form from './../../../container/molecules/form/FormEditKelompokTopografi'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index({route}) {

    const [namaKelompok, setNamaKelompok] = useState('')
    const [jumlah, setJumlah] = useState('')
    const [loading, setLoading] = useState(true)

    const handleChangeInputNamaKelompok = (value) => {
        setNamaKelompok(value)
    }

    const handleChangeInputJumlah = (value) => {
        setJumlah(value)
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil diperbaharui", ToastAndroid.SHORT);
      };

    useEffect(() => {
        async function getDataKelompokTopografi() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.itemId
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_kelompok_topografi_satuan', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        setNamaKelompok(res.data.data[0].nama_kelompok)
                        setJumlah(res.data.data[0].jumlah)
                        setLoading(false)
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataKelompokTopografi()
        console.log('itemId -> ', route.params.itemId)
      }, [])

      const updateKelompokTopografi = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.itemId,
                    nama_kelompok: namaKelompok,
                    jumlah: jumlah
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/update_kelompok_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            namaKelompok,
            jumlah,
            handleChangeInputJumlah,
            handleChangeInputNamaKelompok,
            updateKelompokTopografi
        }}>
            <View style={{flex: 1}}>
                <Form />
            </View>
        </RootContext.Provider>
        
    )
}
