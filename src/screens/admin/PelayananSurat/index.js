import React, {createContext, useState, useEffect} from 'react'
import { View, InteractionManager, ToastAndroid, Alert} from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KelolaSurat/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route, navigation}) {

    const [idDesa, setIdDesa] = useState(route.params.idDesa)
    const [pengajuan, setPengajuan] = useState([])
    const [arsip, setArsip] = useState([])
    const [jenis, setJenis] = useState([])
    const [loading, setLoading] = useState(true)
    const [warna, setWarna] = useState('')
    const [count, setCount] = useState(0);

    async function getData() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_data_pengajuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setPengajuan(res.data.pengajuan)
                    setArsip(res.data.arsip)
                    setJenis(res.data.jenis)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function ubahStatusJenisSurat(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/ubah_status_jenis_surat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    getData()
                    setLoading(false)
                    showToast()
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const deleteArsip = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/delete_arsip_surat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getData()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const alertDeleteArsip = (id) =>
        Alert.alert(
        "Hapus Arsip Surat",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteArsip(id) }
        ],
        { cancelable: false }
        );

          useEffect(() => {
                getData()
            const interval = setInterval(() => {
                setCount((count) => count + 1);
              }, 1000);
            const unsubscribe = navigation.addListener('focus', () => {
                setCount(0);
                getData()
              });
          
              return () => {
                // Clear setInterval in case of screen unmount
                clearTimeout(interval);
                // Unsubscribe for the focus Listener
                unsubscribe;
              };
        
            
          }, [navigation])

      const showToast = () => {
        ToastAndroid.show("Data berhasil diubah", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            idDesa,
            pengajuan,
            arsip,
            jenis,
            ubahStatusJenisSurat,
            alertDeleteArsip,
            warna
        }}>
            <View style={{flex: 1}}>
                <Konten />
            </View>
        </RootContext.Provider>
    )
}
