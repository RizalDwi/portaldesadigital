import React, {useState, createContext} from 'react'
import { View, Text, ToastAndroid } from 'react-native'
import Form from './../../../container/molecules/form/FormTambahTopografi'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index({route}) {

    const [namaKategori, setNamaKategopri] = useState('')
    const [loading, setLoading] = useState(false)
    const addKategoriTopografi = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    nama_kategori: namaKategori,
                    id_topografi: route.params.idTopografi
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_kategori_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setNamaKategopri('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    const handleChangeInputNamaKategori = (value) => {
        setNamaKategopri(value)
    }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            namaKategori,
            handleChangeInputNamaKategori,
            addKategoriTopografi
        }}>
            <View style={{flex: 1}}>
                <Form />
            </View>
        </RootContext.Provider>
    )
}
