import React, {useEffect, useState, createContext} from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Alert, ToastAndroid } from 'react-native'
import Tabel from './../../../container/molecules/Tabel/TabelKelompokTopografi'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();

export default function index({route, navigation}) {

    const [kelompokTopografi, setKelompokTopografi] = useState([])
    const [count, setCount] = useState(0);
    const [loading, setLoading] = useState(true)

    async function getDataKelompokTopografi() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.itemId
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_kelompok_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                   setKelompokTopografi(res.data.data)
                   setLoading(false)
                    console.log("Kelompok Topografi ->",res)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataKelompokTopografi()
        
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getDataKelompokTopografi();
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus kelompok topografi desa",
        "Anda yakin ingin menghapus data kelompok ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteKelompok(id) }
        ],
        { cancelable: false }
        );

        const showToast = () => {
            ToastAndroid.show("Data berhasil dihapus", ToastAndroid.SHORT);
          };

        const deleteKelompok = async(id) => {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: id
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_kelompok_topografi', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        getDataKelompokTopografi()
                        showToast()
                        
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
          }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            kelompokTopografi,
            alertDelete
        }}>
            <View style={{flex: 1, padding: 20}}>
                <TouchableOpacity onPress={() => navigation.navigate('TambahKelompokTopografi', {itemId: route.params.itemId})}>
                        <View style={styles.button}>
                            <Text style={styles.textButton}>Tambah Kelompok Topografi</Text>
                        </View>
                    </TouchableOpacity>
                <Tabel navigation={navigation} />
            </View>
        </RootContext.Provider>
    )
}

const styles = StyleSheet.create({
        textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
        button: {backgroundColor: '#35BEE0', width: '60%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
  });