import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, Image, Alert, ToastAndroid } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import DropDownPicker from 'react-native-dropdown-picker';
import LoadingScreen from './../../LoadingScreen'
export default function index( {route} ) {

    const [dataJawaban, setDataJawaban] = useState([])
    const [dataPilihan, setDataPilihan] = useState([])
    const [loading, setLoading] = useState(true)
    const [loading2, setLoading2] = useState(false)
    const [warna, setWarna] = useState('')

    async function getData() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_polling: route.params.idPolling,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/detail_jawaban', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataJawaban(res.data.jawaban)
                    setDataPilihan(res.data.pilihan)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function filterJawaban(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_pilihan: id,
                    id_polling: route.params.idPolling
                }

                setLoading2(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/detail_jawaban_filter', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setDataJawaban(res.data.jawaban)
                    setLoading2(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getData()
      }, [])

    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
            <View style={{flex:1}}>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Nama</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.name}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Pilihan</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.pilihan}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Alasan</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.alasan}</Text>
                    </View>
                </View>
               
            </View>

        </View>
            // <View style={{flexDirection: 'row'}}>
            //             <View style={styles.rowNo}>
            //                 <Text style={styles.labelRow}>{index + 1}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.name}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.pilihan}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.alasan}</Text>
            //             </View>
                      
                        
            //         </View>
        )
    }

    if(loading) {
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <View style={{marginBottom: 10}}>
                    <Text style={styles.label}>Filte Jawaban</Text>
                    <DropDownPicker
                        items={dataPilihan}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => filterJawaban(value.value)}
                    />
                    <View></View>
            
                </View>
            
            <ScrollView>
                <View style={{width: '100%'}}>

                    {loading2 ? <LoadingScreen /> : <FlatList data={dataJawaban} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } /> }
                    
                </View>
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10}
})

