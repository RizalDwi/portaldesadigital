import React,{useState, createContext, useEffect} from 'react'
import { View, InteractionManager,ToastAndroid, Alert } from 'react-native'
import Header from './../../../container/molecules/header/HeaderProfile'
import Konten from './../../../container/molecules/konten/admin/KontenKelolaProfilDesa/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({navigation, route}) {

    const [kondisiDesa, setKondisiDesa] = useState([])
    const [luasDesa, setLuasDesa] = useState([])
    const [deskripsiTopogarfi, setDeskripsiTopografi] = useState('')
    const [kategoriTopografi, setKategoriTopografi] = useState([])
    const [sejarah, setSejarah] = useState('')
    const [batas_utara, setBatas_utara] = useState('')
    const [batas_selatan, setBatas_selatan] = useState('')
    const [batas_timur, setBatas_timur] = useState('')
    const [batas_barat, setBatas_barat] = useState('')
    const [count, setCount] = useState(0);
    const [loading, setLoading] = useState(true)
    const [idTopografi, setIdTopografi] = useState(0)
    const [idDesa, setIdDesa] = useState(route.params.idDesa)
    const [warna, setWarna] = useState('')

    async function getDataKondisiDesa() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/profildesa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                   if(res.data.kondisi.length > 0){
                      setKondisiDesa(res.data.kondisi[0])
                      setSejarah(res.data.kondisi[0].sejarah)
                      setBatas_utara(res.data.kondisi[0].batas_utara)
                      setBatas_selatan(res.data.kondisi[0].batas_selatan)
                      setBatas_timur(res.data.kondisi[0].batas_timur)
                      setBatas_barat(res.data.kondisi[0].batas_barat)
                   }
                    setLuasDesa(res.data.luas_desa)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
      InteractionManager.runAfterInteractions(() => {
        getDataKondisiDesa()
      });
        
        console.log(kategoriTopografi)
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            InteractionManager.runAfterInteractions(() => {
              getDataKondisiDesa()
            });
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

      const showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT);
      };

      const alertDelete = (id) =>
        Alert.alert(
        "Hapus luas desa",
        "Anda yakin ingin menghapus data luas desa ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteLuasDesa(id) }
        ],
        { cancelable: false }
        );

        const alertDeleteKategoriTopografi = (id) =>
        Alert.alert(
        "Hapus topografi desa",
        "Anda yakin ingin menghapus data topografi ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => deleteKategoriTopografi(id) }
        ],
        { cancelable: false }
        );

      const updateKondisiDesa = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id: kondisiDesa.id,
                    sejarah: sejarah,
                    batas_utara: batas_utara,
                    batas_selatan: batas_selatan,
                    batas_timur: batas_timur,
                    batas_barat: batas_barat
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_kondisi_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    showToast("Data berhasil diperbaharui")
                    
                })
                .catch((err) => {
                  console.log(err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const updateTopografi = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id: idTopografi,
                    deskripsi: deskripsiTopogarfi
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/update_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    showToast("Data berhasil diperbaharui")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const deleteLuasDesa = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_luas_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    showToast("Data berhasil dihapus")
                    getDataKondisiDesa()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const deleteKategoriTopografi = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_kategori_topografi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataKondisiDesa()
                    showToast("Data berhasil dihapus")
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

      const handleChangeInputSejarah = (value) => {
          setSejarah(value)
      }
      const handleChangeInputBatasUtara = (value) => {
        setBatas_utara(value)
      }
      const handleChangeInputBatasSelatan = (value) => {
        setBatas_selatan(value)
    }
    const handleChangeInputBatasTimur = (value) => {
        setBatas_timur(value)
    }
    const handleChangeInputBatasBarat = (value) => {
        setBatas_barat(value)
    }
    const handleChangeInputDeskripsiTopografi = (value) => {
        setDeskripsiTopografi(value)
    }

    if(loading){
      return <LoadingScreen />
    } else {
        return (
          <RootContext.Provider value={{
              kondisiDesa,
              luasDesa,
              kategoriTopografi,
              deskripsiTopogarfi,
              sejarah,
              batas_utara,
              batas_selatan,
              batas_timur,
              batas_barat,
              updateKondisiDesa,
              handleChangeInputSejarah,
              handleChangeInputBatasTimur,
              handleChangeInputBatasSelatan,
              handleChangeInputBatasUtara,
              handleChangeInputBatasBarat,
              alertDelete,
              updateTopografi,
              handleChangeInputDeskripsiTopografi,
              alertDeleteKategoriTopografi,
              idTopografi,
              idDesa,
              warna
          }}>
              <View style={{flex:1}}>
                  <Konten />
              </View>
          </RootContext.Provider>
      )
    }
}
