import React, {useState, createContext} from 'react'
import { View, Text, ScrollView } from 'react-native'
import Konten from './../../../container/molecules/konten/admin/KontenTambahAparat'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {

    const [nama, setNama] = useState('')
    const [nik, setNik] = useState('')
    const [niap, setNiap] = useState('')
    const [nip, setNip] = useState('')
    const [tempatLahir, setTempatLahir] = useState('')
    const [tanggalLahir, setTanggalLahir] = useState(new Date())
    const [jenisKelamin, setJenisKelamin] = useState('')
    const [pendidikan, setPendidikan] = useState('')
    const [agama, setAgama] = useState('')
    const [pangkat, setPangkat] = useState('')
    const [nomorPengangkatan, setNomorPengangkatan] = useState('')
    const [tanggalPengangkatan, setTanggalPengangkatan] = useState(new Date())
    const [nomorPemberhentian, setNomorPemberhentian] = useState('')
    const [tanggalPemberhentian, setTanggalPemberhentian] = useState(new Date())
    const [masaJabatan, setMasaJabatan] = useState('')
    const [jabatan, setJabatan] = useState('')
    const [status, setStatus] = useState('')
    const [tupoksi, setTupoksi] = useState('')
    const [photo, setPhoto] = useState('')

    const handleInputNama = (value) => {
        setNama(value)
    }
    const handleInputNik = (value) => {
        setNik(value)
    }
    const handleInputNiap = (value) => {
        setNiap(value)
    }
    const handleInputTempatLahir = (value) => {
        setTempatLahir(value)
    }
    const handleInputTanggalLahir = (value) => {
        setTanggalLahir(value)
    }
    const handleInputJenisKelamin = (value) => {
        setJenisKelamin(value)
    }
    const handleInputPendidikan = (value) => {
        setPendidikan(value)
    }
    const handleInputAgama = (value) => {
        setAgama(value)
    }
    const handleInputPangkat = (value) => {
        setPangkat(value)
    }
    const handleInputNomorPengangkatan = (value) => {
        setNomorPengangkatan(value)
    }
    const handleInputTanggalPengangkatan = (value) => {
        setTanggalPengangkatan(value)
    }
    const handleInputNomorPemberhentian = (value) => {
        setNomorPemberhentian(value)
    }
    const handleInputTanggalPemberhentian = (value) => {
        setTanggalPemberhentian(value)
    }
    const handleInputMasaJabatan = (value) => {
        setMasaJabatan(value)
    }
    const handleInputJabatan = (value) => {
        setJabatan(value)
    }
    const handleInputStatus = (value) => {
        setStatus(value)
    }
    const handleInputTupoksi = (value) => {
        setTupoksi(value)
    }
    const handleInputPhoto = (value) => {
        setPhoto(value)
    }
    const handleInputNip = (value) => {
        setNip(value)
    }

    const addAparat = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data2 = {
                    id_desa: route.params.idDesa,
                    nama: nama,
                    nik: nik,
                    nip: nip,
                    niap: niap,
                    tempat_lahir: tempatLahir,
                    tanggal_lahir: tanggalLahir,
                    jenis_kelamin: jenisKelamin.value,
                    pendidikan: pendidikan.value,
                    agama: agama.value,
                    pangkat_atau_golongan: pangkat,
                    nomor_sk_pengangkatan: nomorPengangkatan,
                    tanggal_sk_pengangkatan: tanggalPengangkatan,
                    nomor_sk_pemberhentian: nomorPemberhentian,
                    tanggal_sk_pemberhentian: tanggalPemberhentian,
                    masa_jabatan: masaJabatan,
                    jabatan: jabatan,
                    status: status.value,
                    tupoksi: tupoksi,
                    foto: photo
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_aparat', data2, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    console.log("Data 2 -> ", data2)
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data2)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    return (
        <RootContext.Provider value={{
            nama,
            nik,
            niap,
            tempatLahir,
            tanggalLahir,
            jenisKelamin,
            pendidikan,
            agama,
            pangkat,
            nomorPengangkatan,
            tanggalPengangkatan,
            nomorPemberhentian,
            tanggalPemberhentian,
            masaJabatan,
            jabatan,
            status,
            tupoksi,
            photo,
            nip,
            handleInputNama,
            handleInputNik,
            handleInputNiap,
            handleInputTempatLahir,
            handleInputTanggalLahir,
            handleInputJenisKelamin,
            handleInputPendidikan,
            handleInputAgama,
            handleInputPangkat,
            handleInputNomorPengangkatan,
            handleInputTanggalPengangkatan,
            handleInputNomorPemberhentian,
            handleInputTanggalPemberhentian,
            handleInputMasaJabatan,
            handleInputJabatan,
            handleInputStatus,
            handleInputTupoksi,
            handleInputPhoto,
            handleInputNip,
            addAparat
        }}>
            <View style={{flex: 1, padding: 20}}>
                <ScrollView>
                    <Konten />
                </ScrollView>
            </View>
        </RootContext.Provider>
    )
}
