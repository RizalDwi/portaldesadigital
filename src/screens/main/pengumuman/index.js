import React, {useEffect, useState} from 'react'
import { View, InteractionManager } from 'react-native'
import HeaderPengumuman from '../../../container/molecules/header/HeaderPengumuman'
import KontenPengumuman from '../../../container/molecules/konten/KontenPengumuman'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation}) {

    const [dataPengumuman, setDataPengumuman] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);

    async function getDataPengumuman() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    id_users: id_user
                }
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/display_beranda_pengumuman',data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataPengumuman(res.data.data)
                    
                    setLoading(false)
                
                })
                .catch((err) => {
                    setLoading(false)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataPengumuman()
        InteractionManager.runAfterInteractions(() => getDataPengumuman);
        
    },[])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
            <HeaderPengumuman />
            <KontenPengumuman navigation={navigation} dataPengumuman={dataPengumuman} />
            
        </View>
    )
}
