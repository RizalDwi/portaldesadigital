import React, {useEffect, useState} from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Konten from './../../../container/molecules/konten/KontenArtikel'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation, route}) {

    const [namaDesa, setNamaDesa] = useState('')
    const [foto, setFoto] = useState('')
    const [judul, setJudul] = useState('')
    const [isi, setIsi] = useState('')
    const [fotoArtikel, setFotoArtikel] = useState('')
    const [loading, setLoading] = useState(true)
    const [tanggal, setTanggal] = useState('')
    const [count, setCount] = useState(0);

    async function getDataBerita() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{
                let data = {
                    id: route.params.id
                }
              if (result){
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/artikel_berita',data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    
                    setNamaDesa(res.data.data[0].nama_desa)
                    setFoto(res.data.data[0].foto_desa)
                    setJudul(res.data.data[0].judul)
                    setIsi(res.data.data[0].isi)
                    setFotoArtikel(res.data.data[0].foto)
                    setTanggal(res.data.data[0].created_at)
                    setLoading(false)
                
                })
                .catch((err) => {
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataBerita()
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            getDataBerita()
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
        
    },[navigation])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <Konten namaDesa={namaDesa} foto={foto} judul={judul} isi={isi} fotoArtikel={fotoArtikel} tanggal={tanggal} />
        </View>
    )
}
