import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid, Modal, TouchableHighlight } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';

export default function index(props) {

    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(true)
    const [loading2, setLoading2] = useState(false)
    const [loading3, setLoading3] = useState(false)
    const [loading4, setLoading4] = useState(false)
    const [foto, setFoto] = useState('')
    const [id, setId] = useState(0)
    const [warna, setWarna] = useState('')
    const [listProvinsi, setListProvinsi]= useState([])
    const [listKota, setListKota] = useState([])
    const [listKecamatan, setListKecamatan] = useState([])
    const [listDesa, setListDesa] = useState([])

    const [kode, setKode] = useState('')
    
    const [provinsi, setProvinsi] = useState('')
    const [kabupaten, setKabupaten] = useState('')
    const [kecamatan, setKecamatan] = useState('')
    const [desa, setDesa] = useState('')

    const SelectImageFromGaleery = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true
          }).then(image => {
            setModalVisible(!modalVisible);
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    const selectImageFromCamera = () => {
        setModalVisible(!modalVisible);
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
          }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }
    

    async function createDesa() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_user: id_user,
                    provinsi: provinsi,
                    kabupaten: kabupaten,
                    kecamatan: kecamatan,
                    desa: desa,
                    warna_banner: warna,
                    foto: foto,
                    kode: kode
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/create_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    Asynstorage.setItem('admin', '1')
                    props.handleAdmin('1')
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: 'homeLanding' }]
                    })
                
                })
                .catch((err) => {
                    console.log(err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
      getListProvinsi()
      }, [])

      async function getListProvinsi() {
        Axios.get('https://dev.farizdotid.com/api/daerahindonesia/provinsi', {
                    timeout: 20000,
                })
                .then((res) => {
                    console.log(res)
                    res.data.provinsi.map((obj, i) => (
                        setListProvinsi(listProvinsi => [...listProvinsi, {label: obj.nama, value: obj.id }])    
                    ))
                    setLoading(false)
                })
                .catch((err) => {
                    alert(err)
                })
    }

    async function getListKota(id, tempat) {
        setProvinsi(tempat)
        setListKota([])
        setLoading2(true)
        Axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${id}`, {
                    timeout: 20000,
                })
                .then((res) => {
                    console.log(res)
                    res.data.kota_kabupaten.map((obj, i) => (
                        setListKota(listKota => [...listKota, {label: obj.nama, value: obj.id }])    
                    ))
                    setLoading2(false)
                })
                .catch((err) => {
                    alert(err)
                })
    }
    async function getListKecamatan(id, tempat) {
        setKabupaten(tempat)
        setListKecamatan([])
        setLoading3(true)
        Axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${id}`, {
                    timeout: 20000,
                })
                .then((res) => {
                    console.log(res)
                    res.data.kecamatan.map((obj, i) => (
                        setListKecamatan(listKecamatan => [...listKecamatan, {label: obj.nama, value: obj.id }])    
                    ))
                    setLoading3(false)
                })
                .catch((err) => {
                    alert(err)
                })
    }
    async function getListDesa(id, tempat) {
        setKecamatan(tempat)
        setListDesa([])
        setLoading4(true)
        Axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${id}`, {
                    timeout: 20000,
                })
                .then((res) => {
                    console.log(res)
                    res.data.kelurahan.map((obj, i) => (
                        setListDesa(listDesa => [...listDesa, {label: obj.nama, value: obj.id }])    
                    ))
                    setLoading4(false)
                })
                .catch((err) => {
                    alert(err)
                })
    }


    var radio_props = [
        {label: 'param1', value: '4CC9F0' },
        {label: 'param2', value: 'F72585' },
        {label: 'param3', value: '3A0CA3' },
        {label: 'param4', value: '53A687' },
        {label: 'param5', value: 'FFA700' },
        {label: 'param6', value: 'F74E2E' },
        {label: 'param7', value: '495061' }
        
      ];

    const showToast = () => {
        ToastAndroid.show("Data berhasil diubah", ToastAndroid.SHORT);
      };

  

    return (
        <View style={{padding: 20, flex: 1}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centeredView}>
                <View style={styles.modalView}>

                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            selectImageFromCamera();
                        }}>
                            <Text style={styles.textStyle}>Ambil Dari Camera</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            SelectImageFromGaleery();
                        }}>
                            <Text style={styles.textStyle}>Ambil Dari Galeri</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            setModalVisible(!modalVisible);
                        }}
                        >
                        <Text style={styles.textStyle}>Batal</Text>
                    </TouchableHighlight>
                </View>
                </View>
            </Modal>
            <Text style={styles.label}>Formulir Pembuatan Portal Desa</Text>
            <ScrollView style={{marginTop: 10}}>
                
                <Text style={styles.labelIsi}>Alamat Desa</Text>
                <View style={{marginLeft: 30}}>
                        
                        <Text style={styles.labelIsi}>Provinsi</Text>
                        {(() => {
                            if (loading){
                                return <Image source={require('./../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={listProvinsi}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => getListKota(value.value, value.label)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                        
            
                </View>
                <View style={{marginLeft: 30}}>
                        <Text style={styles.labelIsi}>Kabupaten / Kota</Text>
                        {(() => {
                            if (loading2){
                                return <Image source={require('./../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={listKota}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => getListKecamatan(value.value, value.label)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                </View>
                <View style={{marginLeft: 30}}>
                    
                    <Text style={styles.labelIsi}>Kecamatan</Text>
                    {(() => {
                            if (loading3){
                                return <Image source={require('./../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={listKecamatan}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={200}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => getListDesa(value.value, value.label)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                        
                </View>
                <View style={{marginLeft: 30}}>
                    
                    <Text style={styles.labelIsi}>Desa / Kelurahan</Text>
                    {(() => {
                            if (loading4){
                                return <Image source={require('./../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={listDesa}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={200}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => setDesa(value.label)}
                                    
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                        
                </View>
                <View style={{marginTop: 10}}>
                    <Text style={styles.labelIsi}>Kode Desa</Text>
                    <TextInput placeholder="Kode desa" style={styles.form} onChangeText={(value) => setKode(value)} />
                </View>
                <View style={{marginTop: 10}}>
                <Text style={styles.labelIsi}>Pilih Warna Banner</Text>
                <RadioForm
                    formHorizontal={true}
                    animation={true}
                    >
                    {
                        radio_props.map((obj, i) => (
                        <RadioButton labelHorizontal={true} key={i} >
                            {/*  You can set RadioButtonLabel before RadioButtonInput */}
                            <RadioButtonInput
                            obj={obj}
                            index={i}
                            isSelected={() => setId(i)}
                            borderWidth={1}
                            buttonInnerColor={`#${obj.value}`}
                            buttonOuterColor={[]}
                            buttonSize={20}
                            buttonOuterSize={23}
                            buttonStyle={{}}
                            buttonWrapStyle={{marginLeft: 10}}
                            onPress={() => setWarna(obj.value)}
                            />
                        </RadioButton>
                        ))
                    } 
                    </RadioForm>
                </View>
                <View style={{marginTop: 10}}>
                    <Text style={styles.labelIsi}>Warna Yang Dipilih</Text>
                    <View style={{height: 40, backgroundColor: `#${warna}`}}>

                    </View>
                </View>
                <View>
                    <View>
                        <Text style={styles.label}>Foto Desa</Text>
                        <View style={{flex: 1, alignItems: 'center'}}>
                            <TouchableOpacity onPress={() => {
                                                            setModalVisible(true);
                                                            }}>
                                <View style={{width: 190, height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center', borderRadius: 25}}>
                                    <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 15, textAlign: 'center'}}>Upload Gambar</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{marginVertical: 10, paddingHorizontal: '10%', alignItems: 'center'}}>
                        <Image source={foto === '' ? require('./../../../assets/icons/thumnail.png') : {uri: `data:image/png;base64,${foto}`}} style={{width: 400, height: 300}} />
                    </View>
                </View>
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => createDesa()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelIsi : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      centeredView: {
        flex: 1,
        marginTop: 22
      },
      modalView: {
        width: '100%',
        height: 200,
        position: 'absolute',
        bottom: 0,
        backgroundColor: "white",
        padding: 35,
        alignItems: "center",
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        textAlign: "center",
        fontFamily: 'Poppins-Bold'
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
})
