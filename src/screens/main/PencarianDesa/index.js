import React, {useEffect, useState} from 'react'
import { View, InteractionManager } from 'react-native'
import HeaderLanding from '../../../container/molecules/header/HeaderPencarianDesa'
import KontenLanding from '../../../container/molecules/konten/KontenPencarianDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index(props) {

    const [loading, setLoading] = useState(false)
    //const [desa, setDesa] = useState('')
    const [listDesa, setListDesa] = useState([])

    async function getData(desa) {
        props.setWarna('')
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    desa: desa
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/search_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                   setListDesa(res.data.data)
                   setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const navigationDesa = (id) => {
        props.setIdDesa(id)
        props.navigation.navigate('Desa')
    }

    useEffect(() => {
       InteractionManager.runAfterInteractions(() => getData());
    }, [])

    return (
        <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
            <HeaderLanding getData={getData} />
            <KontenLanding loading={loading} listDesa={listDesa} navigationDesa={navigationDesa} />
            
        </View>
    )
}
