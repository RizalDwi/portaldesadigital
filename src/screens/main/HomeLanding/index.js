import React, {useEffect, useState} from 'react'
import { View, Text, TouchableOpacity, Button, InteractionManager} from 'react-native'
import HeaderLanding from '../../../container/molecules/header/HeaderLanding'
import KontenLanding from '../../../container/molecules/konten/KontenLanding'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({navigation}) {

    const [dataBerita, setDataBerita] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);

    async function getDataBerita() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    id_users: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/display_beranda_berita',data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataBerita(res.data.data)
                    setLoading(false)
                
                })
                .catch((err) => {
                    setLoading(false)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    useEffect(() => {
        InteractionManager.runAfterInteractions(() => getDataBerita());
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            InteractionManager.runAfterInteractions(() => getDataBerita());
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
        
    },[navigation])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
            <HeaderLanding navigation={navigation} />
            <KontenLanding navigation={navigation} dataBerita={dataBerita} />
           
        </View>
    )
}
