import React, {useEffect, useState} from 'react'
import { View, InteractionManager } from 'react-native'
import HeaderLanding from '../../../container/molecules/header/HeaderProdukWarga'
import KontenLanding from '../../../container/molecules/konten/KontenProdukWarga'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation}) {

    const [listProduk, setListProduk] = useState([])
    const [count, setCount] = useState(0);
    const [loading, setLoading] = useState(true)

    async function getDataKonten() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_users: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/produk_beranda', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setListProduk(res.data.data)
                    setLoading(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataKonten()
        InteractionManager.runAfterInteractions(() => getDataKonten())
        
      }, [])

      if(loading){
        return <LoadingScreen />
      }
    

    return (
        <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
            <HeaderLanding />
            <KontenLanding listProduk={listProduk} navigation={navigation} />
        </View>
    )
}
