import React, {useEffect, useState} from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'

export default function index({ navigation }) {

    const onLogoutPress = async() => {
        try {
            var token = await Asynstorage.getItem('api_token')
            console.log(token)
            var id = await Asynstorage.getItem('id_user')
            await Asynstorage.getItem('api_token', (error, result) =>{
              if (result){

                let data = {
                    id: id
                }

                console.log(data)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/logout', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    if(res.data.succes){
                        Asynstorage.removeItem('api_token')
                        Asynstorage.removeItem('id_user')
                        Asynstorage.removeItem('username')
                        console.log('Remove Token')
                        console.log(res)

                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }]
                        })
                    } else {
                        alert('Logout gagal')
                    }
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })

            console.log(id)

            
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
      }

    return (
        <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
            <View style={{flex: 1, padding: 20}}>
                <TouchableOpacity onPress={() => onLogoutPress()}>
                <View style={{backgroundColor: '#3C82FF', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{fontFamily: 'SFProDisplay', color: '#FFFFFF', fontSize: 19}}>
                        Logout
                    </Text>
                </View>
            </TouchableOpacity>
            </View>
            
        </View>
    )
}
