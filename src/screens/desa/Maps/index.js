import React, {useState, useEffect} from 'react'
import { View, StyleSheet, InteractionManager} from 'react-native'
import MapView,{Marker, Polygon} from 'react-native-maps'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../screens/LoadingScreen'

export default function index({route, navigation}) {

    const [border, setBorder] = useState([])
    const [loading, setLoading] = useState(true)
    const [modalVisible, setModalVisible] = useState(false);
    const [listCoordinate, setListCoordinate] = useState([])
    const [count, setCount] = useState(0);
    const [latitudeDesa, setLatitudeDesa] = useState(0)
    const [longitudeDesa, setLongtitudeDesa] = useState(0)

    const [position, setPosition] = useState({
        latitude: latitudeDesa,
        longitude: longitudeDesa,
        latitudeDelta: 0.06,
        longitudeDelta: 0.06
      });

      const [margin, setMargin] = useState(1)

      async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_border_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setBorder(res.data.data)
                    console.log(res.data.data[0].latitude)
                    console.log(res.data.data[0].longitude)
                    setLatitudeDesa(res.data.data[0].latitude)
                    setLongtitudeDesa(res.data.data[0].longitude)
                    setListCoordinate(res.data.marker)
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    useEffect(() => {
      InteractionManager.runAfterInteractions(() => {
        getData()
        });
      
    }, [])


    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1}}>
            <MapView style={{flex:1, marginBottom: margin}}
                initialRegion={{
                    latitude: latitudeDesa,
                    longitude: longitudeDesa,
                    latitudeDelta: 0.06,
                    longitudeDelta: 0.06
                }}
                onMapReady={() => setMargin(0)}
                showsUserLocation={true}
                showsMyLocationButton={true}
                onPress={() => console.log("Latitude : " + latitudeDesa +" longtitude " + longitudeDesa)}
                >
              
            <Polygon
                coordinates={border}
                strokeColor="#000" // fallback for when `strokeColors` is not supported by the map-provider
                strokeColors={[
                    '#7F0000',
                    '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                    '#B24112',
                    '#E5845C',
                    '#238C23',
                    '#7F0000'
                ]}
                strokeWidth={2}
        />
        {listCoordinate.map((item, index) => {
                    var source = ''
                    if (item.icon == 'balaidesa'){
                        source = require('./../../../assets/marker/balaidesa.png')
                    } else if(item.icon == 'gereja'){
                        source = require('./../../../assets/marker/gereja.png')
                    } else if(item.icon == 'masjid'){
                        source = require('./../../../assets/marker/masjid.png')
                    } else if(item.icon == 'monumen'){
                        source = require('./../../../assets/marker/monumen.png')
                    } else if(item.icon == 'parlemen'){
                        source = require('./../../../assets/marker/parlemen.png')
                    } else if(item.icon == 'rumah'){
                        source = require('./../../../assets/marker/rumah.png')
                    } else if(item.icon == 'sekolah'){
                        source = require('./../../../assets/marker/sekolah.png')
                    }
                    return (
                        <Marker key={item.id} coordinate={{
                            latitude: parseFloat(item.latitude),
                            longitude: parseFloat(item.longitude)
                        }}
                        icon={source}
                        title={item.judul}
                        description={item.deskripsi}
                        />
                    )
                })}
            </MapView>
            
        </View>
    )
}
const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
      },
      modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '90%',
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width: 80
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "justify"
      },
      form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 14
    }
})
