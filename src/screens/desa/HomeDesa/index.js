import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, InteractionManager } from 'react-native'
import HeaderDesa from '../../../container/molecules/header/HeaderDesa'
import MenuDesa from '../../../container/molecules/MenuDesa'
import Pengumuman from '../../../container/molecules/Pengumuman'
import PollingDesa from '../../../container/molecules/PollingDesa'
import BeritaDesa from '../../../container/molecules/BeritaDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'


export default function index(props) {

    const [dataBerita, setDataBerita] = useState([])
    const [dataPolling, setDataPolling] = useState([])
    const [dataPengumuman, setDataPengumuman] = useState([])
    const [loading, setLoading] = useState(false)
    const [followers, setFollowers] = useState('')
    const [cekFollow, setCekFollow] = useState(false)
    const [warna, setWarna] = useState('')
    const [namaDesa, setNamaDesa] = useState('')
    const [foto, setFoto] = useState('')
    const [alamat, setAlamat] = useState('')

    const handleDataPolling = (value) => {
        setDataPolling(value)
    }

    async function getDataKonten() {
        props.setWarna('')
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: props.idDesa,
                    id_users: id_user,
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_konten_beranda_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataBerita(res.data.berita)
                    setDataPengumuman(res.data.pengumuman)
                    setDataPolling(res.data.polling)
                    setFollowers(res.data.followers)
                    setCekFollow(res.data.cekFollow)
                    props.setWarna(res.data.desa[0].warna_banner)
                    setNamaDesa(res.data.desa[0].nama_desa)
                    Asynstorage.setItem('nama_desa', res.data.desa[0].nama_desa)
                    Asynstorage.setItem('alamat_desa', "Kab. "+res.data.desa[0].kabupaten+", "+res.data.desa[0].provinsi)
                    Asynstorage.setItem('foto_desa', res.data.desa[0].foto)
                    setAlamat("Kab. "+res.data.desa[0].kabupaten+", "+res.data.desa[0].provinsi)
                    setFoto(res.data.desa[0].foto)
                    setWarna(res.data.desa[0].warna_banner)
                    Asynstorage.setItem('warna_desa', res.data.desa[0].warna_banner)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataKonten()
        InteractionManager.runAfterInteractions(() => getDataKonten())
      }, [])

      async function followDesa() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: props.idDesa,
                    id_users: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/follow_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setFollowers(res.data.followers)
                    setCekFollow(res.data.cekFollow)
                    console.log("Polling -> ", res)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    
    if(loading)
    {
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1}}>
            <ScrollView style={{flex: 1, backgroundColor: '#F5F5F5'}}>
                <HeaderDesa followers={followers} cekFollow={cekFollow} followDesa={followDesa} warna={warna} namaDesa={namaDesa} foto={foto} />
                <MenuDesa navigation={props.navigation} idDesa={props.idDesa} warna={warna} />
                <Pengumuman dataPengumuman={dataPengumuman} navigation={props.navigation} />
                <View style={{width: '100%', height: 400, padding: 10}}>
                    <Text style={{fontSize: 24, fontFamily: 'Rubik-Bold', color: '#696969'}}>
                        Polling
                    </Text>
                    <View style={{marginTop: 10}}>
                        <ScrollView horizontal={true}>
                        {dataPolling.map((item, index) => (
                            <View key={index}>
                                <PollingDesa dataPolling={item} handleDataPolling={handleDataPolling} idDesa={props.idDesa} lokasi="desa" />
                            </View>
                            ))}
                        </ScrollView>
                    </View>
                    
                </View>
                
                <BeritaDesa dataBerita={dataBerita} navigation={props.navigation} />
            </ScrollView>
        </View>
    )
}
