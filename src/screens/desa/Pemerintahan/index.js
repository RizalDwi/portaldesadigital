import React, {useEffect, useState, createContext} from 'react'
import { View, Text, ScrollView, StyleSheet, Modal, Pressable,InteractionManager } from 'react-native'
import Konten from '../../../container/molecules/konten/KontenPemerinthan/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {
    const [visi, setVisi] = useState('')
    const [misi, setMisi] = useState([])
    const [aparat, setAparat] = useState([])
    const [loading, setLoading] = useState(true)
    const [modalVisible, setModalVisible] = useState(false);
    const [tupoksi, setTupoksi] = useState('')

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_konten_pemerintahan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                  if(res.data.visi.length > 0){
                    setVisi(res.data.visi[0].visi)
                  }
                   
                   setMisi(res.data.misi)
                   setAparat(res.data.aparat)
                   setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
      InteractionManager.runAfterInteractions(() => {
        getData()
        });
      
    }, [])

    const ModalTupoksi = (value) => {
        setTupoksi(value)
        setModalVisible(true)
        console.log("test")
        
      }

    if(loading){
        return <LoadingScreen />
    }
    return (
        <RootContext.Provider value={{
            misi,
            visi,
            aparat,
            ModalTupoksi
        }}>
            <View style={{flex:1}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>{tupoksi}</Text>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalVisible(!modalVisible)}
                  >
                    <Text style={styles.textStyle}>Tutup</Text>
                  </Pressable>
                </View>
              </View>
            </Modal>
                <Konten />
        </View>
        </RootContext.Provider>
    )
}

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22,
    },
    modalView: {
      backgroundColor: "white",
      borderRadius: 20,
      padding: 20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
      width: '90%',
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      width: 80
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "justify"
    }
  });