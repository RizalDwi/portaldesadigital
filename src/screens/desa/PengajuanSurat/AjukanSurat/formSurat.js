import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ToastAndroid, Button } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../LoadingScreen'
import { ScrollView } from 'react-native-gesture-handler'
import { useForm, Controller } from "react-hook-form";
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import RNFetchBlob from 'rn-fetch-blob'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';

export default function index({route}) {

    const [listData, setListData] = useState([])
    const [loading, setLoading] = useState(true)
    const [nama, setNama] = useState('')
    const [listInput, setListInput] = useState([])
    const { control, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => tambah(data);
    const [listPersyaratan, setListPersyaratan] = useState([])


    const tambah = async(data) => {
        //console.log(route.params.id_desa)
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                
                data['id_format'] = route.params.id_format
                data['id_user'] = id_user

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/ajuan-surat/store', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Request->",res)
                    // setNama('')
                    setLoading(false)
                    // showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function getData() {
        
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_format: route.params.id_format,
                    id_user: id_user
                }
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/kode-isian/form-surat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setListInput(res.data.data)
                    setListPersyaratan(res.data.persyaratan)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getData()
  }, [])

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen/>
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>

            <View style={{alignItems: 'center'}}>
                    <Text style={styles.labelDokumen}>Persyaratan</Text>
                </View>
                <View style={{marginBottom: 10}}>

                {listPersyaratan.map((prop, key) => {
                    var nName = "dokumen_" + key

                    console.log("prop",prop)

                    return (
                        // <Button style={{borderColor: prop[0]}}  key={key}>{prop[1]}</Button>
                        <View>
                            <Text style={styles.label}>{prop.persyaratan.nama}</Text>
                            <Controller
                                control={control}
                                rules={{
                                    required: true,
                                    }}
                                render={({ field: { onChange, onBlur, value } }) => (
                                    <DropDownPicker
                                        items={prop.dokumen}
                                        onBlur={onBlur}
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                                        value={value}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => onChange(value.value)}
                                    />
                                   
                                )}
                                name={nName}
                                defaultValue=""
                            />
                             <View></View>
                        </View>
                    );
                })}
                </View>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.labelDokumen}>Data Surat</Text>
                </View>
            
            <View style={{marginBottom: 10}}>
            
            {listInput.map((prop, key) => {

                    if(prop.slug != "nomor_surat" && prop.slug != "nama" && prop.slug != "no_ktp" && prop.slug != "tanggal_lahir" && prop.slug != "nama_pamong" && prop.slug != "jabatan" && prop.slug != "nip" && prop.slug != "ttd" && 
                    prop.slug != "tanggal_surat" && prop.slug != "no_kk" && prop.slug != "tanggal_mulai" && prop.slug != "tanggal_akhir"){
                        if(prop.slug == "agama"){
                            return (
                            <View>
                                <Text style={styles.label}>{prop.nama}</Text>
                                <Controller
                                    control={control}
                                    rules={{
                                        required: true,
                                        }}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <DropDownPicker
                                            items={[
                                                {label: 'Islam', value: 'Islam'},
                                                {label: 'Katholik', value: 'Katholik'},
                                                {label: 'Protestan', value: 'Protestan'},
                                                {label: 'Budha', value: 'Budha'},
                                                {label: 'Hindu', value: 'Hindu'},
                                                {label: 'Kong hu chu', value: 'Kong hu chu'}

                                            ]}
                                            onBlur={onBlur}
                                            containerStyle={{ height: 40 }}
                                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                                            dropDownMaxHeight={300}
                                            labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                                            value={value}
                                            itemStyle={{
                                                justifyContent: 'flex-start'
                                            }}
                                            onChangeItem={(value) => onChange(value.value)}
                                        />
                                    
                                    )}
                                    name={prop.slug}
                                    defaultValue=""
                                />
                                <View></View>
                            </View>
                            )
                        } else if(prop.slug == "sex"){
                            return (
                            <View>
                                <Text style={styles.label}>{prop.nama}</Text>
                                <Controller
                                    control={control}
                                    rules={{
                                        required: true,
                                        }}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <DropDownPicker
                                            items={[
                                                {label: 'Pria', value: 'Pria'},
                                                {label: 'Wanita', value: 'Wanita'}

                                            ]}
                                            onBlur={onBlur}
                                            containerStyle={{ height: 40 }}
                                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                                            dropDownMaxHeight={300}
                                            labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                                            value={value}
                                            itemStyle={{
                                                justifyContent: 'flex-start'
                                            }}
                                            onChangeItem={(value) => onChange(value.value)}
                                        />
                                    
                                    )}
                                    name={prop.slug}
                                    defaultValue=""
                                />
                                <View></View>
                            </View>
                            )
                        } else if(prop.slug == "status"){
                            return (
                            <View>
                                <Text style={styles.label}>{prop.nama}</Text>
                                <Controller
                                    control={control}
                                    rules={{
                                        required: true,
                                        }}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <DropDownPicker
                                            items={[
                                                {label: 'Lajang', value: 'Lajang'},
                                                {label: 'Sudah Menikah', value: 'Sudah Menikah'}

                                            ]}
                                            onBlur={onBlur}
                                            containerStyle={{ height: 40 }}
                                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                                            dropDownMaxHeight={300}
                                            labelStyle={{ fontSize: 14, fontFamily: 'Poppins', color: '#394761' }}
                                            value={value}
                                            itemStyle={{
                                                justifyContent: 'flex-start'
                                            }}
                                            onChangeItem={(value) => onChange(value.value)}
                                        />
                                    
                                    )}
                                    name={prop.slug}
                                    defaultValue=""
                                />
                                <View></View>
                            </View>
                            )
                        }
                        else if(prop.slug == "kepentingan"){
                            return (
                            <View>
                                <Text style={styles.label}>{prop.nama}</Text>
                                <Controller
                                    control={control}
                                    rules={{
                                        required: true,
                                        }}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <Textarea
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            containerStyle={styles.textareaContainer}
                                            style={styles.textarea}
                                            placeholder={'Tulis Berita'}
                                            placeholderTextColor={'#c7c7c7'}
                                            underlineColorAndroid={'transparent'}
                                            value={value}
                                            placeholder={prop.nama}
                                            onChangeText={onChange}
                                        />
                                    
                                    )}
                                    name={prop.slug}
                                    defaultValue=""
                                />
                                <View></View>
                            </View>
                            )
                        }
                         else {
                            return (
                                // <Button style={{borderColor: prop[0]}}  key={key}>{prop[1]}</Button>
                                <View>
                                    <Text style={styles.label}>{prop.nama}</Text>
                                    <Controller
                                        control={control}
                                        rules={{
                                        required: true,
                                        }}
                                        render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={styles.form}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder={prop.nama}
                                        />
                                        )}
                                        name={prop.slug}
                                        defaultValue=""
                                    />
                                </View>
                            );
                        }
                        
                    }
                    
                })}

            </View>
                
               {/* <TouchableOpacity style={{width: '100%'}} onPress={() => Downnload()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity> */}
              <Button title="Submit" onPress={handleSubmit(onSubmit)} />
            </ScrollView>
            
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        formDokumen: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelDokumen : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 25
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      }
})