import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'


export default function index({route, navigation}) {
    return (
        <View style={{flex: 1, padding: 10}}>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('AjukanSurat', {warna: route.params.warna, id_desa: route.params.idDesa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Buat Surat</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('DokumenPersyaratan', {warna: route.params.warna, id_desa: route.params.idDesa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Dokumen Persyaratan</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('StatusPengajuan', {warna: route.params.warna, id_desa: route.params.idDesa})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Status Pengajuan</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => navigation.navigate('ArsipSurat', {warna: route.params.warna})}>
                    <View style={{backgroundColor: `#${route.params.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Arsip</Text>
                    </View>
                </TouchableOpacity>
            </View>
           
        </View>
    )
}
