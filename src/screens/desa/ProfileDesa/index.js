import React, {useState, createContext, useEffect} from 'react'
import { View, InteractionManager } from 'react-native'
import Konten from '../../../container/molecules/konten/KontenProfileDesa/navigation'
import HeaderProfile from '../../../container/molecules/header/HeaderProfile'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {

    const [dataKondisi, setDataKondisi] = useState([])
    const [dataLuas, setDataLuas] = useState([])
    const [dataTopografi, setDataTopografi] = useState([])
    const [deskripsi, setDeskripsi] = useState('')
    const [loading, setLoading] = useState(true)
    const [luasTotal, setLuasTotal] = useState(0)

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_data_profil_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                   if(res.data.kondisi.length){
                        setDataKondisi(res.data.kondisi)
                   }
                   if(res.data.luas_desa.length > 0){
                        setDataLuas(res.data.luas_desa[0].detail_luas)
                        setLuasTotal(res.data.luas_desa[0].luas_total)
                   }
                   if(res.data.deskripsi.length > 0){
                        setDeskripsi(res.data.deskripsi)
                   }
                   setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
          getData()
          });
        
      }, [])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            dataKondisi,
            dataLuas,
            deskripsi,
            luasTotal
        }}>
            <View style={{flex:1}}>
                <HeaderProfile />
                <Konten />
            </View>
        </RootContext.Provider>
    )
}
