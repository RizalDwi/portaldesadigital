import React, {useEffect, useState} from 'react'
import { View, Text } from 'react-native'
import Header from './../../../container/molecules/header/HeaderProfile'
import MenuTahun from './../../../container/molecules/konten/KontenDanaDesa/MenuTahun'
import Asynstorage from '@react-native-community/async-storage'

export default function index( {navigation, route} ) {

    const [warna, setWarna] = useState('')
    
    const getWarna = async() => {
        setWarna(await Asynstorage.getItem('warna_desa'))
        console.log("Warna ->", await Asynstorage.getItem('warna_desa'))
    }

    useEffect(() => {
        getWarna()
     }, [])

    return (
        <View style={{flex: 1}}>
            <Header />
            <MenuTahun navigation={navigation} idDesa={route.params.idDesa} warna={warna} />
        </View>
    )
}
