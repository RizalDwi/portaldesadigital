import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import Konten from './../../../container/molecules/konten/KontenPengajuanWarga'

export default function index({route}) {
    
    return (
        <View style={{flex: 1}}>
            <ScrollView>
                <Konten idDesa = {route.params.idDesa} />
            </ScrollView>
        </View>
    )
}
