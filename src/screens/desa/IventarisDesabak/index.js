import React, {useEffect, useState, createContext} from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'
import Konten from '../../../container/molecules/konten/KontenIventaris/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export const RootContext = createContext();
export default function index({route}) {

    const [aset, setAset] = useState([])
    const [bangunan, setBangunan] = useState([])
    const [elektronik, setElektronik] = useState([])
    const [jaringan, setJaringan] = useState([])
    const [konstruksi, setKonstruksi] = useState([])
    const [mesin, setMesin] = useState([])
    const [tanah, setTanah] = useState([])
    const [loading, setLoading] = useState(true)

    async function getDataKonten() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_iventaris', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setAset(res.data.aset)
                    setBangunan(res.data.bangunan)
                    setElektronik(res.data.elektronik)
                    setJaringan(res.data.jaringan)
                    setKonstruksi(res.data.konstruksi)
                    setMesin(res.data.mesin)
                    setTanah(res.data.tanah)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataKonten()
      }, [])
    
    if(loading)
    {
        return <LoadingScreen />
    }

    return (
        <RootContext.Provider value={{
            aset,
            bangunan,
            jaringan,
            konstruksi,
            tanah,
            mesin,
            elektronik
        }}>
            <View style={{flex:1}}>
                <Konten/>
            </View>
        </RootContext.Provider>
    )

}

