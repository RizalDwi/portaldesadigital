import React, { useState, useEffect } from 'react'
import { View, InteractionManager } from 'react-native'
import Header from './../../../container/molecules/header/HeaderProfile'
import Konten from '../../../container/molecules/konten/KontenInformasiDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {

    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true)
    const [warna, setWarna] = useState('')

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        setWarna(await Asynstorage.getItem('warna_desa'))
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/list_informasi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setData(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
          getData()
          });
        
      }, [])
    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1}}>
            <Header />
            <Konten navigation={navigation} data={data} warna={warna} />
        </View>
    )
}
