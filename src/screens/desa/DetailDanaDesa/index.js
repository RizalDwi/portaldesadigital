import React, {useEffect, useState} from 'react'
import { View, InteractionManager } from 'react-native'
import Konten from './../../../container/molecules/konten/KontenDanaDesa/DetailTahun'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'

export default function index({navigation, route}) {
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [dataAnggaran, setDataAnggaran] = useState([])
    const [anggaran, setAnggaran] = useState('')
    const [realisasi, setRealisasi] = useState('')
    const [saldo, setSaldo] = useState('')
    const [warna, setWarna] = useState('')

    async function getDataKonten() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    tahun: route.params.tahun
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/rincian_dana', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataAnggaran(res.data.data)
                    setAnggaran(res.data.total[0].anggaran)
                    setRealisasi(res.data.total[0].realisasi)
                    setSaldo(res.data.total[0].saldo)
                    setLoading(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    
      const getWarna = async() => {
        setWarna(await Asynstorage.getItem('warna_desa'))
    }

    useEffect(() => {
        getWarna()
        InteractionManager.runAfterInteractions(() => {
          getDataKonten()
          });
      }, [])

    return (
        <View style={{flex: 1}}>
            <Konten dataAnggaran={dataAnggaran} anggaran={anggaran} realisasi={realisasi} saldo={saldo} warna={warna} />
        </View>
    )
}
