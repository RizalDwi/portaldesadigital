import React, {useState, createContext, useEffect} from 'react'
import { View, Text, Pressable, StyleSheet, Modal, InteractionManager } from 'react-native'
import Konten from './../../../container/molecules/konten/KontenPengajuanProduk/NavigatorPengajuanProduk'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'
import ScreenWarga from './../../ScreenWarga'

export const RootContext = createContext();
export default function index({navigation, route}) {
    const [file, setFile] = useState('')
    const [namaProduk, setNamaProduk] = useState('')
    const [deskripsi, setDeskripsi] = useState('')
    const [loading, setLoading] = useState(true)
    const [status, setStatus] = useState('')
    const [listStatus, setListStatus] = useState([])
    const [count, setCount] = useState(0);
    const [alasan, setAlasan] = useState('')
    const [modalVisible, setModalVisible] = useState(false);

    const handleFile = (value) => {
        setFile(value)
    }

    const handleNamaProduk = (value) => {
        setNamaProduk(value)
    }

    const handleDeskripsi = (value) => {
        setDeskripsi(value)
    }

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_users: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_status_pengajuan_produk', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setStatus(res.data.succes)
                    setListStatus(res.data.data)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showModal = (value) => {
        setAlasan(value)
        setModalVisible(true)
        console.log("test")
        
      }

    async function addProduk() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_users: id_user,
                    judul_produk: namaProduk,
                    deskripsi: deskripsi,
                    gambar_produk: file
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_produk_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    getData()
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
          getData()
          });
        
      }, [])

      if(loading){
          return <LoadingScreen />
      }
      
      if(!status){
          return <ScreenWarga />
      }

    return (
        <RootContext.Provider value={{
            handleFile,
            handleNamaProduk,
            handleDeskripsi,
            addProduk,
            listStatus,
            file,
            showModal
        }}>
            <View style={{flex: 1}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>{alasan}</Text>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalVisible(!modalVisible)}
                  >
                    <Text style={styles.textStyle}>Tutup</Text>
                  </Pressable>
                </View>
              </View>
            </Modal>
                <Konten />
            </View>
        </RootContext.Provider>
    )
}
const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22,
    },
    modalView: {
      backgroundColor: "white",
      borderRadius: 20,
      padding: 20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
      width: '90%',
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      width: 80
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "justify"
    }
  });