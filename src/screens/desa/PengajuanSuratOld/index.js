import React, {useState, createContext, useEffect} from 'react'
import { View, InteractionManager } from 'react-native'
import Konten from '../../../container/molecules/konten/KontenPengjuanSurat/navigation'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'
import ScreenWarga from './../../ScreenWarga'

export const RootContext = createContext();
export default function index({navigation, route}) {
    const [jenis, setJenis] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [kepentingan, setKepentingan] = useState('')
    const [idJenis, setIdJenis] = useState('')
    const [pengajuan, setPengajuan] = useState([])
    const [arsip, setArsip] = useState([])
    const [cekStatus, setCekStatus] = useState(true)

    const handleKepentingan = (value) => {
        setKepentingan(value)
    }

    const handleIdJenis = (value) => {
        setIdJenis(value)
    }
    

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/data_surat_user', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setJenis(res.data.jenis)
                    setPengajuan(res.data.pengajuan)
                    setArsip(res.data.arsip)
                    setCekStatus(res.data.succes)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function addPengajuan() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_users: id_user,
                    id_jenis_surat: idJenis,
                    kepentingan: kepentingan 
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_pengajuan_surat', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    getData()
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getData()
            });
      }, [])

      if(loading) {
          return <LoadingScreen />
      }

      if(!cekStatus) {
          return <ScreenWarga />
      }

    return (
        <RootContext.Provider value={{
            jenis,
            handleIdJenis,
            handleKepentingan,
            addPengajuan,
            pengajuan,
            arsip
        }}>
            <View style={{flex:1}}>
                <Konten />
            </View>
        </RootContext.Provider>
    )

}

