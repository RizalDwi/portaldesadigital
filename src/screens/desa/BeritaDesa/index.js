import React from 'react'
import { View, Text } from 'react-native'
import HeaderBerita from '../../../container/molecules/header/Header'
import KontenBerita from '../../../container/molecules/konten/KontenArtikel'

export default function index() {
    return (
        <View style={{flex: 1}}>
            <HeaderBerita judul="Artikel Berita" />
            <KontenBerita />
            <NavBar />
        </View>
    )
}
