import React,{useEffect, useState} from 'react'
import { View, InteractionManager, ScrollView, StyleSheet, ToastAndroid } from 'react-native'
import Konten from './../../../container/molecules/form/FormKontakDesa'
import LoadingScreen from './../../LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import ScreenWarga from './../../ScreenWarga'

export default function index({route}) {

    const [perihal, setPerihal] = useState('')
    const [judul, setJudul] = useState('')
    const [isi, setIsi] = useState('')
    const [loading, setLoading] = useState(true)
    const [status, setStatus] = useState('')

    const handlePerihal = (value) => {
        setPerihal(value)
    }

    const handleJudul = (value) => {
        setJudul(value)
    }

    const handleIsi = (value) => {
        setIsi(value)
    }

    const showToast = () => {
        ToastAndroid.show("Laporan telah dikirim", ToastAndroid.SHORT);
      };

    async function cek_warga() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_users: id_user,
                    id_desa: route.params.idDesa
                }
                setLoading(true)
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/cek_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log('cek ->', res)
                    setLoading(false)
                    setStatus(res.data.status)
                    
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function addPesan() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_users: id_user,
                    perihal: perihal,
                    judul_laporan: judul,
                    isi_laporan: isi
                }
                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_laporan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            cek_warga()
          });
        
      }, [])

    if(loading){
        return <LoadingScreen />
    }

    if(status != "Diterima") {
        return <ScreenWarga />
    }

    return (
        <View style={{flex: 1}}>
            <ScrollView style={{flex: 1, padding: 10}}>
                <Konten handleIsi={handleIsi} handleJudul={handleJudul} handlePerihal={handlePerihal} addPesan={addPesan} />
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins', fontSize: 14, color: '#394761'},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'}
})