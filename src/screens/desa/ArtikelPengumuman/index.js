import React from 'react'
import { View, Text } from 'react-native'
import Header from './../../../container/molecules/header/Header'
import KontenArtikel from './../../../container/molecules/konten/KontenArtikel'


export default function index() {
    return (
        <View style={{flex: 1}}>
            <Header judul="Pengumuman" />
            <KontenArtikel gambar={require('./../../../assets/images/tanam.jpg')} />
            <NavBar />

        </View>
    )
}
