import React from 'react'
import { View, Text } from 'react-native'
import Header from './../../../container/molecules/header/HeaderProfile'
import Konten from '../../../container/molecules/konten/KontenKelolaIventaris'

export default function index({navigation, route}) {
    return (
        <View style={{flex: 1}}>
            <Header />
            <Konten navigation={navigation} id_desa={route.params.idDesa} />
        </View>
    )
}
