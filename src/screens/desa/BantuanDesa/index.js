import React, {useEffect, useState} from 'react'
import { View, InteractionManager } from 'react-native'
import Konten from './../../../container/molecules/konten/KontenBantuanDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'
import ScreenWarga from './../../ScreenWarga'

export default function index({navigation, route}) {

    const [listBantuan, setListBantuan] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [cekStatus, setCekStatus] = useState(true)
    const [warna, setWarna] = useState('')

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        setWarna(await Asynstorage.getItem('warna_desa'))
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                    id_user: id_user
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_bantuan_from_user', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setListBantuan(res.data.data)
                    setCekStatus(res.data.succes)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getData()
            });
    
        
      }, [])

      if(loading){
          return <LoadingScreen />
      }
    
      if(!cekStatus){
          return <ScreenWarga />
      }

    return (
        <View style={{flex: 1}}>
            
            <Konten listBantuan={listBantuan} warna={warna} />
        </View>
    )
}
