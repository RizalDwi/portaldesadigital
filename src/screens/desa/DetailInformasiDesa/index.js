import React, {useEffect, useState} from 'react'
import { View, Text } from 'react-native'
import Konten from '../../../container/molecules/konten/KontenDetailInformasiDesa'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../LoadingScreen'


export default function index({route}) {

    const [file, setFile] = useState('')
    const [judul, setJudul] = useState('')
    const [deskripsi, setDeskripsi] = useState('')
    const [loading, setLoading] = useState(true)

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_informasi_desa_satuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setJudul(res.data.data[0].judul_informasi)
                    setDeskripsi(res.data.data[0].deskripsi)
                    setFile(res.data.data[0].foto)
                    setLoading(false)
                    console.log(res)
                    
                })
                .catch((err) => {
                    console.log("Error ->", err)
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getData()
      }, [])

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1}}>
            <Konten judul={judul} deskripsi={deskripsi} foto={file} />
        </View>
    )
}
