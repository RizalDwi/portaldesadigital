import * as React from 'react';
import { View, Text, Image } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './../../screens/Login'
import Register from './../../screens/Register'
import BlankScreen from './../../screens/BlankScreen'
import Akun from './../../screens/Akun'
import Input from './../../screens/Input'
import DrawerNavigation from './../DrawerNavigation'

const Stack = createStackNavigator();

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)

    // React.useEffect(() => {
    //     setTimeout(() => {
    //         setIsLoading(!isLoading)
    //     }, 2000)
    // }, [])

    // if(isLoading) {
    //     return <SplashScreen />
    // }

    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} options={{
                    headerShown: false,
                    animationEnabled: false
                    }} />  
                <Stack.Screen name="Register" component={Register} options={{
                    headerShown: false,
                    animationEnabled: false
                    }} />
                <Stack.Screen name="main" children={DrawerNavigation} options={{
                    headerShown: false,
                    animationEnabled: false
                    }} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigation;