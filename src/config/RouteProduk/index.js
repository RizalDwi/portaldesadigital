import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Beranda from './../../screens/main/HomeLanding'
import ArtikelProduk from './../../screens/main/ArtikelProduk'
import Produk from './../../screens/main/ProdukWarga'

const Stack = createStackNavigator();

export default function index() {
    return (
        <Stack.Navigator>
        
        <Stack.Screen name="Produk" component={Produk} options={{
            headerShown: false
            }} />
        <Stack.Screen name="ProdukWarga" component={ArtikelProduk} options={{
                title: 'Produk Warga',
                headerStyle: {
                  backgroundColor: '#4CC9F0',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
    </Stack.Navigator>
    )
}