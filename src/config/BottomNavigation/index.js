import React from 'react'
import { View, Text, Image } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Polling from './../../screens/main/Polling'
import RouteProduk from './../RouteProduk'
import SearchNavigation from './../SearchNavigation'
import RouteBeranda from './../RouteBeranda'
import RoutePengumuman from './../RoutePengumuman'

const Tab = createBottomTabNavigator();

export default function index() {


    const IconBottom = (props) => {
        const {color, focused} = props.data
        let colorSelected = focused ? color : 'black'
        return (
          <View>
            <Image source={props.image} style={{width: 40, height: 40, tintColor: colorSelected}} />
          </View>
        )
      }

    return (
        <Tab.Navigator tabBarOptions={{
            style: {height: 80}
            }}>
            <Tab.Screen name="HomeLanding" children={RouteBeranda}
                options={{
                title: 'Beranda',
                tabBarIcon: (props) => (
                    <IconBottom data={props} image={require('./../../assets/icons/homeNew.png')} />
                )
                }}
            />
            <Tab.Screen name="Pengumuman" children={RoutePengumuman} 
                options={{
                title: 'Pengumuman',
                tabBarIcon: (props) => (
                    <IconBottom data={props} image={require('./../../assets/icons/toa.png')} />
                )
                }}
            />
            <Tab.Screen name="Polling" component={Polling} 
                options={{
                title: 'Polling',
                tabBarIcon: (props) => (
                    <IconBottom data={props} image={require('./../../assets/icons/polling.png')} />
                )
                }}
            />
            <Tab.Screen name="Pencarian" children={SearchNavigation} 
                options={{
                title: 'Pencarian',
                tabBarIcon: (props) => (
                    <IconBottom data={props} image={require('./../../assets/icons/pencarian.png')} />
                )
                }}
            />
            <Tab.Screen name="Produk" children={RouteProduk} 
                options={{
                title: 'Produk Warga',
                tabBarIcon: (props) => (
                    <IconBottom data={props} image={require('./../../assets/icons/produk.png')} />
                )
                }}
            />
            </Tab.Navigator>
    )
}