import React, {useEffect, useState} from 'react'
import { InteractionManager } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import ProfilDesa from './../../screens/admin/ProfilDesa'
import MenuDesa from './../../screens/admin/MenuDesa'
import Pemerintahan from './../../screens/admin/Pemerintahan'
import FormTambahAparatDesa from './../../screens/admin/TambahAparatDesa'
import Iventaris from './../../screens/admin/Iventaris'
import IventarisMesin from './../../container/molecules/Tabel/IventarisMesin'
import IventarisElektronik from './../../container/molecules/Tabel/IventarisElektronik'
import IventarisBangunan from './../../container/molecules/Tabel/IventarisBangunan'
import IventarisJaringan from './../../container/molecules/Tabel/IventarisJaringan'
import IventarisTanah from './../../container/molecules/Tabel/IventarisTanah'
import IventarisKontruksi from './../../container/molecules/Tabel/IventarisKonstruksiDalamPengerjaan'
import IventarisAsetLainya from './../../container/molecules/Tabel/IventarisAsetLainya'
import FormIventarisMesin from './../../container/molecules/form/FormTambahIventaris/FormIventarisMesin'
import FormIventarisElektronik from './../../container/molecules/form/FormTambahIventaris/FormIventarisElektronik'
import FormIventarisGedung from './../../container/molecules/form/FormTambahIventaris/FormIventarisGedung'
import FormIventarisJaringan from './../../container/molecules/form/FormTambahIventaris/FormIventarisJaringan'
import FormIventarisTanah from './../../container/molecules/form/FormTambahIventaris/FormIevntarisTanah'
import FormIventarisKonstruksi from '../../container/molecules/form/FormTambahIventaris/FormIventarisKonstruksi'
import FormIventarisAsetLainya from './../../container/molecules/form/FormTambahIventaris/FormIventarisAsetLainya'
import DanaDesa from './../../screens/admin/DanaDesa'
import APBDes from './../../screens/admin/APBDes'
import FormTambahAnggaran from './../../container/molecules/form/FormTambahAnggaran'
import Realisasi from './../../screens/admin/Realisasi'
import FormTambahRealisasi from './../../container/molecules/form/FormTambahRealisasi'
import KategoriAPBDes from './../../screens/admin/KategoriAPBDes'
import TambahKategori from './../../container/molecules/form/FormTambahKategori'
import SubkategoriAPBDes from './../../screens/admin/SubkategoriAPBDes'
import TambahSubkategori from './../../container/molecules/form/FormTambahSubkategori'
import KontakDesa from './../../screens/admin/KontakDesa'
import DetailPesanWarga from './../../screens/admin/DetailPesanWarga'
import ProdukWarga from './../../screens/admin/ProdukWarga'
import DetailProdukWarga from './../../screens/admin/DetailProdukWarga'
import BeritaDesa from './../../screens/admin/BeritaDesa'
import FormBerita from './../../container/molecules/form/FormBuatBerita'
import TabelBerita from './../../container/molecules/Tabel/TabelBerita'
import PengumumanDesa from './../../screens/admin/PengumumanDesa'
import FormPengumuman from './../../container/molecules/form/FormBuatPengumuman'
import TabelPengumuman from './../../container/molecules/Tabel/TabelPengumuman'
import Polling from './../../screens/admin/PollingDesa'
import TambahPolling from './../../container/molecules/form/FormTambahPolling'
import ChartPolling from './../../screens/admin/ChartPolling'
import PertanyaanPolling from './../../screens/admin/PertanyaanPolling'
import FormPilihanPolling from './../../container/molecules/form/FormPilihanPolling'
import PengajuanWarga from './../../screens/admin/PengajuanWarga'
import DetailPengajuan from './../../screens/admin/DetailPengajuan'
import BantuanDesa from './../../screens/admin/BantuanDesa'
import TambahProgramBantuan from './../../screens/admin/TambahProgramBantuan'
import PesertaBantuan from './../../screens/admin/PesertaBantuan'
import FormTambahPesertaBantuan from './../../container/molecules/form/FormTambahPesertaBantuan'
import InformasiDesa from './../../screens/admin/InformasiDesa'
import TambahTopografi from './../../screens/admin/TambahTopografi'
import EditTopografi from './../../screens/admin/EditTopografi'
import KelompokTopografi from './../../screens/admin/KelompokTopografi'
import TambahLuasDesa from './../../screens/admin/TambahLuasDesa'
import EditLuasDesa from './../../screens/admin/EditLuasDesa'
import TambahKelompokTopografi from './../../screens/admin/TambahKelompokTopografi'
import EditKelompokTopografi from './../../screens/admin/EditKelompokTopografi'
import EditAparat from './../../screens/admin/EditAparat'
import TambahMisi from './../../screens/admin/TambahMisi'
import EditMisi from './../../screens/admin/EditMisi'
import FormEditIventarisMesin from './../../container/molecules/form/FormEditIventaris/FormEditIventarisMesin'
import FormEditIventarisElektronik from './../../container/molecules/form/FormEditIventaris/FormEditIventarisElektronik'
import FormEditIventarisBangunan from './../../container/molecules/form/FormEditIventaris/FormEditIventarisBangunan'
import FormEditIventarisJaringan from './../../container/molecules/form/FormEditIventaris/FormEditIventarisJaringan'
import FormEditIventarisTanah from './../../container/molecules/form/FormEditIventaris/FormEditIventarisTanah'
import FormEditIventarisKonstruksi from './../../container/molecules/form/FormEditIventaris/FormEditIventarisKonstruksi'
import FormEditIventarisAsetLainya from './../../container/molecules/form/FormEditIventaris/FormEditIventarisAsetLainya'
import FormEditSubkategori from './../../container/molecules/form/FormEditSubkategori'
import FormEditKategori from './../../container/molecules/form/FormEditKategori'
import FormEditAnggaran from './../../container/molecules/form/FormEditAnggaran'
import FormEditBerita from './../../container/molecules/form/FormEditBerita'
import FormEditPengumuman from './../../container/molecules/form/FormEditPengumuman'
import FormEditPilihanPolling from './../../container/molecules/form/FormEditPilihanPolling'
import FormEditPolling from './../../container/molecules/form/FormEditPolling'
import FormTambahInformasi from './../../container/molecules/form/FormTambahInformasiDesa'
import FormEditInformasiDesa from './../../container/molecules/form/FormEditInformasiDesa'
import PelayananSurat from './../../screens/admin/PelayananSurat'
import FormTambahJenisSurat from './../../container/molecules/form/FornTambahJenisSurat'
import PengajuanSurat from './../../screens/admin/PengajuanSurat'
import FormEditJenisSurat from './../../container/molecules/form/FormEditJenisSurat'
import ViewSurat from './../../screens/admin/LihatSurat'
import DetailWarga from './../../screens/admin/DetailWarga'
import FormEditProgram from './../../container/molecules/form/FormEditProgramBantuan'
import DetailHasilPolling from './../../screens/admin/DetailHasilPolling'
import Maps from './../../screens/admin/Maps'
import MarkerDesa from './../../screens/admin/MarkerPeta'
import SettingAdmin from './../../screens/admin/SettingAdmin'
import TambahAdmin from './../../screens/admin/TambahAdmin'
import StatistikPengunjung from '../../screens/admin/StatistikPengunjung'
import Asynstorage from '@react-native-community/async-storage'
import PengaturanSurat from '../../screens/admin/PengaturanSurat/home'
import KlasifikasiSurat from '../../screens/admin/PengaturanSurat/KlasifikasiSurat'
import TambahKlasifikasi from '../../screens/admin/PengaturanSurat/KlasifikasiSurat/tambah.js'
import EditKlasifikasi from '../../screens/admin/PengaturanSurat/KlasifikasiSurat/edit.js'
import FormatSurat from '../../screens/admin/PengaturanSurat/FormatSurat'
import PersyaratanSurat from '../../screens/admin/PengaturanSurat/persyaratan'
import KelolaPengajuan from '../../screens/admin/PengaturanSurat/Pengajuan'
import TandaTangan from '../../screens/admin/PengaturanSurat/TandaTangan'
import Arsip from '../../screens/admin/PengaturanSurat/Arsip'
import TambahPersyaratan from '../../screens/admin/PengaturanSurat/persyaratan/tambah.js'
import EditPersyaratan from '../../screens/admin/PengaturanSurat/persyaratan/edit.js'
import TambahTandaTangan from '../../screens/admin/PengaturanSurat/TandaTangan/tambah.js'
import TambahFormatSurat from '../../screens/admin/PengaturanSurat/FormatSurat/tambah.js'
import EditFormat from '../../screens/admin/PengaturanSurat/FormatSurat/edit.js'
import KodeIsian from './../../screens/admin/PengaturanSurat/KodeInput'
import TambahKodeIsian from '../../screens/admin/PengaturanSurat/KodeInput/tambah.js'
import EditKodeIsian from '../../screens/admin/PengaturanSurat/KodeInput/edit.js'
import DetailPersyaratan from '../../screens/admin/PengaturanSurat/DetailPersyaratan'
import TambahDetailPersyaratan from '../../screens/admin/PengaturanSurat/DetailPersyaratan/tambah.js'
import DetailPengajuanSurat from '../../screens/admin/PengaturanSurat/Pengajuan/detail.js'
import PanduanInput from '../../screens/admin/PengaturanSurat/KodeInput/panduan.js'
import PanduanTemplate from '../../screens/admin/PengaturanSurat/FormatSurat/panduan.js'

const Stack = createStackNavigator();

export default function index() {

  const [colorHeader, setColorHeader] = useState('')

  const getWarna = async() => {
    console.log("Warna admin ->", await Asynstorage.getItem('warna_banner_admin'))
    setColorHeader(await Asynstorage.getItem('warna_banner_admin'))
  }

  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
        getWarna()
      });
  }, [])

    return (
    <Stack.Navigator>
        <Stack.Screen name="MenuDesa" component={MenuDesa} options={{
            headerShown: false
            }} />
        <Stack.Screen name="KelolaProfilDesa" component={ProfilDesa} options={{
                title: 'Kelola Profil Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaPemerintahan" component={Pemerintahan} options={{
                title: 'Kelola Pemerintahan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormTambahAparatDesa" component={FormTambahAparatDesa} options={{
                title: 'Tambah Aparat Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaIventaris" component={Iventaris} options={{
                title: 'Kelola Iventaris',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisMesin" component={IventarisMesin} options={{
                title: 'Data Iventaris Mesin',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisElektronik" component={IventarisElektronik} options={{
                title: 'Data Iventaris Elektronik',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisBangunan" component={IventarisBangunan} options={{
                title: 'Data Iventaris Gedung dan Bangunan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisJaringan" component={IventarisJaringan} options={{
                title: 'Data Iventaris Jalan, Irigasi, dan Jaringan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisTanah" component={IventarisTanah} options={{
                title: 'Data Iventaris Tanah',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisKonstruksi" component={IventarisKontruksi} options={{
                title: 'Data Iventaris Konstruksi Dalam Pengerjaan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelIventarisAsetLainya" component={IventarisAsetLainya} options={{
                title: 'Data Iventaris Aset Lainya',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisMesin" component={FormIventarisMesin} options={{
                title: 'Tambah Iventaris Mesin',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisElektronik" component={FormIventarisElektronik} options={{
                title: 'Tambah Iventaris Elektronik',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisGedung" component={FormIventarisGedung} options={{
                title: 'Tambah Iventaris Gedung',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisJaringan" component={FormIventarisJaringan} options={{
                title: 'Tambah Iventaris Jalan, Irigasi, dan Jaringan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisTanah" component={FormIventarisTanah} options={{
                title: 'Tambah Iventaris Tanah',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisKonstruksi" component={FormIventarisKonstruksi} options={{
                title: 'Tambah Iventaris Konstruksi dalam Pengerjaan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormIventarisAsetLainya" component={FormIventarisAsetLainya} options={{
                title: 'Tambah Iventaris Aset Lainya',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaDanaDesa" component={DanaDesa} options={{
                title: 'Kelola Dana Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="APBDes" component={APBDes} options={{
                title: 'Kelola APBDes',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormTambahAnggaran" component={FormTambahAnggaran} options={{
                title: 'Tambah Anggaran',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="Realisasi" component={Realisasi} options={{
                title: 'Realisasi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormTambahRealisasi" component={FormTambahRealisasi} options={{
                title: 'Tambah Realisasi APBDes',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KategoriAPBDes" component={KategoriAPBDes} options={{
                title: 'Pengaturan Kategori',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahKategori" component={TambahKategori} options={{
                title: 'Tambah Kategori',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="SubkategoriAPBDes" component={SubkategoriAPBDes} options={{
                title: 'Pengaturan Sub Kategori',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahSubkategori" component={TambahSubkategori} options={{
                title: 'Tambah Subkategori',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaKontakDesa" component={KontakDesa} options={{
                title: 'Pesan Warga',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailPesanWarga" component={DetailPesanWarga} options={{
                title: 'Detail Pesan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaProdukWarga" component={ProdukWarga} options={{
                title: 'Produk Warga',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailProdukWarga" component={DetailProdukWarga} options={{
                title: 'Detail Produk',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="BeritaDesa" component={BeritaDesa} options={{
                title: 'Kelola Berita Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormBerita" component={FormBerita} options={{
                title: 'Buat Berita',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelBerita" component={TabelBerita} options={{
                title: 'Daftar Berita',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PengumumanDesa" component={PengumumanDesa} options={{
                title: 'Kelola Pengumuman',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormPengumuman" component={FormPengumuman} options={{
                title: 'Buat Pengumuman',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelPengumuman" component={TabelPengumuman} options={{
                title: 'Daftar Pengumuman',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PollingDesa" component={Polling} options={{
                title: 'Kelola Poling',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahPolling" component={TambahPolling} options={{
                title: 'Tambah Poling',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="ChartPolling" component={ChartPolling} options={{
                title: 'Hasil Poling',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PertanyaanPolling" component={PertanyaanPolling} options={{
                title: 'Pertanyaan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormPilihanPolling" component={FormPilihanPolling} options={{
                title: 'Tambah Pilihan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaPengajuanWarga" component={PengajuanWarga} options={{
                title: 'Pengajuan Warga',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailPengajuan" component={DetailPengajuan} options={{
                title: 'Detail Pengajuan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TabelBantuanDesa" component={BantuanDesa} options={{
                title: 'Daftar Program Bantuan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahProgram" component={TambahProgramBantuan} options={{
                title: 'Tambah Program',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PesertaBantuan" component={PesertaBantuan} options={{
                title: 'Peserta Bantuan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahPeserta" component={FormTambahPesertaBantuan} options={{
                title: 'Tambah Peserta',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaInformasiDesa" component={InformasiDesa} options={{
                title: 'Kelola Informasi Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahTopografi" component={TambahTopografi} options={{
                title: 'Tambah Topografi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditTopografi" component={EditTopografi} options={{
                title: 'Edit Topografi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelompokTopografi" component={KelompokTopografi} options={{
                title: 'Kelompok Topografi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahLuasDesa" component={TambahLuasDesa} options={{
                title: 'Tambah Wilayah Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditLuasDesa" component={EditLuasDesa} options={{
                title: 'Ubah Wilayah Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahKelompokTopografi" component={TambahKelompokTopografi} options={{
                title: 'Tambah Kelompok Topografi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditKelompokTopografi" component={EditKelompokTopografi} options={{
                title: 'Edit Kelompok Topografi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditAparat" component={EditAparat} options={{
                title: 'Edit Data Aparat Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahMisi" component={TambahMisi} options={{
                title: 'Tambah Misi Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditMisi" component={EditMisi} options={{
                title: 'Edit Misi Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
         <Stack.Screen name="FormEditIventarisMesin" component={FormEditIventarisMesin} options={{
                title: 'Edit Iventaris Mesin',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditIventarisElektronik" component={FormEditIventarisElektronik} options={{
                title: 'Edit Iventaris Elektronik',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditIventarisBangunan" component={FormEditIventarisBangunan} options={{
                title: 'Edit Iventaris Bangunan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditIventarisJaringan" component={FormEditIventarisJaringan} options={{
                title: 'Edit Iventaris Jaringan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

        <Stack.Screen name="FormEditIventarisTanah" component={FormEditIventarisTanah} options={{
                title: 'Edit Iventaris Tanah',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

        <Stack.Screen name="FormEditIventarisKonstruksi" component={FormEditIventarisKonstruksi} options={{
                title: 'Edit Iventaris Konstruksi Dalam Pengerjaan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditIventarisAsetLainya" component={FormEditIventarisAsetLainya} options={{
                title: 'Edit Iventaris Aset Lainya',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditSubkategori" component={FormEditSubkategori} options={{
                title: 'Edit Subkategori',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditKategori" component={FormEditKategori} options={{
                title: 'Edit Kategori',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditAnggaran" component={FormEditAnggaran} options={{
                title: 'Edit Anggaran',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

        <Stack.Screen name="FormEditBerita" component={FormEditBerita} options={{
                title: 'Edit Berita',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditPengumuman" component={FormEditPengumuman} options={{
                title: 'Edit Pengumuman',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditPilihanPolling" component={FormEditPilihanPolling} options={{
                title: 'Edit Pilihan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditPolling" component={FormEditPolling} options={{
                title: 'Edit Polling',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormTambahInformasi" component={FormTambahInformasi} options={{
                title: 'Tambah Informasi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditInformasiDesa" component={FormEditInformasiDesa} options={{
                title: 'Edit Informasi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PelayananSurat" component={PelayananSurat} options={{
                title: 'Pelayanan Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormTambahJenisSurat" component={FormTambahJenisSurat} options={{
                title: 'Tambah Jenis Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

        <Stack.Screen name="PengajuanSurat" component={PengajuanSurat} options={{
                title: 'Detail Pengajuan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

        <Stack.Screen name="FormEditJenisSurat" component={FormEditJenisSurat} options={{
                title: 'Edit Jenis Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="LihatSurat" component={ViewSurat} options={{
                title: 'Lihat Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailWarga" component={DetailWarga} options={{
                title: 'Detail Warga',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormEditProgram" component={FormEditProgram} options={{
                title: 'Edit Program',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailHasilPolling" component={DetailHasilPolling} options={{
                title: 'Detail Hasil Polling',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="Maps" component={Maps} options={{
                title: 'Kelola Peta Desa',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="MarkerDesa" component={MarkerDesa} options={{
                title: 'Kelola Marker',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="SettingAdmin" component={SettingAdmin} options={{
                title: 'Setting Admin',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahAdmin" component={TambahAdmin} options={{
                title: 'Tambah Admin',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="StatistikPengunjung" component={StatistikPengunjung} options={{
                title: 'Statistik Pengunjung',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PengaturanSurat" component={PengaturanSurat} options={{
                title: 'Pengaturan Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KlasifikasiSurat" component={KlasifikasiSurat} options={{
                title: 'Klasifikasi Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahKlasifikasi" component={TambahKlasifikasi} options={{
                title: 'Tambah Klasifikasi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditKlasifikasi" component={EditKlasifikasi} options={{
                title: 'Edit Klasifikasi',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormatSurat" component={FormatSurat} options={{
                title: 'Format Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
         <Stack.Screen name="Persyaratan" component={PersyaratanSurat} options={{
                title: 'Persyaratan Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KelolaPengajuan" component={KelolaPengajuan} options={{
                title: 'Pengajuan Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
         <Stack.Screen name="TandaTangan" component={TandaTangan} options={{
                title: 'Tanda Tangan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="Arsip" component={Arsip} options={{
                title: 'Arsip Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahPersyaratan" component={TambahPersyaratan} options={{
                title: 'Tambah Persyaratan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
         <Stack.Screen name="EditPersyaratan" component={EditPersyaratan} options={{
                title: 'Edit Persyaratan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahTandaTangan" component={TambahTandaTangan} options={{
                title: 'Tambah Tanda Tangan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahFormatSurat" component={TambahFormatSurat} options={{
                title: 'Tambah Format Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditFormat" component={EditFormat} options={{
                title: 'Edit Format Surat',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KodeIsian" component={KodeIsian} options={{
                title: 'Kode Input',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahKodeIsian" component={TambahKodeIsian} options={{
                title: 'Tambah Kode Input',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="EditKodeIsian" component={EditKodeIsian} options={{
                title: 'Edit Kode Input',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailPersyaratan" component={DetailPersyaratan} options={{
                title: 'Detail Persyaratan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

<Stack.Screen name="TambahDetailPersyaratan" component={TambahDetailPersyaratan} options={{
                title: 'Tambah Persyaratan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

<Stack.Screen name="DetailPengajuanSurat" component={DetailPengajuanSurat} options={{
                title: 'Detail Pengajuan',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PanduanInput" component={PanduanInput} options={{
                title: 'Panduan Input',
                headerStyle: {
                  backgroundColor: `#${colorHeader}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>

      <Stack.Screen name="PanduanTemplate" component={PanduanTemplate} options={{
                      title: 'Panduan Template Surat',
                      headerStyle: {
                        backgroundColor: `#${colorHeader}`,
                      },
                      headerTintColor: '#fff',
                      headerTitleStyle: {
                        fontWeight: 'bold',
                        fontFamily: 'Poppins-Bold'
                      }
              }}/>
    </Stack.Navigator>
    )
}