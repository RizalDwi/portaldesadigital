import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Beranda from './../../screens/main/HomeLanding'
import ArtikelBerita from './../../screens/main/ArtikelBerita'

const Stack = createStackNavigator();

export default function index() {
    return (
        <Stack.Navigator>
        
        <Stack.Screen name="Beranda" component={Beranda} options={{
            headerShown: false
            }} />
        <Stack.Screen name="ArtikelDesa" component={ArtikelBerita} options={{
                title: 'Berita Desa',
                headerStyle: {
                  backgroundColor: '#4CC9F0',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
    </Stack.Navigator>
    )
}