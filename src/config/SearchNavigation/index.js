import React, {useState, useEffect} from 'react'
import { View, Text } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import { createStackNavigator } from '@react-navigation/stack';
import PencarianDesa from './../../screens/main/PencarianDesa'
import HomeDesa from './../../screens/desa/HomeDesa'
import Iventaris from './../../screens/desa/IventarisDesa'
import DanaDesa from './../../screens/desa/DanaDesa'
import DetailDanaDesa from './../../screens/desa/DetailDanaDesa'
import KontakDesa from './../../screens/desa/KontakDesa'
import PengajuanProdukWarga from './../../screens/desa/PengajuanProduk'
import PengajuanSurat from './../../screens/desa/PengajuanSurat/Home'
import PengajuanWarga from './../../screens/desa/PengajuanWarga'
import BantuanDesa from './../../screens/desa/BantuanDesa'
import DetailInfromasi from './../../screens/desa/DetailInformasiDesa'
import InformasiDesa from './../../screens/desa/InformasiDesa'
import ProfileDesa from '../../screens/desa/ProfileDesa'
import Pemerintahan from '../../screens/desa/Pemerintahan'
import ViewSurat from './../../screens/admin/LihatSurat'
import FormKontakDesa from './../../container/molecules/form/FormKontakDesa'
import Maps from './../../screens/desa/Maps'
import ArtikelBerita from './../../screens/main/ArtikelBerita'
import ArtikelPengumuman from './../../screens/main/ArtikelPengumuman'
import IventarisMesin from './../../container/molecules/konten/KontenIventaris/Mesin'
import IventarisAset from './../../container/molecules/konten/KontenIventaris/Aset'
import IventarisBangunan from './../../container/molecules/konten/KontenIventaris/Bangunan'
import IventarisElektronik from './../../container/molecules/konten/KontenIventaris/Elektronik'
import IventarisJaringan from './../../container/molecules/konten/KontenIventaris/Jaringan'
import IventarisKontruksi from './../../container/molecules/konten/KontenIventaris/Konstruksi'
import IventarisTanah from './../../container/molecules/konten/KontenIventaris/Tanah'
import AjukanSurat from '../../screens/desa/PengajuanSurat/AjukanSurat'
import FormSurat from './../../screens/desa/PengajuanSurat/AjukanSurat/formSurat.js'
import DokumenPersyaratan from '../../screens/desa/PengajuanSurat/DokumenPersyaratan'
import TambahDokumen from '../../screens/desa/PengajuanSurat/DokumenPersyaratan/tambah.js'
import ArsipSurat from '../../screens/desa/PengajuanSurat/Arsip'
import StatusPengajuan from '../../screens/desa/PengajuanSurat/Status'

const Stack = createStackNavigator();

export default function index({navigation}) {

  const [warna, setWarna] = useState('')
  const [idDesa, setIdDesa] = useState(0)

  // const getWarna = async() => {
  //     setWarna(await Asynstorage.getItem('warna_desa'))
  //     console.log("Warna ->", await Asynstorage.getItem('warna_desa'))
  // }

  // useEffect(() => {
  //     getWarna()
  // }, [])

    return (
        <Stack.Navigator>
        
        <Stack.Screen name="Pencarian" component={props => <PencarianDesa {...props} navigation={navigation} setWarna={setWarna} setIdDesa={setIdDesa} />} options={{
            headerShown: false
            }} />
        <Stack.Screen name="Desa" component={props => <HomeDesa {...props} navigation={navigation} setWarna={setWarna} idDesa={idDesa} />} options={{
                title: 'Desa',
                
                headerTintColor: '#000',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="ProfilDesa" component={ProfileDesa} options={{
                title: 'Profil Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="Pemerintahan" component={Pemerintahan} options={{
                title: 'Pemerintahan',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="Iventaris" component={Iventaris} options={{
                title: 'Iventaris',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DanaDesa" component={DanaDesa} options={{
                title: 'Dana Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailDanaDesa" component={DetailDanaDesa} options={{
                title: 'Tahun Anggaran 2020',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="KontakDesa" component={KontakDesa} options={{
                title: 'Kontak Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PengajuanProduk" component={PengajuanProdukWarga} options={{
                title: 'Pengajuan Produk',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PengajuanSurat" component={PengajuanSurat} options={{
                title: 'Pengajuan Surat',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="PengajuanWarga" component={PengajuanWarga} options={{
                title: 'Pengajuan Menjadi Warga',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="BantuanDesa" component={BantuanDesa} options={{
                title: 'Bantuan Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DetailInformasi" component={DetailInfromasi} options={{
                title: 'Detail Infromasi Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="InformasiDesa" component={InformasiDesa} options={{
                title: 'Infromasi Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="LihatSurat" component={ViewSurat} options={{
                title: 'Lihat Surat',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormKontakDesa" component={FormKontakDesa} options={{
                title: 'Form Kontak Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="Maps" component={Maps} options={{
                title: 'Peta Desa',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="ArtikelBerita" component={ArtikelBerita} options={{
                title: 'Artikel Berita',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="ArtikelPengumuman" component={ArtikelPengumuman} options={{
                title: 'Artikel Pengumuman',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisMesin" component={IventarisMesin} options={{
                title: 'Iventaris Mesin',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisAset" component={IventarisAset} options={{
                title: 'Iventaris Aset lain',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisBangunan" component={IventarisBangunan} options={{
                title: 'Iventaris Gedung',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisElektronik" component={IventarisElektronik} options={{
                title: 'Iventaris Elektronik',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisJaringan" component={IventarisJaringan} options={{
                title: 'Iventaris Jaringan',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisKontruksi" component={IventarisKontruksi} options={{
                title: 'Iventaris Kontruksi Dalam Pengerjaan',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="IventarisTanah" component={IventarisTanah} options={{
                title: 'Iventaris Tanah',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="AjukanSurat" component={AjukanSurat} options={{
                title: 'Ajukan Surat',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="FormSurat" component={FormSurat} options={{
                title: 'Form Surat',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="DokumenPersyaratan" component={DokumenPersyaratan} options={{
                title: 'Dokumen Persyaratan',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="TambahDokumen" component={TambahDokumen} options={{
                title: 'Tambah Dokumen',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
         <Stack.Screen name="ArsipSurat" component={ArsipSurat} options={{
                title: 'Arsip Surat',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        <Stack.Screen name="StatusPengajuan" component={StatusPengajuan} options={{
                title: 'Status Pengajuan',
                headerStyle: {
                  backgroundColor: `#${warna}`,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
        {/* <Stack.Screen name="Akun" component={Akun} options={{
            headerShown: false,
            animationEnabled: false
            }} />
        
        <Stack.Screen name="Input" component={Input} options={{ headerShown: false }} /> */}
    </Stack.Navigator>
    )
}