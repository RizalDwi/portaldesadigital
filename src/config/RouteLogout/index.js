import * as React from 'react';
import { View, Text, Image } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './../../screens/Login'
import Logout from './../../screens/Logout'

const Stack = createStackNavigator();

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)

    // React.useEffect(() => {
    //     setTimeout(() => {
    //         setIsLoading(!isLoading)
    //     }, 2000)
    // }, [])

    // if(isLoading) {
    //     return <SplashScreen />
    // }

    return (
       
            <Stack.Navigator>
                <Stack.Screen name="Logout" component={Logout} options={{
                    headerShown: false,
                    animationEnabled: false
                    }} />
                <Stack.Screen name="Login" component={Login} options={{
                    headerShown: false,
                    animationEnabled: false
                    }} />  
            </Stack.Navigator>
    )
}

export default AppNavigation;