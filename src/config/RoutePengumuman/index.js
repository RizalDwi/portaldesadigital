import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Beranda from './../../screens/main/HomeLanding'
import ArtikelPengumuman from './../../screens/main/ArtikelPengumuman'
import Pengumuman from './../../screens/main/pengumuman'

const Stack = createStackNavigator();

export default function index() {
    return (
        <Stack.Navigator>
        
        <Stack.Screen name="Pengumuman" component={Pengumuman} options={{
            headerShown: false
            }} />
        <Stack.Screen name="PengumumanDesa" component={ArtikelPengumuman} options={{
                title: 'Pengumuman Desa',
                headerStyle: {
                  backgroundColor: '#4CC9F0',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                  fontFamily: 'Poppins-Bold'
                }
        }}/>
    </Stack.Navigator>
    )
}