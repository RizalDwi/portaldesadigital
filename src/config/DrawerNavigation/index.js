import React, {useEffect, useState} from 'react'
import { View, Text, Image } from 'react-native'
import { createDrawerNavigator } from '@react-navigation/drawer';
import ProfilUser from './../../screens/user/ProfilUser'
import TabBottomNavigation from './../BottomNavigation'
import KelolaDesaNavigation from './../KelolaDesaNavigation'
import FormBuatDesa from './../../screens/main/FormBuatDesa'
import Routes from './../RouteLogout'
import Asynstorage from '@react-native-community/async-storage'

const Drawer = createDrawerNavigator();

export default function index({navigation}) {

    const [admin, setAdmin] = useState(false)
    const [count, setCount] = useState(0)

    const cekAdmin = async() => {
        var getCekAdmin = await Asynstorage.getItem('admin')
        await Asynstorage.getItem('admin', (error, result) =>{
          if (result){
            setAdmin(getCekAdmin)
          }
        })
    }
    const handleAdmin = (value) => {
      setAdmin(value)
    }

    useEffect(() => {
      cekAdmin()
      const interval = setInterval(() => {
          setCount((count) => count + 1);
        }, 1000);
      const unsubscribe = navigation.addListener('focus', () => {
          setCount(0);
          cekAdmin()
        });
    
        return () => {
          // Clear setInterval in case of screen unmount
          clearTimeout(interval);
          // Unsubscribe for the focus Listener
          unsubscribe;
        };
  
      
    }, [navigation])

    // useEffect(() => {
    //   cekAdmin()
    //   console.log("cek admin")
    // }, [])

    const IconDrawer = (props) => {
        const {color, focused} = props.data
        let colorSelected = focused ? color : 'black'
        return (
          <View>
            <Image source={props.image} style={{width: 40, height: 40, tintColor: colorSelected}} />
          </View>
        )
      }

   

    return (
        <Drawer.Navigator initialRouteName="Home" drawerContentOptions={{
            labelStyle: {
              fontWeight: 'bold',
              fontFamily: 'Poppins-Bold',
              fontSize: 14
            }
          }}>
            <Drawer.Screen name="homeLanding" component={TabBottomNavigation} options={{
                title: 'Beranda',
                drawerIcon: (props) => (
                    <IconDrawer data={props} image={require('./../../assets/icons/homeNew.png')} />
                )
                }} />
            <Drawer.Screen name="profilUser" component={ProfilUser} options={{
                title: 'Profil',
                drawerIcon: (props) => (
                    <IconDrawer data={props} image={require('./../../assets/icons/ProfilDrawer.png')} />
                )
                }} />
                {(() => {
                    if (admin == '1'){
                        return (
                          <Drawer.Screen name="kelolaDesa" component={KelolaDesaNavigation} options={{
                            title: 'Kelola Desa',
                            drawerIcon: (props) => (
                                <IconDrawer data={props} image={require('./../../assets/icons/settingWeb.png')} />
                            )
                            }} />
                        )
                    } else {
                      return (
                        <Drawer.Screen name="FormBuatDesa" component={() => <FormBuatDesa navigation={navigation} handleAdmin={handleAdmin} />} options={{
                          title: 'Buat Portal Desa',
                          drawerIcon: (props) => (
                              <IconDrawer data={props} image={require('./../../assets/icons/settingWeb.png')} />
                          )
                          }} />
                      )
                    }
                })()}

              <Drawer.Screen name="Logout" component={Routes} options={{
                  title: 'Keluar',
                  swipeEnabled: false,
                  drawerIcon: (props) => (
                      <IconDrawer data={props} image={require('./../../assets/icons/logout.png')} />
                  )
                  }} />
          </Drawer.Navigator>
    )
}
