import React, {useState, useContext} from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, TextInput } from 'react-native'
import { RootContext } from './../../../screens/admin/TambahAparatDesa'
import DateTimePicker from '@react-native-community/datetimepicker';
export default function index() {

    const state = useContext(RootContext)
    
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || state.tanggalPengangkatan;
        setShow(Platform.OS === 'ios');
        state.handleInputTanggalPengangkatan(currentDate);
      };
    
      const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
      };
    
      const showDatepicker = () => {
        showMode('date');
      };

    return (
        
        <View>
            {show && (
                <DateTimePicker
                testID="dateTimePicker"
                value={state.tanggalPengangkatan}
                mode={mode}
                display="default"
                onChange={onChange}
                />
            )}
                <Text style={styles.label}>Tanggal SK Pengangkatan</Text>
                <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity onPress={showDatepicker}>
                        <View>
                            <Image source={require('./../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                        </View>
                    </TouchableOpacity>
                    <TextInput placeholder="Judul Pesan" style={styles.formTanggal} value={state.tanggalPengangkatan.toDateString()} editable={false} />
                 </View>
            </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        formTanggal: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC',
            marginLeft: 5
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    }
})