import React, {useEffect, useState} from 'react'
import { View, Text, Image } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'


export default function index() {

    const [warna, setWarna] = useState('')
    const [foto, setFoto] = useState('')
    const [namaDesa, setNamaDesa] = useState('')
    const [alamat, setAlamat] = useState('')

    const getWarna = async() => {
        setWarna(await Asynstorage.getItem('warna_desa'))
        setFoto(await Asynstorage.getItem('foto_desa'))
        setNamaDesa(await Asynstorage.getItem('nama_desa'))
        setAlamat(await Asynstorage.getItem('alamat_desa'))
    }

    useEffect(() => {
        getWarna()
     }, [])

    return (
        <View>
            <View style={{width: '100%', backgroundColor: `#${warna}`, height: 170, justifyContent: 'center', alignItems: 'center'}}>
                <View>
                    <Image source={foto === '' ? require('./../../../../assets/icons/thumnail.png') : {uri: `data:image/png;base64,${foto}`}} style={{width: 70, height: 70, borderRadius: 70/2}} />
                </View>
                <View style={{alignItems: 'center'}}>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 17, color: '#FFFFFF' }}>Desa {namaDesa}</Text>
                    <Text style={{fontFamily: 'Poppins', fontSize: 13, color: '#FFFFFF' }}>{alamat}</Text>
                </View>
            </View>
        </View>
    )
}
