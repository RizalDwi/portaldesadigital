import React, { useState, useEffect } from 'react'
import { View, Text, Image, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'

export default function index() {

    const [foto, setFoto] = useState('')

    async function getData() {
        var foto_user = await Asynstorage.getItem('foto_user')
        try {
            await Asynstorage.getItem('foto_user', (error, result) =>{

              if (result){
                setFoto(foto_user)
                
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => getData());
    },[])


    return (
        <View style={{width: '100%', height: 80, backgroundColor: '#FFFFFF', padding: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                <View>
                    <Text style={{fontSize: 30, fontWeight: 'bold', fontFamily: 'Poppins'}}>Polling</Text>
                </View>
                <View>
                    <Image style={{width: 52, height: 52, borderRadius: 52/2}} source={foto === '' ? require('./../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${foto}`}} />
                </View>
            </View>
        </View>
    )
}
