import React, { useState, useEffect } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
export default function index(props) {

    const [foto, setFoto] = useState('')
    const [username, setUsername] = useState('')

    async function getData() {
        var foto_user = await Asynstorage.getItem('foto_user')
        var name = await Asynstorage.getItem('username')
        setUsername(name)
        try {
            await Asynstorage.getItem('foto_user', (error, result) =>{

              if (result){
                setFoto(foto_user)
                
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => getData());
    },[])


    return (
        <View style={{width: '100%', height: 80, backgroundColor: '#FFFFFF', padding: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                <TouchableOpacity onPress={() => props.navigation.openDrawer('home')}>
                    <View>
                        <Image style={{width: 52, height: 52, borderRadius: 52/2}} source={foto === '' ? require('./../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${foto}`}} />
                    </View>
                </TouchableOpacity>
                <View>
                    <Text style={{fontSize: 30, fontWeight: 'bold', fontFamily: 'Poppins'}}>{username}</Text>
                </View>
            </View>
           
        </View>
    )
}
