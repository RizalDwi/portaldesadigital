import React from 'react'
import { View, Text, Image, TextInput } from 'react-native'

export default function index(props) {
    return (
        <View>
             <View style={{width: '100%', height: 280, backgroundColor: `#${props.warna}`, position: 'absolute', top: 0, left: 0}}>
            </View>
            <View style={{width: '100%', height: 130, paddingHorizontal: 10, paddingTop: 40}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                        <Image style={{width: 52, height: 52, borderRadius: 52/2}} source={props.foto === '' ? require('./../../../../assets/icons/thumnail.png') : {uri: `data:image/png;base64,${props.foto}`}} />
                    </View>
                    <View style={{marginLeft: 20}}>
                        <Text style={{fontSize: 20, fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Kelola Desa {props.namaDesa}</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}
