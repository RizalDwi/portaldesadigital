import React from 'react'
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native'

export default function index(props) {
    return (
        <View>
             <View style={{width: '100%', height: 280, backgroundColor: `#${props.warna}`, position: 'absolute', top: 0, left: 0}}>
            </View>
            <View style={{width: '100%', paddingHorizontal: 10, paddingTop: 10}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                        <Image style={{width: 52, height: 52, borderRadius: 52/2}} source={props.foto === '' ? require('./../../../../assets/icons/thumnail.png') : {uri: `data:image/png;base64,${props.foto}`}} />
                    </View>
                    <View style={{marginLeft: 20}}>
                         <Text style={{fontSize: 20, fontFamily: 'Rubik-Bold', color: '#FFFFFF', textAlign: 'center'}}>{props.followers}</Text>
                        <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', color: '#FFFFFF'}}>Pengikut</Text>
                    </View>
                    <View style={{marginLeft: 20}}>
                        <Text style={{fontSize: 13, fontFamily: 'Rubik-Regular', color: '#FFFFFF'}}>Selamat Datang</Text>
                        <Text style={{fontSize: 17, fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Di Desa {props.namaDesa}</Text>
                </View>
                    
                </View>
                
                <View style={{flexDirection: 'row' , width: '100%', marginVertical: 20, justifyContent: 'center'}}>
                {(() => {
                            if (props.cekFollow){
                                return (
                                    <TouchableOpacity onPress={() => props.followDesa()}>
                                        <View style={{width: 100, height: 30, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center', borderRadius: 15}}>
                                                <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 13, textAlign: 'center'}}>Mengikuti</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            } else {
                                return (
                                    <TouchableOpacity onPress={() => props.followDesa()}>
                                        <View style={{width: 90, height: 30, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center', borderRadius: 15}}>
                                                <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 13, textAlign: 'center'}}>Ikuti</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }
                            
                        })()}
                    
                </View>
                {/* <View style={{flexDirection: 'row' ,borderWidth: 1, borderColor: '#707070', borderRadius: 25, height: 50, width: '100%', backgroundColor: '#FFFFFF', marginVertical: 20, alignItems: 'center'}}>
                    <View>
                        <Image source={require('./../../../../assets/icons/searchNews.png')} style={{width: 24, height: 24, marginLeft: 11}} />
                    </View>
                    <View style={{flex: 1, marginLeft: 10}}>
                        <TextInput placeholder="Cari berita di sini"/>
                    </View>
                </View> */}
            </View>
        </View>
    )
}
