import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default function index(props) {
    return (
        <View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View>
                    <Image source={require('./../../../../assets/images/rijal.jpg')} style={styles.imageProfil} />
                </View>
                <View style={{marginLeft: 10}}>
                    <Text style={styles.textLabel}>Pelapor: {props.nama}</Text>
                </View>
            </View>
            <View style={{marginTop: 10}}>
                <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.textData}>Perihal</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.textData}>: {props.perihal}</Text>
                    </View>
                </View>
                <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.textData}>Judul</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.textData}>: {props.judul}</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    imageProfil: {height: 50, width: 50, borderRadius: 25},
    textLabel: {fontFamily: 'Poppins-Bold', fontSize: 16},
    textData : {fontFamily: 'Poppins', fontSize: 14}
})
