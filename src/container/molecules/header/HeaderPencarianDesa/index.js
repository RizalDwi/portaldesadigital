import React, { useState, useEffect } from 'react'
import { View, Text, Image, TextInput, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'

export default function index(props) {

    const [foto, setFoto] = useState('')

    async function getData() {
        var foto_user = await Asynstorage.getItem('foto_user')
        try {
            await Asynstorage.getItem('foto_user', (error, result) =>{

              if (result){
                setFoto(foto_user)
                
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
        InteractionManager.runAfterInteractions(() => getData());
    },[])

    return (
        <View style={{width: '100%', height: 137, backgroundColor: '#FFFFFF', padding: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                <View>
                    <Text style={{fontSize: 30, fontWeight: 'bold', fontFamily: 'Poppins'}}>Pencarian Desa</Text>
                </View>
                <View>
                    <Image style={{width: 52, height: 52, borderRadius: 52/2}} source={foto === '' ? require('./../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${foto}`}} />
                </View>
            </View>
            <View style={{flexDirection: 'row' ,borderWidth: 1, borderColor: '#707070', borderRadius: 25, height: 50, width: '100%', marginVertical: 10, alignItems: 'center'}}>
                <View>
                    <Image source={require('./../../../../assets/icons/searchNews.png')} style={{width: 24, height: 24, marginLeft: 11}} />
                </View>
                <View style={{flex: 1, marginLeft: 10}}>
                    <TextInput placeholder="Cari berita di sini" onChangeText={(value) => props.getData(value)} />
                </View>
            </View>
        </View>
    )
}
