import React from 'react'
import { View, Text, Image } from 'react-native'

export default function index() {
    return (
        <View style={{flexDirection: 'row', width: '100%', marginBottom: 5}}>
            <View>
                <Image source={require('./../../../../assets/images/rijal.jpg')} style={{width: 50, height: 50, borderRadius: 25}} />
            </View>
            <View style={{flex: 1,height: 55, justifyContent: 'center'}}>
                <View style={{marginLeft: 10}}>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 16, color: '#000103'}}>Rizal</Text>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 15, color: '#989898'}}>Desa Widang</Text>
                </View>
                <View style={{position: 'absolute', right: 0, bottom: 5}}>
                    <Text style={{fontSize: 10, color: '#26325B', fontFamily: 'Poppins'}}>1 Oktober 2020</Text>
                </View>
            </View>
        </View>
    )
}
