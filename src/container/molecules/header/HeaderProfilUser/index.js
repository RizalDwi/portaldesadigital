import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

export default function index(props) {
    return (
        <View>
            <View style={{width: '100%',  height: 300, justifyContent: 'center', alignItems: 'center'}}>
                <View style={{marginVertical: 10}}>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 25, color: '#242134'}}>Profil</Text>
                </View>
                <View>
                    <Image source={props.foto === null ? require('./../../../../assets/images/avatar.jpg') : {uri: `data:image/png;base64,${props.foto}`}} style={{width: 120, height: 120, borderRadius: 120/2}} />
                </View>
                <View style={{alignItems: 'center', width: '100%', marginTop: 20}}>
                    <TouchableOpacity onPress={() => { props.modal(true) }} style={{height: 40, width: '35%', borderColor: '#707070', borderWidth: 2, borderRadius: 20, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 18, color: '#242134' }}>
                            Ubah Foto
                        </Text>
                    </TouchableOpacity>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 29, color: '#242134' }}>{props.name}</Text>
                </View>
            </View>
        </View>
    )
}
