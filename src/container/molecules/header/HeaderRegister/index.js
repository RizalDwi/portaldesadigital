import React from 'react'
import { View, Text } from 'react-native'

export default function index() {
    return (
        <View style={{alignItems: 'center', justifyContent: 'center', marginBottom: 100}}>
            <Text style={{fontSize: 28, fontFamily: 'SFProDisplay'}}>Register Sekarang</Text>
            <Text style={{fontSize: 16, fontFamily: 'SFProDisplay', color: '#8B959A'}}>Silahkan mengisi form di bawah ini untuk register</Text>
        </View>
    )
}
