import React from 'react'
import { View, Text, Image } from 'react-native'

export default function index(props) {
    return (
        <View style={{flexDirection: 'row'}}>
            <View>
                <Image source={props.profil == null ? require('./../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${props.profil}`}} style={{width: 60, height: 60, borderRadius: 30}} />
            </View>
            <View style={{marginLeft: 10, justifyContent: 'center', alignItems: 'center', flex: 1}}>
                <Text style={{fontFamily: 'Poppins-Bold', fontSize:20}}>Nama: {props.nama}</Text>
            </View>
        </View>
    )
}
