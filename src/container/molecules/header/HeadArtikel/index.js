import React, {useEffect} from 'react'
import { View, Text, Image } from 'react-native'

export default function index(props) {
    useEffect(() => {
        console.log("foto desa", props.foto)
        
    },[])
    
    return (
        <View style={{flexDirection: 'row', width: '100%'}}>
            <View>
                <Image source={{uri: `data:image/png;base64,${props.foto}`}} style={{width: 50, height: 50, borderRadius: 25}} />
            </View>
            <View style={{flex: 1,height: 60, justifyContent: 'center'}}>
                <View style={{marginLeft: 10}}>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 16, color: '#000103'}}>{props.namaDesa}</Text>
                </View>
                <View style={{alignItems: 'flex-end'}}>
                    <Text style={{fontSize: 10, color: '#26325B', fontFamily: 'Poppins'}}>{props.tanggal}</Text>
                </View>
            </View>
        </View>
    )
}
