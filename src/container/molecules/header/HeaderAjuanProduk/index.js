import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default function index(props) {
    return (
        <View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View>
                    <Image source={props.foto == null ? require('./../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${props.foto}`}} style={{height: 50, width: 50, borderRadius: 25}} />
                </View>
                <View style={{marginLeft: 10}}>
                    <Text style={{fontFamily: 'Poppins-Bold', fontSize: 16}}>Pemilik: {props.name}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({})
