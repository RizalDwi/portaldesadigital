import React from 'react'
import { View, Text } from 'react-native'

export default function index(props) {
    return (
        <View>
            <View style={{paddingVertical: 10}}>
                <Text style={{fontFamily: 'Poppins-Bold', fontSize: 17}}>
                {props.judul}
                </Text>
            </View>
            <View>
                <Text style={{fontFamily: 'Poppins'}}>
                {props.isi}
                </Text>
            </View>
        </View>
    )
}
