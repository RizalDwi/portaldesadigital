import React from 'react'
import { View, Text, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function index(props) {
    return (
        <View style={{width: '100%', height: 340, paddingHorizontal: 10}}>
            <View style={{backgroundColor: '#FFFFFF', width: '100%', height: 330, borderRadius: 10, paddingVertical: 10}} elevation={3}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 5}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('ProfilDesa',{idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/profile.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Profile Desa</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Pemerintahan', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/pemerintahan.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Pemerintahan</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Iventaris', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/iventaris.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Iventaris</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 5}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('DanaDesa', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/dana.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Dana Desa</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Maps', {idDesa: props.idDesa})}>
                        <Image source={require('./../../../assets/icons/map.png')} style={{width: 40, height: 40}} />
                        <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Peta Desa</Text>
                    </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={() => props.navigation.navigate('KontakDesa', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/kontak.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Kontak</Text>
                        </View>
                    </TouchableOpacity>
                    
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 5}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('PengajuanSurat', {idDesa: props.idDesa, warna: props.warna})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/letter.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Ajukan Surat</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('PengajuanProduk', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/product.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Ajukan Produk</Text>
                        </View>
                    </TouchableOpacity>
                
                    <TouchableOpacity onPress={() => props.navigation.navigate('PengajuanWarga', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/warga.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Ajukan Warga</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 5}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('BantuanDesa', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/trust.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Bantuan</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('InformasiDesa', {idDesa: props.idDesa})}>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('./../../../assets/icons/informasi.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Informasi Desa</Text>
                        </View>
                    </TouchableOpacity>
                
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{width: 40, height: 40}} ></View>
                    </View>
                </View>
            </View>
        </View>
    )
}
