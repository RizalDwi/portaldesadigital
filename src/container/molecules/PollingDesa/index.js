import React, { useState } from 'react'
import { View, Text, Image, ScrollView, StyleSheet, FlatList, Touchable } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Textarea from 'react-native-textarea';
import LoadingScreen from './../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import ShowChart from './../konten/KontenShowChart'

export default function index(props) {
    const [alasan, setAlasan] = useState('')
    const [jawaban, setJawaban] = useState('')
    const [loading, setLoading] = useState(false)
    const [showHasil, setShowHasil] = useState(false)
    
    const hasil = () => {
        showHasil? setShowHasil(false) : setShowHasil(true)
    }

    const addJawaban = async() => {

        if((alasan == '') || (jawaban == '')){
            alert('Isi data terlebih dahulu')
        } else {
            var token = await Asynstorage.getItem('api_token')
            var id_user = await Asynstorage.getItem('id_user')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{

                if (result){

                    let data = {
                        lokasi: props.lokasi,
                        alasan: alasan,
                        id_users: id_user,
                        id_pilihan: jawaban,
                        id_polling: props.dataPolling.polling.id,
                        id_desa: props.idDesa
                    }

                    setLoading(true)

                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_jawaban', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        props.handleDataPolling(res.data.polling)
                        console.log("Polling 2 ->", res)
                        setAlasan('')
                        setJawaban('')
                        setLoading(false)
                        
                    })
                    .catch((err) => {
                        setLoading(false)
                        alert(err)
                    })
                }
                })
    
            } catch(err) {
                console.log("Logout -> error : ",err)
            }
        }
      }

      if(showHasil){
          return (
            <View style={{width: 350, height: 350, backgroundColor: '#FFFFFF',padding: 15, marginHorizontal: 5}}>
                <ShowChart idPolling={props.dataPolling.polling.id} pertanyaan={props.dataPolling.polling.pertanyaan} hasil={hasil} />
            </View>
          )
      }


    return (
        <View style={{width: 350, height: 350, backgroundColor: '#FFFFFF',padding: 15, marginHorizontal: 5}}>
                {(() => {
                            if (loading){
                                return <LoadingScreen />
                            } else {
                                return (
                                    <View style={{flex: 1}}>
                                         <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', marginBottom: 10}}>
                                            {props.dataPolling.polling.pertanyaan}
                                        </Text>
                                        {(() => {
                                                    if (props.dataPolling.cekJawaban == 1){
                                                        return (
                                                            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                                                <Text>Anda sudah menjawab pertanyaan ini</Text>
                                                            </View>
                                                        )
                                                    } else {
                                                        return (
                                                            <View style={{flex: 1}}>
                                                                <View>
                                                                    <RadioForm
                                                                        radio_props={props.dataPolling.pilihan}
                                                                        buttonColor={'#000000'}
                                                                        buttonSize={12}
                                                                        initial={-1}
                                                                        onPress={(value) => setJawaban(value) }
                                                                        />
                                                                </View>
                                                                <View style={{marginVertical: 10}}>
                                                                <Textarea
                                                                    containerStyle={styles.textareaContainer}
                                                                    style={styles.textarea}
                                                                    onChangeText={(value) => setAlasan(value)}
                                                                    maxLength={120}
                                                                    placeholder={'Tuliskan alasan anda di sini'}
                                                                    placeholderTextColor={'#c7c7c7'}
                                                                    underlineColorAndroid={'transparent'}
                                                                />
                                                                </View>
                                                            </View>
                                                        )
                                                    }
                                        })()}
                                        
                                        <View style={{flexDirection: 'row'}}>
                                        {(() => {
                                                    if (props.dataPolling.cekJawaban != 1){
                                                        return (
                                                            <TouchableOpacity onPress={() => addJawaban()}>
                                                                <View style={{backgroundColor: '#707070', width: 60, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 15}}>
                                                                    <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', color: '#FFFFFF'}}>
                                                                        Vote
                                                                    </Text>
                                                                </View>
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                        })()}
                                            <TouchableOpacity onPress={() => hasil()}>
                                                <View style={{marginLeft: 10 ,backgroundColor: '#35BEE0', width: 110, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 15}}>
                                                    <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', color: '#FFFFFF'}}>
                                                        Lihat Hasil
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        
                                        </View>
                                    </View>
                                )
                            }
                })()}
            </View>  
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 30,
      justifyContent: 'center',
      alignItems: 'center',
    },
    textareaContainer: {
      height: 140,
      padding: 5,
      backgroundColor: '#F5FCFF',
    },
    textarea: {
      textAlignVertical: 'top',  // hack android
      height: 130,
      fontSize: 14,
      color: '#333',
    },
  });