import React from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView, FlatList } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MaterialComunity from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Tanah from './../Tanah'
import Elektronik from './../Elektronik'
import Gedung from './../Bangunan'
import Jaringan from './../Jaringan'
import Konstruksi from './../Konstruksi'
import Lain from './../Aset'
import Mesin from './../Mesin'

const Tab = createMaterialTopTabNavigator();
export default function index(props) {

    return (
          <Tab.Navigator
              tabBarOptions={{
                showLabel: false,
                showIcon: true,
              }}
          >
            <Tab.Screen name="Mesin" component={Mesin} 
              options={{
                tabBarIcon: (tabInfo) => (
                  <MaterialComunity name="engine" size={25} color={tabInfo.tintColor} />
                ),
              }}
            />
            <Tab.Screen name="Elektronik" component={Elektronik} 
              options={{
                tabBarIcon: (tabInfo) => (
                  <MaterialIcons name="smartphone" size={25} color={tabInfo.tintColor} />
                ),
              }}
            />
        
            <Tab.Screen name="Gedung" component={Gedung} 
                options={{
                    tabBarIcon: (tabInfo) => (
                    <MaterialComunity name="office-building" size={25} color={tabInfo.tintColor} />
                    ),
                }}
                />

            <Tab.Screen name="Tanah" component={Tanah} 
                options={{
                    tabBarIcon: (tabInfo) => (
                    <MaterialComunity name="terrain" size={25} color={tabInfo.tintColor} />
                    ),
                }}
                />
            <Tab.Screen name="Jaringan" component={Jaringan} 
                options={{
                    tabBarIcon: (tabInfo) => (
                    <MaterialComunity name="road-variant" size={25} color={tabInfo.tintColor} />
                    ),
                }}
                />
            
            <Tab.Screen name="Konstruksi" component={Konstruksi} 
              options={{
                tabBarIcon: (tabInfo) => (
                  <MaterialIcons name="construction" size={25} color={tabInfo.tintColor} />
                ),
              }}
            />
            <Tab.Screen name="Lain" component={Lain} 
              options={{
                tabBarIcon: (tabInfo) => (
                  <MaterialIcons name="more-horiz" size={25} color={tabInfo.tintColor} />
                ),
              }}
            />
          </Tab.Navigator>
    )
}
