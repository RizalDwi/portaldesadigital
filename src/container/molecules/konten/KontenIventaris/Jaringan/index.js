import React, {useState, useEffect} from 'react'
import { View, Text, FlatList, ScrollView, StyleSheet, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {
    
    const [dataIventaris, setDataIventaris] = useState([])

    const [warna, setWarna] = useState('')

    async function getDataIventaris() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_iventaris_jaringan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataIventaris(res.data.data)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const getWarna = async() => {
        setWarna(await Asynstorage.getItem('warna_desa'))
    }

    useEffect(() => {
        getWarna()
        InteractionManager.runAfterInteractions(() => {
            getDataIventaris()
            
          });
      }, [])

    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
            <View style={{flex:1}}>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Nama Barang</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.nama_barang}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Kode</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.kode_barang}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Nomor Registrasi</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.nomor_register}</Text>
                    </View>
                </View>
            
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Panjang</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.panjang} KM</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Lebar</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.lebar} M</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Luas</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.luas} M2</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Lokasi</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.letak_atau_alamat}</Text>
                    </View>
                </View>
              
                
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Sumber Dana</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>: {item.asal_usul_barang}</Text>
                    </View>
                </View>
                
            </View>
        </View>
            // <View style={{flexDirection: 'row'}}>
            //             <View style={styles.rowNomor}>
            //                 <Text style={styles.labelRow}>{index + 1}</Text>
            //             </View>
            //             <View style={styles.rowAksi}>
            //             <Text style={styles.labelRow}>{item.nama_barang}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.kode_barang}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.tanggal_kepemilikan}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.asal_usul_barang}</Text>
            //             </View>
            //         </View>
        )
    }

    return (
        <View style={{flex: 1, padding: 10}}>

            <ScrollView>
            <FlatList data={dataIventaris} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}

const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10}
  });
