import React from 'react'
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native'
import LoadingScreen from './../../../../screens/LoadingScreen'

export default function index(props) {

    const renderItem = ({item, index}) => {
        return (
            <View style={{marginTop: 10}}>
                <TouchableOpacity onPress={() => props.navigationDesa(item.id)}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={{uri: `data:image/png;base64,${item.foto}`}} style={{height: 50, width: 50, borderRadius: 25}} />
                    <View style={{marginLeft: 10}}>
                            <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>Desa {item.nama_desa}</Text>
                            <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Kab. {item.kabupaten}, {item.provinsi}</Text>
                    </View>
                </View>
                </TouchableOpacity>
            </View>
        )
    }

    if(props.loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex:1, padding: 20}}>
            <FlatList data={props.listDesa} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </View>
    )
}
