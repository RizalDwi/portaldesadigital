import React, {useEffect, useState} from 'react'
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native'

export default function index(props) {

    const renderItem = ({item, index}) => {

        var today = new Date(item.created_at);
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        today = `${dd}-${mm}-${yyyy}`;
        return (
            <View style={{width: '100%'}}>
                
            <TouchableOpacity onPress={() => props.navigation.navigate('ProdukWarga', {id: item.id})} style={{width: '100%', backgroundColor: '#FFFFFF', padding: 10, borderRadius: 10, marginVertical: 5}} elevation={3} >
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View>
                            <Image source={{uri: `data:image/png;base64,${item.foto_profil}`}} style={{width: 50, height: 50, borderRadius: 25}} />
                        </View>
                        <View style={{marginLeft: 10}}>
                            <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.name}</Text>
                            <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Desa {item.nama_desa}</Text>
                            
                        </View>
                    </View>
                    <View style={{marginVertical: 10}}>
                        <Image source={{uri: `data:image/png;base64,${item.gambar_produk}`}} style={{width: '100%', height: 220}} />
                    </View>
                    <View>
                        <Text style={{fontSize: 16}}>{item.judul_produk}</Text>
                    </View>
                    <View style={{marginTop: 17}}>
                        <Text style={{fontSize: 14, color: '#26325B'}}>{today}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )

    }

    // const renderItem = ({ item }) => {
    //     return (
    //         <View style={{width: '100%', height: 390, backgroundColor: '#FFFFFF', padding: 10, borderRadius: 10, marginVertical: 5}} elevation={3} >
    //             <View style={{flexDirection: 'row', alignItems: 'center'}}>
    //                 <View>
    //                     <Image source={require('./../../../assets/images/widang.jpg')} style={{width: 50, height: 50, borderRadius: 25}} />
    //                 </View>
    //                 <View style={{marginLeft: 10}}>
    //                     <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>Desa Widang</Text>
    //                     <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Kab. Tuban, Jawa Timur</Text>
    //                 </View>
    //             </View>
    //             <View style={{marginVertical: 10}}>
    //                 <Image source={require('./../../../assets/images/upacara.jpg')} style={{width: '100%', height: 220}} />
    //             </View>
    //             <View>
    //                 <Text style={{fontSize: 16}}>{item.isi}</Text>
    //             </View>
    //             <View style={{marginTop: 17}}>
    //                 <Text style={{fontSize: 14, color: '#26325B'}}>1 Oktober 2019</Text>
    //             </View>
    //         </View>
    //     )
    // }

    return (
        <ScrollView style={{flex: 1, padding: 10}}>
            <FlatList data={props.listProduk} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </ScrollView>
    ) 
}
