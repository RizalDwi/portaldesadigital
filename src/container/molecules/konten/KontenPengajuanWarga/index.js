import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Image, InteractionManager, Modal, ToastAndroid, StyleSheet  } from 'react-native'
import Form from '../../form/FormPengajuanWarga'
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import ImgToBase64 from 'react-native-image-base64';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index(props) {

    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const [nama, setNama] = useState('')
    const [nik, setNik] = useState('')
    const [kk, setKK] = useState('')
    const [telp, setTelp] = useState('')
    const [alamat, setAlamat] = useState('')
    const [tanggal, setTanggal] = useState(new Date())
    const [loading, setLoading] = useState(true)
    const [status, setStatus] = useState('')

    const handleNama = (value) => {
        setNama(value)
    }
    const handleNik = (value) => {
        setNik(value)
    }
    const handleKK = (value) => {
        setKK(value)
    }

    const handleTelp = (value) => {
        setTelp(value)
    }

    const handleAlamat = (value) => {
        setAlamat(value)
    }

    const handleTanggal = (value) => {
        setTanggal(value)
    }

    const toggleCamera = () => {
        setType(type === 'back' ? 'front' : 'back')
    }

    const takePicture = async() => {
        const options = { quality: 0.5, base64: true}
        if (camera) {
            const data = await camera.takePictureAsync(options)
            console.log("TakePicture -> data", data)
            ImgToBase64.getBase64String(data.uri)
                .then(base64String => {
                    setPhoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
            setIsVisible(false)
        }
    }

    async function addPengajuanWarga() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    nama_lengkap: nama,
                    nik: nik,
                    no_kk: kk,
                    no_telp: telp,
                    alamat: alamat,
                    tanggal_lahir: tanggal,
                    foto_pengajuan: photo,
                    id_users: id_user,
                    id_desa: props.idDesa
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tambah_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setStatus("Proses")
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function cek_warga() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_users: id_user,
                    id_desa: props.idDesa
                }
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/cek_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setStatus(res.data.status)
                    console.log(res.data.status)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function deleteAjuan() {
        var token = await Asynstorage.getItem('api_token')
        var id_user = await Asynstorage.getItem('id_user')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_users: id_user,
                    id_desa: props.idDesa
                }
                
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/delete_pengajuan_user', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    // setJudul('')
                    // setIsi('')
                    // setFoto('')
                    setStatus(res.data.status)
                    console.log(res.data.status)
                    setLoading(false)
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            cek_warga()
          });
        
      }, [])


    // const uploadImage = (uri) => {
    //     const sessionID = new Date().getTime()
    //     return storage()
    //     .ref(`image/${sessionID}`)
    //     .putFile(uri)
    //     .then((response) => {
    //         alert('sukses')
    //     })
    //     .catch((error) => {
    //         alert('error')
    //     })
    // }
    const showToast = () => {
        ToastAndroid.show("Data sedang diajukan", ToastAndroid.SHORT);
      };

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={ref => {
                            camera = ref;
                        }}
                    >

                    <View style={{width: 40, height: 40, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', borderRadius: 20, marginTop: 20, marginLeft: 20}}>
                        <TouchableOpacity onPress={() => toggleCamera()}>
                            <MaterialCommunity name='rotate-3d-variant' size={20}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <View style={{width: '60%', height: 290, borderRadius: 100, borderColor: '#FFFFFF', borderWidth: 2}}></View>
                        <View style={{width: '60%', height: 140, borderColor: '#FFFFFF', borderWidth: 2, marginTop: 70}}></View>
                    </View>
                    <View style={{position: 'absolute', width: '100%', height: 70, bottom: 10, alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => takePicture()}>
                            <View style={{width: 70, height: 70, borderWidth: 2,borderColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', borderRadius: 70/2}}>
                                <View style={{width: 60, height: 60, borderRadius: 30, backgroundColor: '#FFFFFF'}}>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    if(loading) {
        return <LoadingScreen />
    }

    if (status == "Proses"){
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20}}>
                <Text style={{fontFamily: 'Rubik-Regular', fontSize: 17, textAlign: 'center'}}>Pengajuan anda sedang diproses, silahkan tunggu</Text>
            </View>
        )
    }

    if(status == "Ditolak"){
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20}}>
                <Text style={{fontFamily: 'Rubik-Regular', fontSize: 17, textAlign: 'center'}}>Pengajuan anda ditolak, silahkan kirim data lagi dan pastikan data anda benar</Text>
                <TouchableOpacity style={{marginTop: 20}} onPress={() => deleteAjuan()}>
                    <View style={{height: 40, width: 120, backgroundColor: '#2286FF', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 15}}>Ajukan Lagi</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
    if(status == "Diterima"){
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20}}>
                <Text style={{fontFamily: 'Rubik-Regular', fontSize: 17, textAlign: 'center'}}>Anda sudah menjadi warga desa ini</Text>
                
            </View>
        )
    }

    return (
        
        <View style={{padding: 10}}>
            {renderCamera()}
            <Form nama={handleNama} nik={handleNik} kk={handleKK} telp={handleTelp} alamat={handleAlamat} tanggal={handleTanggal} />
            <View>
                <Text style={styles.label}>Foto dengan KTP</Text>
                <TouchableOpacity onPress={() => setIsVisible(true)} style={{height: 40, width: 180, backgroundColor: '#2286FF', justifyContent: 'center', alignItems: 'center', borderRadius: 20}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#FFFFFF', fontFamily: 'Poppins-Bold'}}>Ambil Gambar</Text>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 10, paddingHorizontal: '10%'}}>
                <Image source={photo === null ? require('./../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${photo}`}} style={{width: 300, height: 400}} />
            </View>
            <TouchableOpacity style={{width: '100%'}} onPress={() => addPengajuanWarga()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                    <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Ajukan</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    }
})