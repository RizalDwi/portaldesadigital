import React from 'react'
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native'

export default function index(props) {


    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity  onPress={() => props.navigation.navigate('PengumumanDesa', {id: item.id})} style={{width: '100%', height: 370, backgroundColor: '#FFFFFF', padding: 10, borderRadius: 10, marginVertical: 5}} elevation={3} key={index} >
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                        <Image source={{uri: `data:image/png;base64,${item.foto_desa}`}} style={{width: 50, height: 50, borderRadius: 25}} />
                    </View>
                    <View style={{marginLeft: 10}}>
                        <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.nama_desa}</Text>
                        <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Kab. {item.kabupaten}, {item.provinsi}</Text>
                    </View>
                </View>
                <View style={{marginVertical: 10}}>
                    <Image source={{uri: `data:image/png;base64,${item.foto_pengumuman}`}} style={{width: '100%', height: 220}} />
                </View>
                <View>
                    <Text style={{fontSize: 16}}>{item.judul}</Text>
                </View>
                <View style={{marginTop: 17}}>
                    <Text style={{fontSize: 14, color: '#26325B'}}>{item.waktu}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <ScrollView style={{flex: 1, padding: 10}}>
           <FlatList data={props.dataPengumuman} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </ScrollView>
    ) 
}
