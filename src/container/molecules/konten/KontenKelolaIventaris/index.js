import React, {useState, useEffect} from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'

export default function index(props) {

    const [warna, setWarna] = useState('')
    
    const getWarna = async() => {
        setWarna(await Asynstorage.getItem('warna_desa'))
        console.log("Warna ->", await Asynstorage.getItem('warna_desa'))
    }
 
    useEffect(() => {
        getWarna()
     }, [])

    return (
        <View style={{flex: 1, padding: 10}}>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisMesin', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Mesin</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisElektronik', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Elektronik</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisBangunan', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Gedung</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisJaringan', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Jalan, Irigasi, dan Jaringan</Text>
                    </View>
                </TouchableOpacity>
            </View>

            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisTanah', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Tanah</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisKontruksi',{idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Konstruksi Dalam Pengerjaan</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('IventarisAset', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Aset Lainya</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
