import React from 'react'
import { View, Text, Image, ScrollView, StyleSheet, FlatList } from 'react-native'
import PollingDesa from './../../PollingDesa'

export default function index(props) {

    const renderItem = ({ item, index }) => {
        return (
            <View style={{marginVertical: 5}}>
                <View style={{flexDirection: 'row', alignItems: 'center', backgroundColor: '#FFFFFF', marginLeft: 5}}>
                    <View style={{marginLeft: 10, marginTop: 10}}>
                        <Image source={{uri: `data:image/png;base64,${item.polling.foto}`}} style={{width: 50, height: 50, borderRadius: 25}} />
                    </View>
                    <View style={{marginLeft: 10}}>
                        <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>Desa {item.polling.nama_desa}</Text>
                        <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Kab. {item.polling.kabupaten}, {item.polling.provinsi}</Text>
                    </View>
                </View>
                <PollingDesa dataPolling={item} handleDataPolling={props.handleDataPolling} idDesa={item.idDesa} lokasi="beranda" />
            </View>
        )
    }

    return (
        <ScrollView style={{flex: 1, padding: 10}}>
           <FlatList data={props.dataPolling} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </ScrollView>
    ) 
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 30,
      justifyContent: 'center',
      alignItems: 'center',
    },
    textareaContainer: {
      height: 140,
      padding: 5,
      backgroundColor: '#F5FCFF',
    },
    textarea: {
      textAlignVertical: 'top',  // hack android
      height: 130,
      fontSize: 14,
      color: '#333',
    },
  });

  {/* <FlatList
            data={arrayBerita}
            renderItem={renderItem}
           /> */}
        //    <View style={{width: '100%', height: 420, backgroundColor: '#FFFFFF', padding: 10, borderRadius: 10, marginVertical: 5}} elevation={3} >
        //         <View style={{flexDirection: 'row', alignItems: 'center'}}>
        //             <View>
        //                 <Image source={require('./../../../../assets/images/widang.jpg')} style={{width: 50, height: 50, borderRadius: 25}} />
        //             </View>
        //             <View style={{marginLeft: 10}}>
        //                 <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>Desa Widang</Text>
        //                 <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Kab. Tuban, Jawa Timur</Text>
        //             </View>
        //         </View>
        //         <View style={{width: '100%',padding: 15}}>
        //                 <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', marginBottom: 10}}>
        //                     Bagaimana Pelayanan dari Pemerintah Kelurahan?
        //                 </Text>
        //                 <View>
        //                     <RadioForm
        //                         radio_props={radio_props}
        //                         initial={0}
        //                         buttonColor={'#000000'}
        //                         buttonSize={12}
        //                         onPress={(value) => setRadioValue(true) }
        //                         />
        //                 </View>
        //                 <View style={{marginVertical: 10}}>
        //                 <Textarea
        //                     containerStyle={styles.textareaContainer}
        //                     style={styles.textarea}
                           
        //                     maxLength={120}
        //                     placeholder={'Tuliskan alasan anda di sini'}
        //                     placeholderTextColor={'#c7c7c7'}
        //                     underlineColorAndroid={'transparent'}
        //                 />
        //                 </View>
        //                 <View style={{flexDirection: 'row'}}>
        //                     <View style={{backgroundColor: '#707070', width: 60, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 15}}>
        //                         <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', color: '#FFFFFF'}}>
        //                             Vote
        //                         </Text>
        //                     </View>
        //                     <View style={{marginLeft: 10 ,backgroundColor: '#35BEE0', width: 110, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 15}}>
        //                         <Text style={{fontSize: 17, fontFamily: 'Rubik-Regular', color: '#FFFFFF'}}>
        //                             Lihat Hasil
        //                         </Text>
        //                     </View>
        //                 </View>
        //             </View>
        //     </View>