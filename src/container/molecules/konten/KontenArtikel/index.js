import React from 'react'
import { View, Text, Image, ScrollView} from 'react-native'
import HeadArtikel from '../../header/HeadArtikel'
import Artikel from '../../Artikel'

export default function index(props) {
    return (
        <ScrollView style={{flex: 1}}>
            <HeadArtikel tanggal={props.tanggal} namaDesa={props.namaDesa} foto={props.foto} />
            <Image source={{uri: `data:image/png;base64,${props.fotoArtikel}`}} style={{height: 220, width: '100%'}} />
            <Artikel judul={props.judul} isi={props.isi} />
        </ScrollView>
    )
}
