import React, {useState, useEffect, useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, Touchable } from 'react-native'
import { RootContext } from './../../../../../screens/desa/PengajuanSurat'

export default function index({navigation}) {
    
    const state = useContext(RootContext)

    const renderItem = ({ item, index }) => {

            return (
                <View style={styles.cardContainer}>
                    <View style={{flex:1}}>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Jenis Surat</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.nama_jenis}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanggal</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.created_at}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nomor Surat</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.nomor_surat}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanda Tangan</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.tanda_tangan}</Text>
                            </View>
                        </View>
                        
                        <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                             <TouchableOpacity onPress={() => navigation.navigate('LihatSurat', {id: item.id})}>
                                <View style={{backgroundColor: '#4CC9F0', width: 90, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Lihat Surat</Text>
                                </View>
                            </TouchableOpacity>
                            
                        </View>
                    </View>
                        
                </View>
                // <View style={{width: '100%', height: 45, borderWidth: 1, borderColor: '#707070', flexDirection: 'row' }}>
                //     <View style={styles.rowTableAksi}>
                //     <TouchableOpacity onPress={() => navigation.navigate('LihatSurat', {id: item.id})} style={{flex: 1}}>
                //         <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                //                 <View style={{backgroundColor: '#4CC9F0', justifyContent: 'center', height: '70%', width: '97%', borderRadius: 5}}>
                //                     <Text style={styles.fontStatus}>Lihat Surat</Text>
                //                 </View>
                //             </View>
                //             </TouchableOpacity>
                //     </View>
                    
                //     <View style={styles.rowTableTanggal}>
                //         <Text style={styles.rowTableFont}>{item.created_at}</Text>
                //     </View>
                //     <View style={styles.rowTableJenis}>
                //         <Text style={styles.rowTableFont}>{item.nama_jenis}</Text>
                //     </View>
                //     <View style={styles.rowTableNomorSurat}>
                //         <Text style={styles.rowTableFont}>{item.nomor_surat}</Text>
                //     </View>
                //     <View style={styles.rowTableTtd}>
                //         <Text style={styles.rowTableFont}>{item.tanda_tangan}</Text>
                //     </View>
                // </View>
            )
        
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <FlatList data={state.arsip} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />    
            </ScrollView>
            
        </View>
    )
}

const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10},
    fontStatus : {fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 15, textAlign: 'center'}
  });