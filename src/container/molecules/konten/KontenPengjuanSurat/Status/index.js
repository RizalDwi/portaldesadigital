import React, {useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList } from 'react-native'
import { RootContext } from './../../../../../screens/desa/PengajuanSurat'

export default function index() {
    
    const state = useContext(RootContext)

    const renderItem = ({ item, index }) => {
            return (
                <View style={styles.cardContainer}>
                    <View style={{flex:1}}>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Jenis Surat</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.nama_jenis}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanggal</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.created_at}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Kepentingan</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.kepentingan}</Text>
                            </View>
                        </View>
                       
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Status</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                            <View style={item.status == "Dalam Proses" ? styles.wrapStatusProses : item.status == "Sedang Diteruskan" ? styles.wrapStatusTeruskan : styles.wrapStatusTolak}>
                                 <Text style={styles.fontStatus}>{item.status}</Text>
                             </View>
                            </View>
                        </View>
                    </View>
                        
                </View>
                
            )
    }

    return (
        <ScrollView style={{flex: 1, padding: 20}}>
            <ScrollView>
            <FlatList data={state.pengajuan} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10},
    fontStatus : {fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 13, textAlign: 'center'},
    wrapStatusTolak : {backgroundColor: '#BB2424', justifyContent: 'center', width: '70%', borderRadius: 5},
    wrapStatusProses : {backgroundColor: '#4CC9F0', justifyContent: 'center', width: '70%', borderRadius: 5},
    wrapStatusTeruskan : {backgroundColor: '#26A718', justifyContent: 'center', borderRadius: 5, paddingHorizontal: 5}
  });