import React from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Pengajuan from './../Pengajuan'
import Status from './../Status'
import Arsip from './../Arsip'

const Tab = createMaterialTopTabNavigator();
export default function index() {

    return (
          <Tab.Navigator>
            <Tab.Screen name="Pengajuan" component={Pengajuan} />
            <Tab.Screen name="Status" component={Status} />
            <Tab.Screen name="Arsip" component={Arsip} />           
          </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    title: { height: 40, backgroundColor: '#E5E5E5', padding: 10 },
    head: { height: 40, backgroundColor: '#F5F5F5' },
    text: { margin: 6 },
    dataWrapper: { marginTop: -1},
    scene: {
        flex: 1,
      }
  });