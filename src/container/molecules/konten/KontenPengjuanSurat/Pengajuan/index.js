import React, {useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, Touchable } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Value } from 'react-native-reanimated';
import Textarea from 'react-native-textarea';
import { RootContext } from './../../../../../screens/desa/PengajuanSurat'

export default function index() {
    const state = useContext(RootContext)
    return (
        <View style={{flex: 1, padding: 20, borderColor: 'pink'}}>
            <ScrollView>
            <View>
                <Text style={styles.label}>Perihal</Text>
                <DropDownPicker
                    items={state.jenis}
                
                    containerStyle={{ height: 40 }}
                    style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    dropDownMaxHeight={300}
                    labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    onChangeItem = {(value) => state.handleIdJenis(value.value)}
                />
                <View></View>
           
            </View>
            <View>
                <Text style={styles.label}>Tulis Kepentingan</Text>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Tulis Kepentingan'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        onChangeText = {(value) => state.handleKepentingan(value)}
                    />
                </View>
            </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.addPengajuan()}>
                    <View style={{width: '100%', height: 50, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Ajukan Surat</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 60,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
})
