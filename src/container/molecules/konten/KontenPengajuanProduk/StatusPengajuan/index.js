import React, {useState, useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image, TouchableOpacity } from 'react-native'
import { RootContext } from './../../../../../screens/desa/PengajuanProduk'

export default function index() {
    const state = useContext(RootContext)    

    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
                    <View style={{flex:1}}>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nama Produk</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.judul_produk}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanggal</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.created_at}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Status</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                            <View style={item.status == "Proses" ? styles.wrapTextProsses : item.status == "Ditolak"?  styles.wrapTextDitolak : styles.wrapTextDiterima}>
                                 <Text style={styles.labelRowStatus}>{item.status}</Text>
                             </View>
                            </View>
                        </View>
                        {(() => {
                            if (item.alasan != null){
                                return (
                                    <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                                            <TouchableOpacity onPress={() => state.showModal(item.alasan) }>
                                            <View style={{backgroundColor: '#4CC9F0', width: 90, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                                <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Lihat Alasan</Text>
                                            </View>
                                        </TouchableOpacity>
                                        
                                    </View>
                                )
                            }
                        })()}
                        
                    </View>
                        
                </View>
            // <View style={{flexDirection: 'row'}}>
            //             <View style={styles.rowNomor}>
            //                 <Text style={styles.labelRow}>{index + 1}</Text>
            //             </View>
            //             <View style={styles.rowAksi}>
            //             {(() => {
            //                 if (item.alasan != null){
            //                     return (
            //                         <View style={styles.button}>
            //                             <TouchableOpacity onPress={() => state.showModal(item.alasan) }>
            //                                 <Image source={require('./../../../../../assets/icons/more_detail.png')} style={{width: 40, height: 40}} />
            //                             </TouchableOpacity>
            //                         </View>
            //                     )
            //                 }
            //             })()}
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.judul_produk}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <Text style={styles.labelRow}>{item.created_at}</Text>
            //             </View>
            //             <View style={styles.row}>
            //                 <View style={item.status == "Proses" ? styles.wrapTextProsses : item.status == "Ditolak"?  styles.wrapTextDitolak : styles.wrapTextDiterima}>
            //                     <Text style={styles.labelRowStatus}>{item.status}</Text>
            //                 </View>
            //             </View>
            //         </View>
        )
    }

    return (
        <View style={{flex: 1, padding: 10}}>
            <ScrollView>
                 <FlatList data={state.listStatus} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}



const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10},
    wrapTextProsses: {
        alignItems: 'center',
        width: 100,
        backgroundColor: '#4CC9F0'
    },
    wrapTextDitolak: {
        alignItems: 'center',
        width: 100,
        backgroundColor: '#BB2424'
    },
    wrapTextDiterima: {
        alignItems: 'center',
        width: 100,
        backgroundColor: '#26A718'
    },
    
})

