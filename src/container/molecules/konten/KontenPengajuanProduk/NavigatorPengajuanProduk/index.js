import React from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import FormPengajuan from './../../../form/FormPengajuanProduk'
import StatusPengajuan from './../StatusPengajuan'

const Tab = createMaterialTopTabNavigator();
export default function index() {

    return (
          <Tab.Navigator   tabBarOptions={{
                labelStyle: { fontFamily: 'Poppins-Bold' }
            }}>
            <Tab.Screen name="Form" component={FormPengajuan} options={{
                tabBarLabel: "Form Pengajuan"
            }} />
            <Tab.Screen name="Status" component={StatusPengajuan} options={{
                tabBarLabel: "Status Pengajuan"
            }} />
          </Tab.Navigator>
    )
}





const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    title: { height: 40, backgroundColor: '#E5E5E5', padding: 10 },
    head: { height: 40, backgroundColor: '#F5F5F5' },
    text: { margin: 6 },
    dataWrapper: { marginTop: -1},
    scene: {
        flex: 1,
      }
  });