import React from 'react'
import { View, Text, Image, ScrollView} from 'react-native'
import HeadArtikel from '../../header/HeadProdukWarga'
import Artikel from '../../Artikel'

export default function index(props) {
    return (
        <ScrollView style={{flex: 1, padding: 20}}>
            <Image source={{uri: `data:image/png;base64,${props.foto}`}} style={{height: 220, width: '100%'}} />
            <Artikel judul= {props.judul} isi={props.deskripsi} />
        </ScrollView>
    )
}
