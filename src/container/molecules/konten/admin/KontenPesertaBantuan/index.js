import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

export default function index(props) {
    return (
        <View>
            <View style={{flexDirection: 'row', marginBottom: 10}}>
                <View style={{flex: 1}}>
                    <Text style={styles.text}>Nama Program</Text>
                    <Text style={styles.text}>Sasaran Peserta</Text>
                    <Text style={styles.text}>Masa Berlaku</Text>
                    <Text style={styles.text}>Keterangan</Text>
                </View>
                <View style={{paddingLeft: 20, flex: 1}}>
                    <Text style={styles.text}>: BPNT</Text>
                    <Text style={styles.text}>: Keluarga / KK</Text>
                    <Text style={styles.text}>: 13/12/2015 - 13/12/2017</Text>
                    <Text style={styles.text}>: Bantuan Pemerintah Non Tunai</Text>
                </View>
            </View>
            <View>
                <TouchableOpacity onPress={() => props.navigation.navigate('TambahPeserta', {id: props.id, idDesa: props.idDesa})}>
                    <View style={[styles.button,{ backgroundColor: `#${props.warna}`}]}>
                        <Text style={styles.textButton}>Tambah Peserta</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins-Bold', fontSize: 14},
    button: {backgroundColor: '#35BEE0', width: '45%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2},
    textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
})