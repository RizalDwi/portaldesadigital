import React, {useContext} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, TextInput } from 'react-native'
import { RootContext } from './../../../../../../screens/admin/PengajuanWarga'

export default function index({navigation}) {

    const state = useContext(RootContext)

    const renderItem = ({item, index}) => {
        return (
            <View style={{borderColor: '#DFDFDF', borderBottomWidth: 1, marginVertical: 5}}>
                    <TouchableOpacity onPress={() => navigation.navigate('DetailWarga',{id: item.id})} style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                        <Image source={item.foto == null ? require('./../../../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${item.foto}`}} style={{height: 70, width: 70, borderRadius: 70/2}} />
                        <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.nama_lengkap}</Text>
                                <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>{item.nik}</Text>
                                <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.alamat}</Text>
                        </View>
                    </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{flex:1, padding: 20}}>
            
            <FlatList data={state.listWarga} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </View>
    )
}

const styles = StyleSheet.create({
    textButoon: {fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18},
    button: {backgroundColor: '#4CC9F0', width: 120, height: 40, marginTop: 10, justifyContent: 'center', alignItems: 'center'}
})


