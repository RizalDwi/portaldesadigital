import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'

export default function index(props) {

    return (
        <View style={{marginTop: 10}}>
            <View style={{width: '100%', flexDirection: 'row'}}>
                <View>
                    <Text style={styles.text}>NIK</Text>
                    <Text style={styles.text}>No. KK</Text>
                    <Text style={styles.text}>Tanggal Lahir</Text>
                    <Text style={styles.text}>Alamat</Text>
                    <Text style={styles.text}>No. Telp</Text>
                </View>
                <View style={{paddingLeft: 20}}>
                    <Text style={styles.text}>: {props.nik}</Text>
                    <Text style={styles.text}>: {props.kk}</Text>
                    <Text style={styles.text}>: {props.tanggal}</Text>
                    <Text style={styles.text}>: {props.alamat}</Text>
                    <Text style={styles.text}>: {props.telp}</Text>
                </View>
            </View>
            <View style={{marginTop: 10, alignItems: 'center'}}>
                <Image source={{uri: `data:image/png;base64,${props.photo}`}} style={{width: 350, height: 467}} />
            </View>
            <View style={{marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                <TouchableOpacity onPress={() => props.terimaPengajuan()}>
                    <View style={{height: 40, width: 150, backgroundColor: '#26A718', justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Setujui</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity  onPress={() => props.tolakPengajuan()}>
                    <View style={{height: 40, width: 150, backgroundColor: '#BB2424', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18}}>Tolak</Text>
                        </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {fontFamily: 'Poppins-Bold', fontSize: 18},
})
