import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function index(props) {
    return (
        <View style={{marginTop: 10}}>
            <View>
                <Text style={styles.textLabel}>
                    Isi Laporan
                </Text>
            </View>
            <View style={styles.textData}>
                <Text>{props.isi}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    textLabel: {fontFamily: 'Poppins-Bold', fontSize: 16},
    textData : {fontFamily: 'Poppins', fontSize: 14}
})
