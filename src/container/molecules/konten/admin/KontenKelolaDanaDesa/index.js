import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'


export default function index(props) {
    return (
        <View style={{flex: 1, padding: 10}}>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('APBDes', {idDesa: props.idDesa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>APBDes</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('Realisasi', {idDesa: props.idDesa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Realisasi APBDes</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('KategoriAPBDes', {idDesa: props.idDesa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Sumber Dana</Text>
                    </View>
                </TouchableOpacity>
            </View>
           
        </View>
    )
}
