import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

export default function index(props) {
    return (
        <View style={{width: '100%', height: 430, paddingHorizontal: 10}}>
            <View style={{backgroundColor: '#FFFFFF', width: '100%', height: 510, borderRadius: 10, paddingVertical: 10}} elevation={3}>
                <View style={{flexDirection: 'row', justifyContent:'space-around'}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaProfilDesa',{idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                        
                            <Image source={require('./../../../../../assets/icons/profile.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Profile Desa</Text>
                        
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaPemerintahan', {idDesa: props.id_desa})}>
                    <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                        <Image source={require('./../../../../../assets/icons/pemerintahan.png')} style={{width: 40, height: 40}} />
                        <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Pemerintahan</Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaIventaris', {idDesa: props.id_desa})}>
                    <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                        <Image source={require('./../../../../../assets/icons/iventaris.png')} style={{width: 40, height: 40}} />
                        <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Iventaris</Text>
                    </View>
                    </TouchableOpacity>
                    
                </View>
                <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaDanaDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/dana.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Dana Desa</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Maps', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/map.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Peta Desa</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaKontakDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/kontak.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Laporan Warga</Text>
                        </View>
                    </TouchableOpacity>
                    
                </View>
                <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('PengaturanSurat', {id_desa: props.id_desa, warna: props.warna})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/letter.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Kelola Surat</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaProdukWarga', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/product.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10, textAlign: 'center'}}>Kelola Produk Warga</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('BeritaDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/create.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Buat Berita</Text>
                        </View>
                    </TouchableOpacity>
                    
                </View>
                <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('PengumumanDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/billboard.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10,textAlign: 'center'}}>Kelola Pengumuman</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('PollingDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/voting.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Kelola Polling</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaPengajuanWarga', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/warga.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10, textAlign: 'center'}}>Pengajuan Warga</Text>
                        </View>
                    </TouchableOpacity>
                   
                </View>
                <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('TabelBantuanDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/trust.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Bantuan</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('KelolaInformasiDesa', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/informasi.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Informasi Desa</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('SettingAdmin', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/admin-tools.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10}}>Setting Admin</Text>
                        </View>
                    </TouchableOpacity>
                    
                </View>
                <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('StatistikPengunjung', {idDesa: props.id_desa})}>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./../../../../../assets/icons/chart.png')} style={{width: 40, height: 40}} />
                            <Text style={{fontSize: 10, fontFamily: 'Rubik-Regular', marginTop: 10, textAlign: 'center'}}>Statistik Pengunjung</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                         </View>
                        <View style={{width: 80, height: 80, justifyContent: 'center', alignItems: 'center'}}>
                            </View>
                    
                </View>
            </View>
        </View>
    )
}
