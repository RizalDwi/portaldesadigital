import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList } from 'react-native'

export default function index(props) {

    const renderItem = ({item, index}) => {
        return (
            <View style={{borderColor: '#DFDFDF', borderBottomWidth: 1, marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailProdukWarga',{id: item.id})}>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                        <Image source={item.foto == null ? require('./../../../../../assets/images/avatar.jpg') :  {uri: `data:image/png;base64,${item.foto}`}} style={{height: 50, width: 50, borderRadius: 25}} />
                        <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.name}</Text>
                                <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>{item.judul_produk}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{textAlign: 'right', fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>{item.created_at}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{flex:1}}>
            <FlatList data={props.listPengajuan} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </View>
    )
}

const styles = StyleSheet.create({})
