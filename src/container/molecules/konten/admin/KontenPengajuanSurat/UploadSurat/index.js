import React, {useContext, useState} from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Dimensions } from 'react-native'
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob'
import Pdf from 'react-native-pdf';
import { RootContext } from './../../../../../../screens/admin/PengajuanSurat'


export default function index() {

    const state = useContext(RootContext)

    //const [file, setFile] = useState('')

    const convertFile = (exampleFilePath) => {
        const fs = RNFetchBlob.fs;
        fs.readFile(exampleFilePath, 'base64')
          .then(data => {
              state.handleFile(data)
              console.log(data)
          })
      }

    const getFile = async() => {
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.pdf],
            });
            
            convertFile(res.uri)
            
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              // User cancelled the picker, exit any dialogs or menus and move on
            } else {
              throw err;
            }
          }
    }

    return (
        <View style={{flex: 1, padding: 20}}>
                <View>
                    <Text style={styles.label}>Nomor Surat</Text>
                    <TextInput placeholder="Nomor Surat" style={styles.form} onChangeText={(value) => state.handleNomorSurat(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Tanda Tangan</Text>
                    <TextInput placeholder="Tanda Tangan" style={styles.form} onChangeText={(value) => state.handleTandaTangan(value)} />
                </View>
                <View>
                    <TouchableOpacity style={{width: '100%'}} onPress={() => getFile()}>
                        <View style={{width: 130, height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center', borderRadius: 20}}>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 13, textAlign: 'center'}}>Pilih File Surat</Text>
                            </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerPdf}>
                <Pdf
                        source={{uri: `data:application/pdf;base64,${state.file}` }}
                        onLoadComplete={(numberOfPages,filePath)=>{
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page,numberOfPages)=>{
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error)=>{
                            console.log(error);
                        }}
                        onPressLink={(uri)=>{
                            console.log(`Link presse: ${uri}`)
                        }}
                        style={styles.pdf}/>
                </View> 
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => state.uploadSurat()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Upload</Text>
                    </View>
               </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      containerPdf: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,

    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    }
})