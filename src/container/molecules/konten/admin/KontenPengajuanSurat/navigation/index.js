import React from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import StatusPengajuan from './../StatusPengajuan'
import Upload from './../UploadSurat'

const Tab = createMaterialTopTabNavigator();
export default function index() {

    return (
          <Tab.Navigator>
            <Tab.Screen name="status" component={StatusPengajuan} options={{title: 'Ubah Status'}}/>
            <Tab.Screen name="upload" component={Upload} options={{title: 'Upload Surat'}} />           
          </Tab.Navigator>
    )
}