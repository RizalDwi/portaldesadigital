import React, {useContext, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { RootContext } from './../../../../../../screens/admin/PengajuanSurat'

export default function index() {
    const state = useContext(RootContext)

    useEffect(() => {
        console.log("Pengajuan ->",state.pengajuan)
    }, [])
    return (
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.row}>
                <View style={styles.column}>
                    <Text style={styles.title}>Nama Pengaju</Text>
                </View>
                <View style={styles.column}>
                    <Text style={styles.label}>: {state.pengajuan.name}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.column}>
                    <Text style={styles.title}>NIK</Text>
                </View>
                <View style={styles.column}>
                    <Text style={styles.label}>: 3466345675</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.column}>
                    <Text style={styles.title}>Tanggal</Text>
                </View>
                <View style={styles.column}>
                    <Text style={styles.label}>: {state.pengajuan.created_at}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.column}>
                    <Text style={styles.title}>Jenis Surat</Text>
                </View>
                <View style={styles.column}>
                    <Text style={styles.label}>: {state.pengajuan.nama_jenis}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.column}>
                    <Text style={styles.title}>Status</Text>
                </View>
                <View style={styles.column}>
                    <View style={ state.pengajuan.status == "Dalam Proses" ? styles.wrapTextProsses : state.pengajuan.status == "Sedang Diteruskan" ? styles.wrapTextTeruskan : styles.wrapTextDitolak }>
                        <Text style={styles.texStatus}>{state.pengajuan.status}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.column}>
                    <Text style={styles.title}>Kepentingan</Text>
                </View>
                <View style={styles.column}>
                    <Text style={styles.label}>: {state.pengajuan.kepentingan}</Text>
                </View>
            </View>
            <View style={styles.buttonFooter}>
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateStatusTerukan() }>
                    <View style={styles.buttonStatus}>
                        <Text style={styles.textButton}>Sedang diteruskan</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateStatusTolak()}>
                    <View style={styles.buttonTolak}>
                        <Text style={styles.textButton}>Tolak</Text>
                    </View>
                </TouchableOpacity>
            </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20
    },
    row: {
        flexDirection: 'row'
    },
    column: {
        width: '50%',
    },
    title: {
        fontFamily: 'Rubik-Bold',
        fontSize: 15
    },
    label: {
        fontFamily: 'Rubik-Regular',
        fontSize: 15
    },
    wrapTextProsses: {
        alignItems: 'center',
        width: 110,
        backgroundColor: '#4CC9F0'
    },
    wrapTextTeruskan: {
        alignItems: 'center',
        width: 110,
        backgroundColor: '#26A718'
    },
    wrapTextDitolak: {
        alignItems: 'center',
        width: 110,
        backgroundColor: '#BB2424'
    },
    texStatus: {
        color: '#FFFFFF',
        fontFamily: 'Rubik-Bold'
    },
    buttonFooter: {
        
        width: '100%',
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36

    },
    buttonStatus: {
        backgroundColor: '#26A718',
        width: '100%',
        height: 40,
        marginBottom: 10,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonTolak: {
        backgroundColor: '#BB2424',
        width: '100%',
        height: 40,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        color: '#FFFFFF',
        fontFamily: 'Poppins-Bold',
        fontSize: 20
    }
})
