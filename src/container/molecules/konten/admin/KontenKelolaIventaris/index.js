import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'


export default function index(props) {
    return (
        <View style={{flex: 1, padding: 10}}>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisMesin', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Mesin</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisElektronik', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Elektronik</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisBangunan', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Gedung</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisJaringan', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Jalan, Irigasi, dan Jaringan</Text>
                    </View>
                </TouchableOpacity>
            </View>

            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisTanah', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Tanah</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisKonstruksi',{idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Konstruksi Dalam Pengerjaan</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TabelIventarisAsetLainya', {idDesa: props.id_desa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Aset Lainya</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
