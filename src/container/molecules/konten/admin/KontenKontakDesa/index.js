import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList } from 'react-native'

export default function index(props) {


    return (
        <View style={{flex:1}}>
            {props.dataLaporan.map((item, index) => (
                    <View key={index}>
                        <View style={{borderColor: '#DFDFDF', borderBottomWidth: 1, marginVertical: 5}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('DetailPesanWarga', {id: item.id})}>
                                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                                    <Image source={require('./../../../../../assets/images/rijal.jpg')} style={{height: 50, width: 50, borderRadius: 25}} />
                                    <View style={{marginLeft: 10}}>
                                            <Text style={{fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'}}>{item.name}</Text>
                                            <Text style={{fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>Perilha: {item.perihal}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <Text style={{textAlign: 'right', fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>{item.created_at}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                ))}
            
        </View>
    )
}

const styles = StyleSheet.create({})
