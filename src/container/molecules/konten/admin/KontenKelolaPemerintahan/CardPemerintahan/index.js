import React, {useContext, useEffect} from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, ScrollView, StyleSheet } from 'react-native'
import { RootContext } from './../../../../../../screens/admin/Pemerintahan'

export default function index( {navigation} ) {
    const state = useContext(RootContext)


    return (
       
        <View style={{flex: 1, padding: 10}} >
            <ScrollView>
                <TouchableOpacity onPress={() => navigation.navigate('FormTambahAparatDesa',{idDesa: state.idDesa})}>
                    <View style={{backgroundColor: `#${state.warna}`, width: 270, height: 30, alignItems: 'center', justifyContent: 'center', borderRadius: 15}}>
                        <Text style={{fontFamily: 'Rubik-Regular', fontSize: 18, color: '#FFFFFF'}}>Tambah Aparat Pemerintah</Text>
                    </View>
                </TouchableOpacity>
                {state.aparat.map((item, index) => (
                    <View key={index}>
                        <View style={{width: '100%', height: 250, borderRadius: 10, padding: 10, marginTop: 10, borderWidth: 1, borderColor: '#000000'}}>
                        <View style={{flexDirection: 'row', marginBottom: 10}}>
                            <View>
                                <Image source={{uri: `data:image/png;base64,${item.foto}`}} style={{width: 90.5, height: 135, borderRadius: 10}} />
                            </View>
                            <View style={{flex: 1, marginLeft: 5}}>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>NAMA : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>JABATAN : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>NIAP : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>TEMPAT, TANGGAL LAHIR : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>AGAMA : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>PANGKAT / GOLONGAN : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>PENDIDIKAN TERAKHIR : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>NOMOR SK : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>TANGGAL SK : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>MASA JABATAN : </Text>
                                <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>STATUS : </Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.nama}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.jabatan}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.niap}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.tempat_lahir}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.agama}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.pangkat_atau_golongan}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.pendidikan}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.nomor_sk_pengangkatan}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.tanggal_sk_pengangkatan}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.masa_jabatan}</Text>
                                <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.status}</Text>
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end'}}>
                            <TouchableOpacity onPress={() => navigation.navigate('EditAparat',{idAparat: item.id})}>
                                <View style={{backgroundColor: '#F19000', width: 50, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Edit</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => state.alertDeleteAparat(item.id)}>
                                <View style={{backgroundColor: '#E03535', width: 60, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Hapus</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => state.ModalTupoksi(item.tupoksi)}>
                                <View style={{backgroundColor: '#35BEE0', width: 140, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Tupoksi Jabatan</Text>
                                </View>
                            </TouchableOpacity>
                            
                        </View>
                    </View>          
                    </View>
                ))}
               
            </ScrollView>
        
     </View>
    )
    
}
