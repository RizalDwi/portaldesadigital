import React,{useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, TouchableOpacity} from 'react-native'
import Textarea from 'react-native-textarea';
import Tabel from './../../../../Tabel/TabelMisi'
import { RootContext } from './../../../../../../screens/admin/Pemerintahan'

export default function index({navigation}) {
    const state = useContext(RootContext)

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Visi</Text>
                </View>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Tulis Sejarah'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        value={state.visi}
                        onChangeText={(value) =>  state.handleChangeInputVisi(value)}
                    />
                </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateVisi()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
              
                <TouchableOpacity style={{marginTop: 10}} onPress={() => navigation.navigate('TambahMisi',{idVisi: state.idVisi})}>
                    <View style={[styles.button,{ backgroundColor: `#${state.warna}`}]}>
                        <Text style={styles.textButton}>Tambah Misi</Text>
                    </View>
                </TouchableOpacity>
                <Tabel navigation={navigation} warna={state.warna} />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      form: { width: '85%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
        button: {width: '40%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
  });