import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Tabel from './../../../../../container/molecules/Tabel/TabelBantuanDesa'

export default function index(props) {
    return (
        <View style={{flex: 1}}>
            <TouchableOpacity onPress={() => props.navigation.navigate('TambahProgram', {idDesa: props.idDesa})}>
                <View style={[styles.button,{ backgroundColor: `#${props.warna}`}]}>
                    <Text style={styles.textButton}>Tambah Program Bantuan</Text>
                </View>
            </TouchableOpacity>
            <Tabel warna={props.warna} navigation={props.navigation} listProgram={props.listProgram} idDesa={props.idDesa} alertDelete={props.alertDelete} />
        </View>
    )
}

const styles = StyleSheet.create({
    button: {backgroundColor: '#35BEE0', width: '60%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2},
    textButton: {fontFamily: 'Rubik', fontSize: 12, color: '#FFFFFF'},
})
