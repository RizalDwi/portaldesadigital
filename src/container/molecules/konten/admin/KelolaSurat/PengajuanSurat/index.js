import React, {useState, useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image, TouchableOpacity, Alert, Dimensions } from 'react-native'

import { RootContext } from './../../../../../../screens/admin/PelayananSurat'

export default function index({navigation}) {
    const state = useContext(RootContext)

    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
            <View style={{flex:1}}>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Nama Pengaju</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.name}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Tanggal</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.created_at}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Jenis Surat</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.nama_jenis}</Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Status</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.status}</Text>
                    </View>
                </View>
               
            </View>
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                    <TouchableOpacity onPress={() => navigation.navigate('PengajuanSurat', {idPengaju: item.id, idDesa: state.idDesa}) }>
                        <View style={{backgroundColor: '#F19000', width: 50, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                            <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Edit</Text>
                        </View>
                    </TouchableOpacity>
                </View>
        </View>

        )
    }

    return (
        <View style={{flex: 1, marginTop: 10, padding: 20}}>
            <ScrollView>
                    <FlatList data={state.pengajuan} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10}
})

