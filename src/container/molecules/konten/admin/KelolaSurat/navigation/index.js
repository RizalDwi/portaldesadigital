import React from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import PengajuanSurat from './../PengajuanSurat'
import ArsipSurat from './../ArsipSurat'
import JenisSurat from './../JenisSurat'

const Tab = createMaterialTopTabNavigator();
export default function index() {

    return (
          <Tab.Navigator>
            <Tab.Screen name="Pengajuan" component={PengajuanSurat}/>
            <Tab.Screen name="Jenis" component={JenisSurat} />
            <Tab.Screen name="Arsip" component={ArsipSurat} />           
          </Tab.Navigator>
    )
}