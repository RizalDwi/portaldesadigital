import React, {useContext, useEffect, useState} from 'react'
import { View, Text, StyleSheet, ScrollView, TextInput, Image, TouchableOpacity} from 'react-native'
import { RootContext } from './../../../../../../screens/admin/ProfilDesa'
import Tabel from './../../../../Tabel/TabelLuasDesa'

export default function index({navigation}) {
    const state = useContext(RootContext)
    const [luas, setLuas] = useState(0)

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Luas Total : {luas} Ha</Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('TambahLuasDesa', {idDesa: state.idDesa})}>
                    <View style={[styles.button,{ backgroundColor: `#${state.warna}`}]} >
                        <Text style={styles.textButton}>Tambah Luas Desa</Text>
                    </View>
                </TouchableOpacity>
               <Tabel navigation={navigation} warna={state.warna} setLuas={setLuas} luas={luas} />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        textAlign: 'justify'
    },
    form: { width: '80%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        textButton: {fontFamily: 'Rubik', fontSize: 12, color: '#FFFFFF'},
        button: {width: '40%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
  });