import React, {useContext} from 'react'
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity} from 'react-native'
import Textarea from 'react-native-textarea';
import { RootContext } from './../../../../../../screens/admin/ProfilDesa'

export default function index() {

    const state = useContext(RootContext)

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Sejarah Desa</Text>
                </View>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Tulis Sejarah'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        value={state.sejarah}
                        onChangeText={(value) =>  state.handleChangeInputSejarah(value)}
                    />
                </View>
                <View>
                    <Text style={styles.title}>Batas Wilayah</Text>
                    <Text style={styles.text}>Secara demografis, desa Widang berbatasan dengan:</Text>
                </View>
                <View style={{marginTop: 10}}>
                    <View style={styles.input}>
                        <Text style={styles.label}>Utara</Text>
                        <TextInput placeholder="Batas Utara" style={styles.form} value={state.batas_utara} onChangeText={(value) =>  state.handleChangeInputBatasUtara(value)}/>
                    </View>
                    <View style={styles.input}>
                        <Text style={styles.label}>Selatan</Text>
                        <TextInput placeholder="Batas Selatan" style={styles.form} value={state.batas_selatan} onChangeText={(value) =>  state.handleChangeInputBatasSelatan(value)}/>
                    </View>
                    <View style={styles.input}>
                        <Text style={styles.label}>Timur</Text>
                        <TextInput placeholder="Batas Timur" style={styles.form} value={state.batas_timur} onChangeText={(value) =>  state.handleChangeInputBatasTimur(value)}/>
                    </View>
                    <View style={styles.input}>
                        <Text style={styles.label}>Barat</Text>
                        <TextInput placeholder="Batas Barat" style={styles.form} value={state.batas_barat} onChangeText={(value) =>  state.handleChangeInputBatasBarat(value)}/>
                    </View>
                </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateKondisiDesa()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
                </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        textAlign: 'justify'
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      
      form: { width: '100%',
            height: 60,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        label : {
            fontFamily: 'Poppins-Bold',
            color: '#394761',
            fontSize: 15
        }
  });
