import React, {createContext, useEffect, useState} from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Kondisi from './../KontenKondisiDesa'
import Luas from './../KontenLuasDesa'
import Topografi from './../KontenTopografi'

const Tab = createMaterialTopTabNavigator();
export default function index() {

    return (
            <Tab.Navigator>
              <Tab.Screen name="Kondisi" component={Kondisi} />
              <Tab.Screen name="Luas" component={Luas} />        
            </Tab.Navigator>
          
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    title: { height: 40, backgroundColor: '#E5E5E5', padding: 10 },
    head: { height: 40, backgroundColor: '#F5F5F5' },
    text: { margin: 6 },
    dataWrapper: { marginTop: -1},
    scene: {
        flex: 1,
      }
  });