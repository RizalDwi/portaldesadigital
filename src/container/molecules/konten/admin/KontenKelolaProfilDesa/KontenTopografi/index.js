import React, {useContext, useEffect} from 'react'
import { View, Text, StyleSheet, Image, TextInput, FlatList, ScrollView, TouchableOpacity} from 'react-native'
import Textarea from 'react-native-textarea';
import { RootContext } from './../../../../../../screens/admin/ProfilDesa'
import Tabel from './../../../../Tabel/TabelTopografi'

export default function index({navigation}) {
    const state = useContext(RootContext)


    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Deskripsi Topografi</Text>
                </View>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Deskripsi Topografi'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        value={state.deskripsiTopogarfi}
                        onChangeText={(value) =>  state.handleChangeInputDeskripsiTopografi(value)}
                    />
                </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateTopografi()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
                <View style={{marginTop: 10, marginBottom: 10}}>
                    <View>
                        <Text style={styles.title}>Topografi</Text>
                    </View>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('TambahTopografi',{idTopografi: state.idTopografi})}>
                    <View style={[styles.button,{ backgroundColor: `#${state.warna}`}]}>
                        <Text style={styles.textButton}>Tambah Kategori Baru</Text>
                    </View>
                </TouchableOpacity>
                <Tabel navigation={navigation} warna={state.warna} />
            </ScrollView>
                
               
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        textAlign: 'justify'
    },
    
    textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      container: {
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      form: { width: '60%',
            height: 35,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        form2: { width: '78%',
            height: 35,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        textButton: {fontFamily: 'Rubik', fontSize: 14, color: '#FFFFFF'},
        button: {backgroundColor: '#35BEE0', width: '50%', height: 28, justifyContent: 'center', alignItems: 'center', borderRadius: 28/2}
  });