import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity } from 'react-native'

export default function index(props) {

    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
         var x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    
    }
   
    var indikatorKategori = ''
    var indikatorSubkategori = ''

    const renderItem = ({ item, index }) => {
        return (
            <View>
                {(() => {
                            if (indikatorKategori != item.nama_kategori){
                                indikatorKategori = item.nama_kategori
                                return (
                                    <View style={styles.rowKategori}>
                                         <Text style={styles.labelKategoriRow}>{item.nama_kategori}</Text>
                                    </View>
                                )
                            }
                            
                        })()}
                        {(() => {
                            
                            if(indikatorSubkategori != item.nama_subkategori){
                                indikatorSubkategori = item.nama_subkategori
                                return (
                                    <View style={{flexDirection: 'row'}}>
                                                <View style={styles.rowSubkategori}>
                                                    <Text style={styles.labelRow}>{item.nama_subkategori}</Text>
                                                </View>
                                                <View style={styles.rowRincianDana}>
                                                    <Text style={styles.labelRow}>Rincian Dana</Text>
                                                </View>
                                                
                                    </View>
                                )

                            }
                            
                        })()}

                        <View style={{flexDirection: 'row'}}>
                            <View style={styles.rowKategoriKosong}>
                                <Text style={styles.labelRow2}></Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.labelRow2}>{item.nama_kegiatan}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.labelRow2}>Rp. {addCommas(item.anggaran)}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.labelRow2}>Rp. {addCommas(item.realisasi == null? 0 : item.realisasi) }</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.labelRow2}>Rp. {addCommas(item.realisasi == null? item.anggaran - 0 : item.anggaran - item.realisasi) }</Text>
                            </View>
                            
                        </View>

            </View>
        )
        
    }

    return (
        <View style={{flex: 1, marginTop: 10}}>
            <ScrollView horizontal={true}>
                <View style={{width: '100%'}}>
                    <View style={{flexDirection: 'row', backgroundColor: `#${props.warna}` }}>
                        <View style={styles.rowHeadKategori}>
                            <Text style={styles.labelHeader}>Sumber Dana</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Perencanaan Kegiatan</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Anggaran</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Realisasi</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Saldo</Text>
                        </View>
                      
                    </View>
                    <FlatList data={props.dataAnggaran} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
                    <View style={{flexDirection: 'row'}}>
                            <View style={styles.rowKategoriKosong2}>
                                <Text style={styles.labelRow}>Total Bidang Penyelenggaraan Pemerintah Desa</Text>
                            </View>
                            <View style={styles.rowFooter}>
                                <Text style={styles.labelRow}>Rp. {addCommas(props.anggaran == null ? 0 : props.anggaran)}</Text>
                            </View>
                            <View style={styles.rowFooter}>
                                <Text style={styles.labelRow}>Rp. {addCommas(props.realisasi == null ? 0 : props.realisasi)}</Text>
                            </View>
                            <View style={styles.rowFooter}>
                                <Text style={styles.labelRow}>Rp. {addCommas(props.saldo == null ? 0 : props.saldo)}</Text>
                            </View>
                            
                        </View>
                    
                </View>
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center'
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center'
    },
    labelRow2 : {
        fontFamily: 'Poppins',
        color: '#000000',
        fontSize: 14,
        textAlign: 'center'
    },
    labelKategoriRow : {
        fontFamily: 'Poppins',
        color: '#FFFFFF',
        fontSize: 14,
    },
    row: {
        borderColor: '#000000', borderWidth: 2, width: 250, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowFooter: {
        borderColor: '#FFFFFF', borderWidth: 2, width: 250, alignItems: 'center', justifyContent: 'center', padding: 10, backgroundColor: '#000000'
    },
    rowHeadKategori: {
        borderColor: '#000000', borderWidth: 2, width: 250, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowKategori: {
        borderColor: '#000000', borderWidth: 2, width: '100%', padding: 10, backgroundColor: '#92ACFF'
    },
    rowRincianDana: {
        borderColor: '#000000', borderWidth: 2, flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10, backgroundColor:'#39A44B'
    },
    
    rowKategoriKosong: {
        borderColor: '#000000', borderWidth: 2, width: 250, padding: 10,
    },
    rowKategoriKosong2: {
        borderColor: '#FFFFFF', borderWidth: 2, flex: 1, padding: 10, backgroundColor: '#000000'
    },
    rowSubkategori: {
        borderColor: '#000000', borderWidth: 2, width: 250, padding: 10,backgroundColor:'#39A44B'
    }
})
