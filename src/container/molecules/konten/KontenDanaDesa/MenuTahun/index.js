import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'


export default function index(props) {
    return (
        <View style={{flex: 1, padding: 10}}>
            <Text style={{fontFamily: 'Poppins-Bold', fontSize: 30}}>Dana Desa</Text>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailDanaDesa', {tahun: 2019, idDesa: props.idDesa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Tahun Anggaran 2019</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailDanaDesa', {tahun: 2020, idDesa: props.idDesa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Tahun Anggaran 2020</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginVertical: 5}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailDanaDesa', {tahun: 2021, idDesa: props.idDesa})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>Tahun Anggaran 2021</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
