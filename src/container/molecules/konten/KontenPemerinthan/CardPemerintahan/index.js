import React, {useContext} from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native'
import { RootContext } from './../../../../../screens/desa/Pemerintahan'

export default function index() {
    const state = useContext(RootContext)

  
    return (
        <ScrollView style={{flex: 1, padding: 10}} >
            {state.aparat.map((item, index) => (
                                    <View key={index}>
                                         <View style={{width: '100%', borderRadius: 10, padding: 10, marginVertical: 5, borderWidth: 1, borderColor: '#000000'}}>
                                            <View style={{flexDirection: 'row', marginBottom: 10}}>
                                                <View>
                                                    <Image source={{uri: `data:image/png;base64,${item.foto}`}} style={{width: 90.5, height: 135, borderRadius: 10}} />
                                                </View>
                                                <View style={{flex: 1}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>NAMA : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.nama}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>Jabatan : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.jabatan}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>NIAP : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.niap}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>TEMPAT, TANGGAL LAHIR : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.tempat_lahir}, {item.tanggal_lahir}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>AGAMA : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.agama}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>PANGKAT / GOLONGAN : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.pangkat_atau_golongan}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>PENDIDIKAN TERKAHIR : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.pendidikan}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>NOMOR SK : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.nomor_sk_pengangkatan}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>TANGGAL SK : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.tanggal_sk_pengangkatan}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>MASA JABATAN : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.masa_jabatan}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <View style={{flex: 1, marginLeft: 5}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', textAlign: 'right', fontSize: 8}}>STATUS : </Text>
                                                        </View>
                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontFamily: 'Poppins-Bold', color: '#646464', fontSize: 8}}>{item.status}</Text>
                                                        </View>
                                                    </View>
                                                
                                                </View>
                                            </View>
                                            <View style={{flex: 1, alignItems: 'flex-end'}}>
                                                <TouchableOpacity onPress={() => state.ModalTupoksi(item.tupoksi)} style={{width: '40%'}}>
                                                    <View style={{backgroundColor: '#35BEE0', height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30}}>
                                                        <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Tupoksi Jabatan</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                ))}
        </ScrollView>
    )
}

