import React from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Card from './../CardPemerintahan'
import VisiDanMisi from './../VisiDanMisi'

const Tab = createMaterialTopTabNavigator();
export default function index() {

    return (
          <Tab.Navigator>
            <Tab.Screen name="visiDanMisi" component={VisiDanMisi} options={{title: 'Visi & Misi'}}/>
            <Tab.Screen name="Pemerintahan" component={Card} />           
          </Tab.Navigator>
    )
}