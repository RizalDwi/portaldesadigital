import React, {useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList} from 'react-native'
import { RootContext } from './../../../../../screens/desa/Pemerintahan'

export default function index() {
    const state = useContext(RootContext)


    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Visi</Text>
                </View>
                <View>
                    <Text style={styles.text}>{state.visi}</Text>
                </View>
                <View>
                    <Text style={styles.title}>Misi</Text>
                </View>
                {state.misi.map((item, index) => (
                                    <View key={index}>
                                        <Text style={styles.text}>- {item.misi}</Text>
                                    </View>
                                ))}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        
    }
  });