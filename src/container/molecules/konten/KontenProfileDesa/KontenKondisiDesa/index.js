import React, {useContext, useEffect, useState} from 'react'
import { View, Text, StyleSheet, ScrollView} from 'react-native'
import { RootContext } from './../../../../../screens/desa/ProfileDesa'

export default function index() {
    const state = useContext(RootContext)
    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Sejarah Desa</Text>
                </View>
                <View>
                    <Text style={styles.text}>
                       {state.dataKondisi.length > 0 ? state.dataKondisi[0].sejarah : ""}
                    </Text>
                </View>
                <View>
                    <Text style={styles.title}>Batas Wilayah</Text>
                </View>
                <View>
                    <Text style={styles.text}>- Sebelah utara : {state.dataKondisi.length > 0 ? state.dataKondisi[0].batas_utara : ""}</Text>
                    <Text style={styles.text}>- Sebelah selatan : {state.dataKondisi.length > 0 ? state.dataKondisi[0].batas_selatan : ""}</Text>
                    <Text style={styles.text}>- Sebelah timur : {state.dataKondisi.length > 0 ? state.dataKondisi[0].batas_timur : ""}</Text>
                    <Text style={styles.text}>- Sebelah barat : {state.dataKondisi.length > 0 ? state.dataKondisi[0].batas_barat : ""}</Text>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        textAlign: 'justify'
    }
  });
