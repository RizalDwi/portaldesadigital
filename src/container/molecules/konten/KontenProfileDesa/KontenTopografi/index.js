import React, {useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList} from 'react-native'
import { RootContext } from './../../../../../screens/desa/ProfileDesa'

export default function index() {
    const state = useContext(RootContext)

    // const renderKategori = ({item, index}) => {
    //     console.log("Item Kelompok",item)
    //     return (
    //         <View>
    //                 <View>
    //                     <Text style={styles.title}>{item.kategori.nama_kategori} :</Text>
    //                 </View>
    //                 <FlatList data={item.kelomok} renderItem={renderKelompok} keyExtractor = { (item, index) => index.toString() } />
    //             </View>
    //     )
    // }

    // const renderKelompok = ({item, index}) => {
    //     return (
    //         <View style={{flexDirection: 'row'}}>
    //                     <View style={{height: 25, flex: 1}}>
    //                         <Text style={styles.text}>- {item.nama_kelompok}</Text>
    //                     </View>
    //                     <View style={{height: 25, flex: 1}}>
    //                         <Text style={styles.text}>: {item.jumlah} Orang</Text>
    //                     </View>
    //         </View>
    //     )
    // }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View style={{marginBottom: 10}}>
                    <View>
                            <Text style={styles.title}>Topografi</Text>
                        </View>
                        <View>
                            <Text style={styles.text}>{state.deskripsi}</Text>
                        </View>
                </View>
                {state.dataTopografi.map((item, index) => (
                            <View key={index}>
                                <View>
                                    <Text style={styles.title}>{item.kategori.nama_kategori} :</Text>
                                </View>
                                {item.kelomok.map((item, index) => (
                                    <View key={index}>
                                        <View style={{flexDirection: 'row'}}>
                                                <View style={{height: 25, flex: 1}}>
                                                    <Text style={styles.text}>- {item.nama_kelompok}</Text>
                                                </View>
                                                <View style={{height: 25, flex: 1}}>
                                                    <Text style={styles.text}>: {item.jumlah} Orang</Text>
                                                </View>
                                        </View>
                                    </View>
                                ))}
                            </View>
                ))}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        textAlign: 'justify'
    }
  });