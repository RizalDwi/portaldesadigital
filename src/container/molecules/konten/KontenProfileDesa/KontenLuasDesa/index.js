import React, {useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList} from 'react-native'
import { RootContext } from './../../../../../screens/desa/ProfileDesa'

export default function index() {
    const state = useContext(RootContext)

    // const renderItem = ({item, index}) => {
    //     return (
    //         <View style={{flexDirection: 'row'}}>
    //                     <View style={{height: 25, flex: 1}}>
    //                         <Text style={styles.text}>- {item.nama_tempat}</Text>
    //                     </View>
    //                     <View style={{height: 25, flex: 1}}>
    //                         <Text style={styles.text}>: {item.luas} Ha</Text>
    //                     </View>
    //         </View>
    //     )
    // }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.title}>Luas Wilayah</Text>
                </View>
                <View>
                    <Text style={styles.text}> Luas desa Widang adalah {state.luasTotal} Ha</Text>
                    {state.dataLuas.map((item, index) => (
                                    <View key={index}>
                                        <View style={{flexDirection: 'row'}}>
                                                    <View style={{height: 25, flex: 1}}>
                                                        <Text style={styles.text}>- {item.nama_tempat}</Text>
                                                    </View>
                                                    <View style={{height: 25, flex: 1}}>
                                                        <Text style={styles.text}>: {item.luas} Ha</Text>
                                                    </View>
                                        </View>
                                    </View>
                                ))}
                    
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 16
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 14,
        textAlign: 'justify'
    }
  });