import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

export default function index(props) {
    return (
        <View style={{flex: 1, padding: 20}}>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.title}>Username</Text>
                <Text style={styles.text}>{props.name}</Text>
            </View>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.title}>Email</Text>
                <Text style={styles.text}>{props.email}</Text>
            </View>
            <View style={{alignItems: 'center'}}> 
                <TouchableOpacity onPress={() => { props.updateFoto() }} style={{height: 40, width: '35%', borderColor: '#707070', borderWidth: 2, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                            <Text style={{fontFamily: 'Poppins-Bold', fontSize: 18, color: '#242134' }}>
                                Simpan
                            </Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily: 'Poppins-Bold',
        fontSize: 20
    },
    text: {
        fontFamily: 'Poppins',
        fontSize: 18,
        marginLeft: 10,
        textAlign: 'justify'
    }
  });