import React, {useEffect} from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'

export default function index(props) {


    const renderItem = ({item, index}) => {
        return (
            <View style={{marginVertical: 5, width: '100%', height: 43}}>
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailInformasi',{id: item.id})}>
                    <View style={{backgroundColor: `#${props.warna}`, width: '100%', height: 40, justifyContent: 'center', paddingLeft: 10}}>
                        <Text style={{fontFamily: 'Poppins-Bold', fontSize: 20, color: '#FFFFFF'}}>{item.judul_informasi}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{flex: 1, paddingHorizontal: 10, paddingVertical: 5}}>
            <FlatList data={props.data} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
        </View>
    )
}
