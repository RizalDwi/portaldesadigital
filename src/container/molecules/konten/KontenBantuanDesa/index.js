import React from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, InteractionManager } from 'react-native'

export default function index(props) {
     
    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
                    <View style={{flex:1}}>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nama Program</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.nama_program}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanggal Mulai</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.tanggal_mulai}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanggal Berakhir</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>: {item.tanggal_berakhir}</Text>
                            </View>
                        </View>
            
                    </View>
                        
                </View>
        )
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
            <FlatList data={props.listBantuan} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10},
})
