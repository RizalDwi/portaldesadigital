import React from 'react'
import { View, Text, Image, FlatList, TouchableOpacity } from 'react-native'

export default function index(props) {

    const RenderItem = (props) => {
        return(
            <TouchableOpacity onPress={() => props.navigation.navigate('ArtikelBerita', {id: item.id})} style={{width: '100%', height: 110, marginVertical: 5, flexDirection: 'row'}}>
                <View>
                    <Image source={{uri: `data:image/png;base64,${item.foto}`}} style={{width:160, height: '100%', borderRadius: 10}} />
                </View>
                <View style={{flex: 1, marginLeft: 10}}>
                    <Text style={{fontSize: 17, fontFamily: 'SFProDisplay', color: '#26325B'}}>{item.judul}</Text>
                    <Text style={{fontSize: 12, fontFamily: 'SFProDisplay', color: '#26325B', marginTop: 10}}>{item.created_at}</Text>
                </View>

            </TouchableOpacity>
        )
    }

    return (
        <View style={{width: '100%', padding: 10}}>
        <Text style={{fontSize: 24, fontFamily: 'Rubik-Bold', color: '#696969'}}>
            Berita
        </Text>
        <View style={{marginTop: 10}}>
            {props.dataBerita.map((item, index) => (
            <View key={index}>
                <TouchableOpacity onPress={() => props.navigation.navigate('ArtikelBerita', {id: item.id})} style={{width: '100%', height: 110, marginVertical: 5, flexDirection: 'row'}}>
                    <View>
                        <Image source={{uri: `data:image/png;base64,${item.foto}`}} style={{width:160, height: '100%', borderRadius: 10}} />
                    </View>
                    <View style={{flex: 1, marginLeft: 10}}>
                        <Text style={{fontSize: 17, fontFamily: 'SFProDisplay', color: '#26325B'}}>{item.judul}</Text>
                        <Text style={{fontSize: 12, fontFamily: 'SFProDisplay', color: '#26325B', marginTop: 10}}>{item.created_at}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            ))}
            
        </View>
        
    </View>
    )
}
