import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';

export default function index(props) {

    return (
        <View style={{paddingBottom: 20}}>
            
            <View>
                <Text style={styles.label}>Perihal</Text>
                <DropDownPicker
                    items={[
                    {label: 'Keluhan', value: 'keluhan'},
                    {label: 'Gangguan keamanan', value: 'Gangguan keamanan'},
                    {label: 'Saran & Kritik', value: 'Saran & Kritik'},
                    {label: 'Permohonan Informasi', value: 'Permohonan Informasi'}
                    ]}
                
                    containerStyle={{ height: 60 }}
                    style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    dropDownMaxHeight={300}
                    labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    onChangeItem={(value) => props.handlePerihal(value.value)}
                />
                <View></View>
           
            </View>
            <View>
                <Text style={styles.label}>Judul</Text>
                <TextInput placeholder="Judul Pesan" style={styles.form} onChangeText={(value) => props.handleJudul(value)} />
            </View>
            <View>
                <Text style={styles.label}>Isi Pesan</Text>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Isi Pesan'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        onChangeText={(value) => props.handleIsi(value)}
                    />
                </View>
            </View>
            <TouchableOpacity onPress={() => props.addPesan()}>
                <View style={{height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                    <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Kirim Pesan</Text>
                </View>
            </TouchableOpacity>
            
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 60,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
})
