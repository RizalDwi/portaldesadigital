import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, ToastAndroid } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';
import LoadingScreen from './../../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {

    const [namaBarang, setNamaBarang] = useState('')
    const [kodeBarang, setKodeBarang] = useState('')
    const [nomorRegister, setNomorRegister] = useState('')
    const [merk, setMerk] = useState('')
    const [cc, setCC]= useState('')
    const [bahan, setBahan] = useState('')
    const [tahunPembelian, setTahunPembelian] = useState('')
    const [namaPabrik, setNamaPabrik] = useState('')
    const [nomorRangka, setNomorRangka] = useState('')
    const [nomorMesin, setNomorMesin] = useState('')
    const [nomorPolisi, setNomorPolisi] = useState('')
    const [bpkb, setBpkb] = useState('')
    const [asalBarang, setAsalBarang] = useState('')
    const [harga, setHarga] = useState(0)
    const [keterangan, setKeterangan] = useState('')
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function getNomorRegister() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id_desa: route.params.idDesa
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/nomor_register_mesin', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        console.log(res)
                        setNomorRegister(res.data.data)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        console.log("Id Desa form ->", route.params.idDesa)
        getNomorRegister()
      
      }, [])

      const addIventaris = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    nama_barang: namaBarang,
                    kode_barang: kodeBarang,
                    nomor_register: nomorRegister,
                    merk: merk,
                    cc: cc,
                    bahan: bahan,
                    tahun_pembelian: tahunPembelian,
                    nama_pabrik: namaPabrik,
                    nomor_rangka: nomorRangka,
                    nomor_mesin: nomorMesin,
                    nomor_polisi: nomorPolisi,
                    bpkb: bpkb,
                    asal_usul_barang: asalBarang,
                    harga: harga,
                    keterangan: keterangan,
                    id_desa: route.params.idDesa
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_iventaris_mesin', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    console.log("Data 2 -> ", data)
                    setNamaBarang('')
                    setKodeBarang('')
                    setNomorRegister(res.data.nomor_register)
                    setMerk('')
                    setCC('')
                    setBahan('')
                    setTahunPembelian('')
                    setNamaPabrik('')
                    setNomorRangka('')
                    setNomorMesin('')
                    setNomorPolisi('')
                    setBpkb('')
                    setAsalBarang('')
                    setHarga(0)
                    setKeterangan('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };
      
      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                    <Text style={styles.label}>Nama Barang</Text>
                    <TextInput placeholder="Nama Barang" style={styles.form} onChangeText={(value) => setNamaBarang(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Kode Barang</Text>
                    <TextInput placeholder="Kode Barang" style={styles.form} onChangeText={(value) => setKodeBarang(value)}/>
                </View>
                <View>
                    <Text style={styles.label}>Nomor Registrasi</Text>
                    <TextInput placeholder="Nomor Registrasi" style={styles.form} value={nomorRegister} editable={false} />
                </View>
                <View>
                    <Text style={styles.label}>Merk/Type</Text>
                    <TextInput placeholder="Merk/Type" style={styles.form} onChangeText={(value) => setMerk(value)}/>
                </View>
                <View>
                    <Text style={styles.label}>Ukuran/CC</Text>
                    <TextInput placeholder="Ukuran/CC" style={styles.form} onChangeText={(value) => setCC(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Bahan</Text>
                    <TextInput placeholder="Bahan" style={styles.form} onChangeText={(value) => setBahan(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Tahun Pembelian</Text>
                    <DropDownPicker
                        items={[
                        {label: '2021', value: '2021'},
                        {label: '2020', value: '2020'},
                        {label: '2019', value: '2019'},
                        {label: '2018', value: '2018'},
                        {label: '2017', value: '2017'},
                        {label: '2016', value: '2016'},
                        {label: '2015', value: '2015'},
                        {label: '2014', value: '2014'},
                        {label: '2013', value: '2013'},
                        {label: '2012', value: '2012'},
                        {label: '2011', value: '2011'},
                        {label: '2010', value: '2010'},
                        {label: '2009', value: '2009'},
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}

                        onChangeItem={(value) => setTahunPembelian(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Nama Pabrik</Text>
                    <TextInput placeholder="Nama Pabrik" style={styles.form} onChangeText={(value) => setNamaPabrik(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Rangka</Text>
                    <TextInput placeholder="Nomor Rangka" style={styles.form} onChangeText={(value) => setNomorRangka(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Mesin</Text>
                    <TextInput placeholder="Nomor Mesin" style={styles.form} onChangeText={(value) => setNomorMesin(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Polisi</Text>
                    <TextInput placeholder="Nomor Polisi" style={styles.form} onChangeText={(value) => setNomorPolisi(value)} />
                </View>
                <View>
                    <Text style={styles.label}>BPKB</Text>
                    <TextInput placeholder="BPKB" style={styles.form} onChangeText={(value) => setBpkb(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Asal Usul</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Bantuan Kabupaten', value: 'Bantuan Kabupaten'},
                        {label: 'Bantuan Pemerintah', value: 'Bantuan Pemerintah'},
                        {label: 'Bantuan Provinsi', value: 'Bantuan Provinsi'},
                        {label: 'Pembelian Sendiri', value: 'Pembelian Sendiri'},
                        {label: 'Sumbangan', value: 'Sumbangan'},
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setAsalBarang(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Harga</Text>
                    <TextInput placeholder="Harga" style={styles.form} onChangeText={(value) => setHarga(value)} keyboardType="numeric" />
                </View>
                <View>
                    <Text style={styles.label}>Keterangan</Text>
                    <View style={styles.container}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            placeholder={'Keterangan'}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(value) => setKeterangan(value)}
                        />
                    </View>
                </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => addIventaris()}>
                    <View style={{width: '100%', height: 60, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
})
