import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';
import DateTimePicker from '@react-native-community/datetimepicker';
import LoadingScreen from './../../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {
    const [mode1, setMode1] = useState('date');
    const [show1, setShow1] = useState(false);
    const [mode2, setMode2] = useState('date');
    const [show2, setShow2] = useState(false);

    const [namaBarang, setNamaBarang] = useState('')
    const [fisikBangunan, setFisikBangunan] = useState('')
    const [bangunanBertingkat, setBangunanBertingkat] = useState('')
    const [konstruksiBeton, setKonstruksiBeton] = useState(false)
    const [alamat, setAlamat] = useState('')
    const [nomorBangunan, setNomorBangunan] = useState('')
    const [tanggalBangunan, setTanggalBangunan] = useState(new Date())
    const [tanggalMulai, setTanggalMulai] = useState(new Date())
    const [statusTanah, setStatusTanah] = useState('')
    const [kodeTanah, setKodeTanah] = useState('')
    const [asalBarang, setAsalBarang] = useState('')
    const [harga, setHarga] = useState(0)
    const [keterangan, setKeterangan] = useState('')
    const [loading, setLoading] = useState(false)
    const [luas, setLuas] = useState('')

    

    const onChange1 = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow1(Platform.OS === 'ios');
        setTanggalBangunan(currentDate);
      };

      const onChange2 = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow2(Platform.OS === 'ios');
        setTanggalMulai(currentDate);
      };
    
      const showMode1 = (currentMode) => {
        setShow1(true);
        setMode1(currentMode);
      };
    
      const showDatepicker1 = () => {
        showMode1('date');
      };

      const showMode2 = (currentMode) => {
        setShow2(true);
        setMode2(currentMode);
      };
    
      const showDatepicker2 = () => {
        showMode2('date');
      };

      const addIventaris = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    nama_barang: namaBarang,
                    fisik_bangunan: fisikBangunan,
                    bangunan_bertingkat: bangunanBertingkat,
                    konstruksi_beton: konstruksiBeton,
                    letak_atau_alamat: alamat,
                    nomor_bangunan: nomorBangunan,
                    tanggal_dokumen_bangunan: tanggalBangunan,
                    tanggal_mulai: tanggalMulai,
                    status_tanah: statusTanah,
                    nomor_kode_tanah: kodeTanah,
                    asal_usul_barang: asalBarang,
                    harga: harga,
                    keterangan: keterangan,
                    id_desa: route.params.idDesa,
                    luas: luas
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_iventaris_konstruksi_dalam_pengerjaan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    console.log("Data 2 -> ", data)
                    setNamaBarang('')
                    setFisikBangunan('')
                    setBangunanBertingkat('')
                    setKonstruksiBeton(false)
                    setAlamat('')
                    setNomorBangunan('')
                    setTanggalBangunan(new Date())
                    setTanggalMulai(new Date())
                    setStatusTanah('')
                    setKodeTanah('')
                    setAsalBarang('')
                    setHarga(0)
                    setKeterangan('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    if(loading) {
        return <LoadingScreen />
    }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                    <Text style={styles.label}>Nama Barang</Text>
                    <TextInput placeholder="Nama Barang" style={styles.form} onChangeText={(value) => setNamaBarang(value)} />
                </View>
                
                <View>
                    <Text style={styles.label}>Fisik Bangunan</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Darurat', value: 'Darurat'},
                        {label: 'Permanen', value: 'Permanen'},
                        {label: 'Semi Permanen', value: 'Semi Permanen'},
                        ]}
                    
                        containerStyle={{ height: 60 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setFisikBangunan(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Bangunan Bertingkat (Lantai)</Text>
                    <DropDownPicker
                        items={[
                        {label: '1', value: '1'},
                        {label: '2', value: '2'},
                        {label: '3', value: '3'},
                        {label: '4', value: '4'}
                        ]}
                    
                        containerStyle={{ height: 60 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setBangunanBertingkat(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Konstruksi Beton</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Ya', value: true},
                        {label: 'Tidak', value: false}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setKonstruksiBeton(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Luas (M2)</Text>
                    <TextInput placeholder="Luas" style={styles.form} onChangeText={(value) => setLuas(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Lokasi</Text>
                    <TextInput placeholder="Lokasi" style={styles.form} onChangeText={(value) => setAlamat(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Bangunan</Text>
                    <TextInput placeholder="Nomor Bangunan" style={styles.form} onChangeText={(value) => setNomorBangunan(value)} />
                </View>
                    {show1 && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={tanggalBangunan}
                    mode={mode1}
                    display="default"
                    onChange={onChange1}
                    />
                )}
                <View>
                    <Text style={styles.label}>Tanggal Dokumen Bangunan</Text>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={showDatepicker1}>
                            <View>
                                <Image source={require('./../../../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                            </View>
                        </TouchableOpacity>
                        <TextInput style={styles.formTanggal} value={tanggalBangunan.toDateString()} editable={false} />
                    </View>
                </View>
                {show2 && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={tanggalMulai}
                    mode={mode2}
                    display="default"
                    onChange={onChange2}
                    />
                )}
                <View>
                    <Text style={styles.label}>Tanggal Mulai</Text>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={showDatepicker2}>
                            <View>
                                <Image source={require('./../../../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                            </View>
                        </TouchableOpacity>
                        <TextInput style={styles.formTanggal} value={tanggalMulai.toDateString()} editable={false} />
                    </View>
                </View>
                <View>
                    <Text style={styles.label}>Status Tanah</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Tanah Milik Pemda', value: 'Tanah Milik Pemda'},
                        {label: 'Tanah Negara', value: 'Tanah Negara'},
                        {label: 'Tanah Hak Ulayat (tanah Masyarakat Hukum Adat)', value: 'Tanah Hak Ulayat'},
                        {label: 'Tanah Hak (Tanah Kepunyaan Perorangan atau Badan Hukum)', value: 'Tanah Hak'}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setStatusTanah(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Nomor Kode Tanah</Text>
                    <TextInput placeholder="Nomor Kode Tanah" style={styles.form} onChangeText={(value) => setKodeTanah(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Asal Usul</Text>
                    <DropDownPicker
                        items={[
                            {label: 'Bantuan Kabupaten', value: 'Bantuan Kabupaten'},
                            {label: 'Bantuan Pemerintah', value: 'Bantuan Pemerintah'},
                            {label: 'Bantuan Provinsi', value: 'Bantuan Provinsi'},
                            {label: 'Pembelian Sendiri', value: 'Pembelian Sendiri'},
                            {label: 'Sumbangan', value: 'Sumbangan'},
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setAsalBarang(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Harga</Text>
                    <TextInput placeholder="Harga" style={styles.form}  onChangeText={(value) => setHarga(value)}/>
                </View>
                <View>
                    <Text style={styles.label}>Keterangan</Text>
                    <View style={styles.container}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            placeholder={'Keterangan'}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(value) => setKeterangan(value)}
                        />
                    </View>
                </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => addIventaris()}>
                    <View style={{height: 60, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
               
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      formTanggal: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC',
            marginLeft: 5
        }
})
