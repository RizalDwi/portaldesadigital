import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, Platform, TouchableOpacity, Image } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker';

export default function index(props) {
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        props.tanggal(currentDate)
        setDate(currentDate);
      };
    
      const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
      };
    
      const showDatepicker = () => {
        showMode('date');
      };
    
      

    return (
        <View>
            <View>
                <Text style={styles.label}>Nama</Text>
                <TextInput placeholder="Nama Anda" style={styles.form} onChangeText = {(value) => props.nama(value)} />
            </View>
            <View>
                <Text style={styles.label}>NIK</Text>
                <TextInput placeholder="NIK" style={styles.form} onChangeText = {(value) => props.nik(value)} />
            </View>
            <View>
                <Text style={styles.label}>No. KK</Text>
                <TextInput placeholder="No. KK" style={styles.form} onChangeText = {(value) => props.kk(value)} />
            </View>
            <View>
                <Text style={styles.label}>No. Telp</Text>
                <TextInput placeholder="Nomor Telepon Anda" style={styles.form} onChangeText = {(value) => props.telp(value)} />
            </View>
            <View>
                <Text style={styles.label}>Alamat</Text>
                <TextInput placeholder="Alamat" style={styles.form} onChangeText = {(value) => props.alamat(value)} />
            </View>
            {show && (
                <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                display="default"
                onChange={onChange}
                />
            )}
            <View>
                <Text style={styles.label}>Tanggal Lahir</Text>
                <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity onPress={showDatepicker}>
                        <View>
                            <Image source={require('./../../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                        </View>
                    </TouchableOpacity>
                    <TextInput placeholder="Judul Pesan" style={styles.formTanggal} value={date.toDateString()} editable={false} />
                 </View>
            </View>
                
            
            
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        formTanggal: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC',
            marginLeft: 5
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
