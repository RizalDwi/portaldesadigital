import React, {useState, useContext} from 'react'
import { StyleSheet, Text, View, TextInput, Platform, TouchableOpacity, Image } from 'react-native'

import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';
import TanggalLahir from './../../../atom/DatePickerFormEditTanggalLahirAparatDesa'
import TanggalSKPengangkatan from './../../../atom/DatePickerFormEditTanggalPengangkatanAparat'
import TanggalSKPemberhentian from './../../../atom/DatePickerFormEditTanggalPemberhentianAparat'
import { RootContext } from './../../../../screens/admin/EditAparat'

export default function index() {
    const state = useContext(RootContext)  

    return (
        <View>
            <View>
                <Text style={styles.label}>Nama Pegawai Desa</Text>
                <TextInput placeholder="Nama" style={styles.form} value={state.nama} onChangeText={(value) =>  state.handleInputNama(value)} />
            </View>
            <View>
                <Text style={styles.label}>NIK</Text>
                <TextInput placeholder="NIK" style={styles.form} value={state.nik} onChangeText={(value) =>  state.handleInputNik(value)} />
            </View>
            <View>
                <Text style={styles.label}>NIAP</Text>
                <TextInput placeholder="NIAP" style={styles.form} value={state.niap} onChangeText={(value) =>  state.handleInputNiap(value)} />
            </View>
            <View>
                <Text style={styles.label}>NIP</Text>
                <TextInput placeholder="NIP" style={styles.form} value={state.nip} onChangeText={(value) =>  state.handleInputNip(value)} />
            </View>
            <View>
                <Text style={styles.label}>Tempat Lahir</Text>
                <TextInput placeholder="Judul Pesan" style={styles.form} value={state.tempatLahir} onChangeText={(value) =>  state.handleInputTempatLahir(value)} />
            </View>
            <TanggalLahir />
            
            <View>
                <Text style={styles.label}>Jenis Kelamin</Text>
                <DropDownPicker
                    items={[
                    {label: 'Laki-laki', value: 'laki'},
                    {label: 'Perempuan', value: 'perempuan'}
                    ]}
                
                    containerStyle={{ height: 60 }}
                    style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    dropDownMaxHeight={300}
                    labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    onChangeItem={(value) => state.handleInputJenisKelamin(value.value)}
                />
                <View></View>
           
            </View>
            <View>
                <Text style={styles.label}>Pendidikan</Text>
                <DropDownPicker
                    items={[
                    {label: 'TIDAK / BELUM SEKOLAH', value: 'TIDAK / BELUM SEKOLAH'},
                    {label: 'BELUM TAMAT SD/SEDERAJAT', value: 'BELUM TAMAT SD/SEDERAJAT'},
                    {label: 'TAMAT SD / SEDERAJAT', value: 'TAMAT SD / SEDERAJAT'},
                    {label: 'SLTP/SEDERAJAT', value: 'SLTP/SEDERAJAT'},
                    {label: 'SLTA / SEDERAJAT', value: 'SLTA / SEDERAJAT'},
                    {label: 'DIPLOMA I / II', value: 'DIPLOMA I / II'},
                    {label: 'AKADEMI/ DIPLOMA III/S. MUDA', value: 'AKADEMI/ DIPLOMA III/S. MUDA'},
                    {label: 'DIPLOMA IV/ STRATA I', value: 'DIPLOMA IV/ STRATA I'},
                    {label: 'STRATA II', value: 'STRATA II'},
                    {label: 'STRATA III', value: 'STRATA III'}
                    ]}
                
                    containerStyle={{ height: 60 }}
                    style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    dropDownMaxHeight={300}
                    labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    onChangeItem={(value) => state.handleInputPendidikan(value.value)}
                />
                <View></View>
           
            </View>
            <View>
                <Text style={styles.label}>Agama</Text>
                <DropDownPicker
                    items={[
                    {label: 'ISLAM', value: 'ISLAM'},
                    {label: 'KRISTEN', value: 'KRISTEN'},
                    {label: 'KATHOLIK', value: 'KATHOLIK'},
                    {label: 'HINDU', value: 'HINDU'},
                    {label: 'BUDHA', value: 'BUDHA'},
                    {label: 'KHONGHUCU', value: 'KHONGHUCU'},
                    {label: 'KEPERCAYAAN TERHADAP TUHAN YME / LAINNYA', value: 'KEPERCAYAAN TERHADAP TUHAN YME / LAINNYA'}
                    ]}
                
                    containerStyle={{ height: 60 }}
                    style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    dropDownMaxHeight={300}
                    labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    onChangeItem={(value) => state.handleInputAgama(value.value)}
                />
                <View></View>
           
            </View>
            <View>
                <Text style={styles.label}>Pangkat / Golongan</Text>
                <TextInput placeholder="Pangkat / Golongan" style={styles.form} value={state.pangkat} onChangeText={(value) =>  state.handleInputPangkat(value)} />
            </View>
            <View>
                <Text style={styles.label}>Nomor SK Pengangkatan</Text>
                <TextInput placeholder="Pangkat / Golongan" style={styles.form} value={state.nomorPengangkatan} onChangeText={(value) =>  state.handleInputNomorPengangkatan(value)} />
            </View>
           <TanggalSKPengangkatan />
            <View>
                <Text style={styles.label}>Nomor SK Pemberhentian</Text>
                <TextInput placeholder="Nomor SK Pemberhentian" style={styles.form} value={state.nomorPemberhentian} onChangeText={(value) =>  state.handleInputNomorPemberhentian(value)} />
            </View>
            <TanggalSKPemberhentian />
            <View>
                <Text style={styles.label}>Masa Jabatan</Text>
                <TextInput placeholder="Masa Jabatan" style={styles.form} value={state.masaJabatan} onChangeText={(value) =>  state.handleInputMasaJabatan(value)} />
            </View>
            <View>
                <Text style={styles.label}>Jabatan</Text>
                <TextInput placeholder="Jabatan" style={styles.form} value={state.jabatan} onChangeText={(value) =>  state.handleInputJabatan(value)} />
            </View>
            <View>
                <Text style={styles.label}>Status</Text>
                <DropDownPicker
                    items={[
                    {label: 'Aktif', value: 'Aktif'},
                    {label: 'Tidak Aktif', value: 'Tidak Aktif'}
                    ]}
                
                    containerStyle={{ height: 60 }}
                    style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    dropDownMaxHeight={300}
                    labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    onChangeItem={(value) => state.handleInputStatus(value.value)}
                />
                <View></View>
           
            </View>
            <View>
                <Text style={styles.label}>Tupoksi</Text>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Tupoksi'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        value={state.tupoksi}
                        onChangeText={(value) =>  state.handleInputTupoksi(value)}
                    />
                </View>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
        formTanggal: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC',
            marginLeft: 5
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      }
})
