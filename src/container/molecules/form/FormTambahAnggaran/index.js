import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {

    const [dataKategori, setDataKategori] = useState([])
    const [dataSubkategori, setDataSubkategori] = useState([])
    const [loading, setLoading] = useState(true)
    const [loading2, setLoading2] = useState(false)
    const [namaKegiatan, setNamaKegiatan] = useState('')
    const [periode, setPeriode] = useState('')
    const [tahap, setTahap] = useState('')
    const [jumlahAnggaran, setJumlahAnggaran] = useState(0)
    const [penanggungJawab, setPenanggungJawab] = useState('')
    const [listPenanggungJawab, setListPenanggungJawab] = useState([])
    const [idSubkategori, setIdSubkategori] = useState('')

    useEffect(() => {
        async function getDataKategori() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id_desa: route.params.idDesa
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_kategori', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        setDataKategori(res.data.data)
                        setListPenanggungJawab(res.data.aparat)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataKategori()
      
      }, [])

      async function getListSubkategori(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_kategori: id,
                }
                setLoading2(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_subkategori', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setDataSubkategori(res.data.data)
                    setLoading2(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function addKegiatan() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    nama_kegiatan: namaKegiatan,
                    periode: periode,
                    tahap: tahap,
                    jumlah_anggaran: jumlahAnggaran,
                    penanggung_jawab: penanggungJawab,
                    id_subkategori: idSubkategori,
                    id_desa: route.params.idDesa,
                    id_penanggung_jawab: penanggungJawab
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tambah_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setNamaKegiatan('')
                    setPeriode('')
                    setTahap('')
                    setJumlahAnggaran(0)
                    setPenanggungJawab('')
                    setIdSubkategori('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

      if(loading){
          return <LoadingScreen />
      }
    
      function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
         var x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    
    }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                        <Text style={styles.label}>Kategori Anggaran</Text>
                        <DropDownPicker
                            items={dataKategori}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => getListSubkategori(value.value)}
                        />
                        <View></View>
            
                </View>
                <View style={{width: "100%"}}>
                        <Text style={styles.label}>Subkategori Anggaran</Text>
                        {(() => {
                            if (loading2){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={dataSubkategori}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => setIdSubkategori(value.value)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                </View>
                <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{flex: 1, paddingRight: 5}}>
                            <Text style={styles.label}>Periode</Text>
                            <DropDownPicker
                                items={[
                                {label: "2019", value: "2019"},
                                {label: "2020", value: "2020"},
                                {label: "2021", value: "2021"}
                                ]}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={300}
                                labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setPeriode(value.value)}
                            />
                            <View></View>
                
                    </View>
                    <View style={{flex: 1, paddingLeft: 5}}>
                            <Text style={styles.label}>Pilih Tahap</Text>
                            <DropDownPicker
                                items={[
                                {label: 'Tahap 1', value: 'Tahap 1'},
                                {label: 'Tahap 2', value: 'Tahap 2'},
                                {label: 'Tahap 3', value: 'Tahap 3'},
                                ]}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={300}
                                labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setTahap(value.value)}
                            />
                            <View></View>
                
                    </View>
                    
                </View>
                <View>
                    <Text style={styles.label}>Penanggung Jawab</Text>
                    <DropDownPicker
                            items={listPenanggungJawab}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => setPenanggungJawab(value.value)}
                        />
                        <View></View>
                </View>
                <View>
                    <Text style={styles.label}>Nama Kegiatan</Text>
                    <TextInput placeholder="Nama Kegiatan" style={styles.form} onChangeText={(value) => setNamaKegiatan(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Jumlah Anggran (Rp)</Text>
                    <Text style={styles.labelComa}>Rp. {addCommas(jumlahAnggaran)}</Text>
                    <TextInput placeholder="Jumlah Anggaran (Rp)" style={styles.form} keyboardType="numeric" onChangeText={(value) => setJumlahAnggaran(value)} />
                </View>
               
               <TouchableOpacity style={{width: '100%'}} onPress={() => addKegiatan()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelComa : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 10
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
