import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, ToastAndroid, Image } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';
import LoadingScreen from './../../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import DateTimePicker from '@react-native-community/datetimepicker';

export default function index({route}) {

    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const [namaBarang, setNamaBarang] = useState('')
    const [kodeBarang, setKodeBarang] = useState('')
    const [nomorRegister, setNomorRegister] = useState('')
    const [KondisiBangunan, setKondisiBangunan] = useState('')
    const [konstruksi, setKonstruksi] = useState('')
    const [lebar, setLebar] = useState('')
    const [panjang, setPanjang] = useState(0)
    const [luas, setLuas] = useState('')
    const [alamat, setAlamat] = useState('')
    const [nomorKepemilikan, setNomorKepemilikan] = useState('')
    const [tanggalKepemilkan, setTanggalKepemilikan] = useState(new Date())
    const [statusTanah, setStatusTanah] = useState('')
    const [kodeTanah, setKodeTanah] = useState('')
    const [asalBarang, setAsalBarang] = useState('')
    const [harga, setHarga] = useState('')
    const [keterangan, setKeterangan] = useState('')
    const [loading, setLoading] = useState(true)

    

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setTanggalBangunan(currentDate);
      };
    
      const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
      };
    
      const showDatepicker = () => {
        showMode('date');
      };

    useEffect(() => {
        async function getDataIventaris() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.id
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_iventaris_jaringan', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        setNamaBarang(res.data.data[0].nama_barang)
                        setKodeBarang(res.data.data[0].kode_barang)
                        setNomorRegister(res.data.data[0].nomor_register)
                        setKondisiBangunan(res.data.data[0].kondisi_bangunan)
                        setKonstruksi(res.data.data[0].konstruksi)
                        setLebar(res.data.data[0].lebar)
                        setPanjang(res.data.data[0].panjang)
                        setLuas(res.data.data[0].luas)
                        setAlamat(res.data.data[0].letak_atau_alamat)
                        setNomorKepemilikan(res.data.data[0].nomor_kepemilikan)
                        setTanggalKepemilikan(new Date(res.data.data[0].tanggal_kepemilikan))
                        setStatusTanah(res.data.data[0].status_tanah)
                        setKodeTanah(res.data.data[0].nomor_kode_tanah)
                        setAsalBarang(res.data.data[0].asal_usul_barang)
                        setHarga(res.data.data[0].harga)
                        setKeterangan(res.data.data[0].keterangan)
                        setLoading(false)
                        console.log(res.data.data)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        console.log("Id Desa form ->", route.params.idDesa)
        getDataIventaris()
      
      }, [])

      const updateIventaris = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    nama_barang: namaBarang,
                    kode_barang: kodeBarang,
                    nomor_register: nomorRegister,
                    kondisi_bangunan: KondisiBangunan,
                    konstruksi: konstruksi,
                    lebar: lebar,
                    panjang: panjang,
                    luas: luas,
                    letak_atau_alamat: alamat,
                    nomor_kepemilikan: nomorKepemilikan,
                    tanggal_kepemilikan: tanggalKepemilkan,
                    status_tanah: statusTanah,
                    nomor_kode_tanah: kodeTanah,
                    asal_usul_barang: asalBarang,
                    harga: harga,
                    keterangan: keterangan,
                    id: route.params.id,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_iventaris_jaringan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    console.log("Data 2 -> ", data)
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil diperbaharui", ToastAndroid.SHORT);
      };
      
      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                    <Text style={styles.label}>Nama Barang</Text>
                    <TextInput placeholder="Nama Barang" style={styles.form} value={namaBarang} onChangeText={(value) => setNamaBarang(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Kode Barang</Text>
                    <TextInput placeholder="Kode Barang" style={styles.form} value={kodeBarang} onChangeText={(value) => setKodeBarang(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Registrasi</Text>
                    <TextInput placeholder="Nomor Registrasi" style={styles.form} value={nomorRegister} editable={false} />
                </View>
                <View>
                    <Text style={styles.label}>Kondisi Bangunan</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Baik', value: 'Baik'},
                        {label: 'Rusak Ringan ', value: 'Rusak Ringan'},
                        {label: 'Rusak Sedang', value: 'Rusak Sedang'},
                        {label: 'Rusak Berat', value: 'Rusak Berat'}
                        ]}
                    
                        containerStyle={{ height: 60 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setKondisiBangunan(value.value)}
                    />
                    <View></View>
            
                </View>
                
                <View>
                    <Text style={styles.label}>Konstruksi</Text>
                    <View style={styles.container}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            placeholder={'Konstruksi'}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(value) => setKonstruksi(value)}
                            value={konstruksi}
                        />
                    </View>
                </View>
                <View>
                    <Text style={styles.label}>Panjang (KM)</Text>
                    <TextInput placeholder="Panjang (KM)" style={styles.form} value={panjang} onChangeText={(value) => setPanjang(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Lebar (M)</Text>
                    <TextInput placeholder="Lebar (M)" style={styles.form} value={lebar} onChangeText={(value) => setLebar(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Luas (M2)</Text>
                    <TextInput placeholder="Luas (M2)" style={styles.form} value={luas} onChangeText={(value) => setLuas(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Lokasi</Text>
                    <TextInput placeholder="Lokasi" style={styles.form} value={alamat} onChangeText={(value) => setAlamat(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Kepemilikan</Text>
                    <TextInput placeholder="Nomor Kepemilikan" style={styles.form} value={nomorKepemilikan} onChangeText={(value) => setNomorKepemilikan(value)} />
                </View>
                    {show && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={tanggalKepemilkan}
                    mode={mode}
                    display="default"
                    onChange={onChange}
                    />
                )}
                <View>
                    <Text style={styles.label}>Tanggal Dokumen Kepemilikan</Text>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={showDatepicker}>
                            <View>
                                <Image source={require('./../../../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                            </View>
                        </TouchableOpacity>
                        <TextInput placeholder="Judul Pesan" style={styles.formTanggal} value={tanggalKepemilkan.toDateString()} editable={false} />
                    </View>
                </View>
                <View>
                    <Text style={styles.label}>Status Tanah</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Tanah Milik Pemda', value: 'Tanah Milik Pemda'},
                        {label: 'Tanah Negara', value: 'Tanah Negara'},
                        {label: 'Tanah Hak Ulayat (tanah Masyarakat Hukum Adat)', value: 'Tanah Hak Ulayat (tanah Masyarakat Hukum Adat)'},
                        {label: 'Tanah Hak (Tanah Kepunyaan Perorangan atau Badan Hukum)', value: 'Tanah Hak (Tanah Kepunyaan Perorangan atau Badan Hukum)'}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setStatusTanah(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Nomor Kode Tanah</Text>
                    <TextInput placeholder="Nomor Kode Tanah" value={kodeTanah} style={styles.form} onChangeText={(value) => setKodeTanah(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Asal Usul</Text>
                    <DropDownPicker
                        items={[
                            {label: 'Bantuan Kabupaten', value: 'Bantuan Kabupaten'},
                            {label: 'Bantuan Pemerintah', value: 'Bantuan Pemerintah'},
                            {label: 'Bantuan Provinsi', value: 'Bantuan Provinsi'},
                            {label: 'Pembelian Sendiri', value: 'Pembelian Sendiri'},
                            {label: 'Sumbangan', value: 'Sumbangan'},
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setAsalBarang(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Harga</Text>
                    <TextInput placeholder="Harga" style={styles.form} value={harga.toString()} onChangeText={(value) => setHarga(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Keterangan</Text>
                    <View style={styles.container}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            placeholder={'Keterangan'}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(value) => setKeterangan(value)}
                            value={keterangan}
                        />
                    </View>
                </View>
                <TouchableOpacity onPress={() => updateIventaris()} style={{width: '100%'}}>
                    <View style={{width: '100%', height: 60, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
})
