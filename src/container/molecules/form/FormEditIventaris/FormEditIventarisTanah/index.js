import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, ToastAndroid, Image } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';
import LoadingScreen from './../../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import DateTimePicker from '@react-native-community/datetimepicker';

export default function index({route}) {

    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const [namaBarang, setNamaBarang] = useState('')
    const [kodeBarang, setKodeBarang] = useState('')
    const [nomorRegister, setNomorRegister] = useState('')
    const [luasTanah, setLuasTanah] = useState('')
    const [tahunPengadaan, setTahunPengadaan] = useState('')
    const [alamat, setAlamat] = useState('')
    const [hakTanah, setHakTanah] = useState('')
    const [tanggalSertfikat, setTanggalSertifikat] = useState(new Date())
    const [nomorSertifikat, setNomorSertifikat] = useState('')
    const [penggunaan, setPenggunaan] = useState('')
    const [asalBarang, setAsalBarang] = useState('')
    const [harga, setHarga] = useState('')
    const [keterangan, setKeterangan] = useState('')
    const [loading, setLoading] = useState(true)

    

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setTanggalBangunan(currentDate);
      };
    
      const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
      };
    
      const showDatepicker = () => {
        showMode('date');
      };

    useEffect(() => {
        async function getDataIventaris() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id: route.params.id
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_iventaris_tanah', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        setNamaBarang(res.data.data[0].nama_barang)
                        setKodeBarang(res.data.data[0].kode_barang)
                        setNomorRegister(res.data.data[0].nomor_register)
                        setLuasTanah(res.data.data[0].luas_tanah)
                        setTahunPengadaan(res.data.data[0].tahun_pengadaan)
                        setAlamat(res.data.data[0].letak_atau_alamat)
                        setHakTanah(res.data.data[0].hak_tanah)
                        setTanggalSertifikat(new Date(res.data.data[0].tanggal_sertifikat))
                        setNomorSertifikat(res.data.data[0].nomor_sertifikat)
                        setPenggunaan(res.data.data[0].penggunaan)
                        setAsalBarang(res.data.data[0].asal_usul_barang)
                        setHarga(res.data.data[0].harga)
                        setKeterangan(res.data.data[0].keterangan)
                        setLoading(false)
                        console.log(res.data.data)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        console.log("Id Desa form ->", route.params.idDesa)
        getDataIventaris()
      
      }, [])

      const updateIventaris = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    nama_barang: namaBarang,
                    kode_barang: kodeBarang,
                    nomor_register: nomorRegister,
                    luas_tanah: luasTanah,
                    letak_atau_alamat: alamat,
                    hak_tanah: hakTanah,
                    nomor_sertifikat: nomorSertifikat,
                    tanggal_sertifikat: tanggalSertfikat,
                    penggunaan: penggunaan,
                    asal_usul_barang: asalBarang,
                    harga: harga,
                    keterangan: keterangan,
                    tahun_pengadaan: tahunPengadaan,
                    id: route.params.id,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_iventaris_tanah', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    console.log("Data 2 -> ", data)
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }
    const showToast = () => {
        ToastAndroid.show("Data berhasil diperbaharui", ToastAndroid.SHORT);
      };
      
      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                    <Text style={styles.label}>Nama Barang</Text>
                    <TextInput placeholder="Nama Barang" style={styles.form} value={namaBarang} onChangeText={(value) => setNamaBarang(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Kode Barang</Text>
                    <TextInput placeholder="Kode Barang" style={styles.form} value={kodeBarang} onChangeText={(value) => setKodeBarang(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Nomor Registrasi</Text>
                    <TextInput placeholder="Nomor Registrasi" style={styles.form} value={nomorRegister} editable={false} />
                </View>
                <View>
                    <Text style={styles.label}>Luas Tanah (M2)</Text>
                    <TextInput placeholder="Luas (M2)" style={styles.form} value={luasTanah} onChangeText={(value) => setLuasTanah(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Tahun Pengadaan</Text>
                    <DropDownPicker
                        items={[
                        {label: '2020', value: '2020'},
                        {label: '2021', value: '2021'}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setTahunPengadaan(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Lokasi</Text>
                    <TextInput placeholder="Lokasi" style={styles.form} value={alamat} onChangeText={(value) => setAlamat(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Hak Tanah</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Hak Pakai', value: 'Hak Pakai'},
                        {label: 'Hak Pengelolaan', value: 'Hak Pengelolaan'}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setHakTanah(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Nomor Sertifikat</Text>
                    <TextInput placeholder="Nomor Sertifikat" style={styles.form} value={nomorSertifikat} onChangeText={(value) => setNomorSertifikat(value)} />
                </View>
                    {show && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={tanggalSertfikat}
                    mode={mode}
                    display="default"
                    onChange={onChange}
                    />
                )}
                <View>
                    <Text style={styles.label}>Tanggal Sertifikat</Text>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={showDatepicker}>
                            <View>
                                <Image source={require('./../../../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                            </View>
                        </TouchableOpacity>
                        <TextInput placeholder="Judul Pesan" style={styles.formTanggal} value={tanggalSertfikat.toDateString()} editable={false} />
                    </View>
                </View>
                <View>
                    <Text style={styles.label}>Penggunaan</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Industri', value: 'Industri'},
                        {label: 'Jalan', value: 'Jalan'},
                        {label: 'Komersial', value: 'Komersial'},
                        {label:  'Pemukiman', value: 'Pemukiman'},
                        {label: 'Tanah Publik', value: 'Tanah Publik'},
                        {label: 'Tanah Kosong', value: "Tanah Kosong"}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setPenggunaan(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Asal Usul</Text>
                    <DropDownPicker
                        items={[
                        {label: 'Bantuan Kabupaten', value: 'Bantuan Kabupaten'},
                        {label: 'Pembelian Sendiri', value: 'Pembelian Sendiri'}
                        ]}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => setAsalBarang(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>Harga</Text>
                    <TextInput placeholder="Harga" style={styles.form} value={harga.toString()} onChangeText={(value) => setHarga(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Keterangan</Text>
                    <View style={styles.container}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            placeholder={'Keterangan'}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(value) => setKeterangan(value)}
                            value={keterangan}
                        />
                    </View>
                </View>
                <TouchableOpacity style={{width: '100%'}} onPress={() => updateIventaris()}>
                    <View style={{width: '100%', height: 60, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
                
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
})
