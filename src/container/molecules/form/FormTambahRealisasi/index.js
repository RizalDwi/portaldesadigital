import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {

    const [dataKategori, setDataKategori] = useState([])
    const [dataSubkategori, setDataSubkategori] = useState([])
    const [loading, setLoading] = useState(true)
    const [loading2, setLoading2] = useState(false)
    const [loading3, setLoading3] = useState(false)
    const [loading4, setLoading4] = useState(false)
    const [namaKegiatan, setNamaKegiatan] = useState('')
    const [periode, setPeriode] = useState('')
    const [tahap, setTahap] = useState('')
    const [jumlahAnggaran, setJumlahAnggaran] = useState('')
    const [jumlahRealisasi, setJumlahRealisasi] = useState('')
    const [penanggungJawab, setPenanggungJawab] = useState('')
    const [dataKegiatan, setDataKegiatan] = useState([])
    const [idApbdes, setIdApbdes] = useState('')
    const [kegiatan, setKegiatan] = useState([])

    useEffect(() => {
        async function getDataKategori() {
            var token = await Asynstorage.getItem('api_token')
            try {
                await Asynstorage.getItem('api_token', (error, result) =>{
    
                  if (result){
    
                    let data = {
                        id_desa: route.params.idDesa
                    }
    
                    Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_kategori', data, {
                        timeout: 20000,
                        headers: {
                            'Authorization' : 'Bearer ' + token
                        }
                    })
                    .then((res) => {
                        setDataKategori(res.data.data)
                        setLoading(false)
                    
                    })
                    .catch((err) => {
                        alert(err)
                    })
                  }
                })
      
            } catch(err) {
              console.log("Logout -> error : ",err)
            }
        }
        getDataKategori()
      
      }, [])

      async function getListSubkategori(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_kategori: id,
                }
                setLoading2(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_subkategori', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setDataSubkategori(res.data.data)
                    setLoading2(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function getListKegiatan(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_subkategori: id,
                }
                setLoading3(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_kegiatan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setDataKegiatan(res.data.data)
                    setLoading3(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function getKegiatan(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id,
                }
                setLoading4(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_kegiatan_realisasi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setIdApbdes(res.data.data[0].id)
                    setPeriode(res.data.data[0].periode)
                    setTahap(res.data.data[0].tahap)
                    setPenanggungJawab(res.data.data[0].aparat)
                    setJumlahAnggaran(res.data.data[0].jumlah_anggaran)
                    setLoading4(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }


    async function addRealisasi() {
        if (jumlahRealisasi > jumlahAnggaran){
            alert('Realisasi tidak boleh melebihi anggaran')
        } else {
            var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    jumlah_anggaran: jumlahRealisasi,
                    id_apbdes: idApbdes,
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_realisasi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setPeriode('')
                    setTahap('')
                    setJumlahAnggaran('')
                    setJumlahRealisasi('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

      if(loading){
          return <LoadingScreen />
      }

      function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
         var x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    
    }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                        <Text style={styles.label}>Kategori Anggaran</Text>
                        <DropDownPicker
                            items={dataKategori}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => getListSubkategori(value.value)}
                        />
                        <View></View>
            
                </View>
                <View style={{width: "100%"}}>
                        <Text style={styles.label}>Subkategori Anggaran</Text>
                        {(() => {
                            if (loading2){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={dataSubkategori}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => getListKegiatan(value.value)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                </View>
                <View style={{width: "100%"}}>
                        <Text style={styles.label}>Nama Kegiatan</Text>
                        {(() => {
                            if (loading3){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={dataKegiatan}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => getKegiatan(value.value)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                </View>
                <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{flex: 1, paddingRight: 5}}>
                            <Text style={styles.label}>Periode</Text>
                            {(() => {
                            if (loading4){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <TextInput placeholder="Periode" style={styles.form} editable={false} value={periode} />
                            )
                        })()}
                            
                
                    </View>
                    <View style={{flex: 1, paddingLeft: 5}}>
                            <Text style={styles.label}>Tahap</Text>
                            
                            {(() => {
                            if (loading4){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <TextInput placeholder="Tahap" value={tahap} style={styles.form} editable={false} />
                            )
                        })()}
                
                    </View>
                    
                </View>
                <View>
                    <Text style={styles.label}>Penanggung Jawab</Text>
                    {(() => {
                            if (loading4){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <TextInput placeholder="Penanggung Jawab" style={styles.form} value={penanggungJawab} editable={false}  />
                            )
                        })()}
                    
                </View>
                <View>
                    <Text style={styles.label}>Jumlah Anggran (Rp)</Text>
                    {(() => {
                            if (loading4){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <TextInput placeholder="Jumlah Anggaran (Rp)" style={styles.form} value={addCommas(jumlahAnggaran.toString())} editable={false} keyboardType="numeric"  />
                            )
                        })()}
                   
                </View>
                <View>
                    <Text style={styles.label}>Jumlah Realisasi (Rp)</Text>
                    <Text style={styles.labelComa}>Rp. {addCommas(jumlahRealisasi)}</Text>
                    <TextInput placeholder="Jumlah Anggaran (Rp)" style={styles.form} keyboardType="numeric" onChangeText={(value) => setJumlahRealisasi(value)} />
                </View>
               
               <TouchableOpacity style={{width: '100%'}} onPress={() => addRealisasi()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelComa : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 10
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
