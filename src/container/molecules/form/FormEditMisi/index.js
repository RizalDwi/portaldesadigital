import React, {useContext} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native'
import { RootContext } from './../../../../screens/admin/EditMisi'

export default function index() {
    const state = useContext(RootContext)

    return (
        <View>
                <View>
                    <Text style={styles.label}>Misi Desa</Text>
                    <TextInput placeholder="Misi Desa" style={styles.form} value={state.misi} onChangeText={(value) =>  state.handleInputMisi(value)}/>
                </View>
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateMisi()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
