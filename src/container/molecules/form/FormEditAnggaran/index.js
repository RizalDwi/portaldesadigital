import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {

    const [dataKategori, setDataKategori] = useState([])
    const [dataSubkategori, setDataSubkategori] = useState([])
    const [loading, setLoading] = useState(true)
    const [loading2, setLoading2] = useState(false)
    const [namaKegiatan, setNamaKegiatan] = useState('')
    const [periode, setPeriode] = useState('')
    const [tahap, setTahap] = useState('')
    const [jumlahAnggaran, setJumlahAnggaran] = useState(0)
    const [penanggungJawab, setPenanggungJawab] = useState('')
    const [idSubkategori, setIdSubkategori] = useState('')
    const [namaKategori, setNamaKategori] = useState('')
    const [namaSubkategori, setNamaSubkategori] = useState('')
    const [listPenanggungJawab, setListPenanggungJawab] = useState([])
    const [idPenanggungJawab, setIdPenanggungJawab] = useState('')

    async function getDataKategori() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_kategori', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataKategori(res.data.data)
                    setListPenanggungJawab(res.data.aparat)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function getDataKegiatan() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_apbdes_satuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setNamaKegiatan(res.data.data[0].nama_kegiatan)
                    setPeriode(res.data.data[0].periode)
                    setTahap(res.data.data[0].tahap)
                    setJumlahAnggaran(res.data.data[0].jumlah_anggaran)
                    setIdSubkategori(res.data.data[0].id_subkategori)
                    setPenanggungJawab(res.data.data[0].nama)
                    setNamaKategori(res.data.data[0].nama_kategori)
                    setNamaSubkategori(res.data.data[0].nama_subkategori)
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        getDataKategori()
        getDataKegiatan()
      
      }, [])

      async function getListSubkategori(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_kategori: id,
                }
                setLoading2(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_list_subkategori', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setDataSubkategori(res.data.data)
                    setLoading2(false)
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function updateKegiatan() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    nama_kegiatan: namaKegiatan,
                    periode: periode,
                    tahap: tahap,
                    jumlah_anggaran: jumlahAnggaran,
                    id_penanggung_jawab: idPenanggungJawab,
                    id_subkategori: idSubkategori,
                    id: route.params.id
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/edit_apbdes', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataKegiatan()
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil diubah", ToastAndroid.SHORT);
      };

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{padding: 20, flex: 1}}>
            <ScrollView>
                <View>
                        <Text style={styles.label}>Kategori Anggaran</Text>
                        <Text style={styles.labelIsi}>Kategori saat ini : {namaKategori}</Text>
                        <DropDownPicker
                            items={dataKategori}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => getListSubkategori(value.value)}
                        />
                        <View></View>
            
                </View>
                <View style={{width: "100%"}}>
                        <Text style={styles.label}>Subkategori Anggaran</Text>
                        <Text style={styles.labelIsi}>Subkategori saat ini : {namaSubkategori}</Text>
                        {(() => {
                            if (loading2){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <View>
                                    <DropDownPicker
                                        items={dataSubkategori}
                                    
                                        containerStyle={{ height: 40 }}
                                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                                        dropDownMaxHeight={300}
                                        labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        onChangeItem={(value) => setIdSubkategori(value.value)}
                                    />
                                    <View></View>
                                </View>
                            )
                        })()}
                </View>
                <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{flex: 1, paddingRight: 5}}>
                            <Text style={styles.label}>Periode</Text>
                            <Text style={styles.labelIsi}>Periode saat ini : {periode}</Text>
                            <DropDownPicker
                                items={[
                                {label: "2019", value: "2019"},
                                {label: "2020", value: "2020"},
                                {label: "2021", value: "2021"}
                                ]}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={300}
                                labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setPeriode(value.value)}
                            />
                            <View></View>
                
                    </View>
                    <View style={{flex: 1, paddingLeft: 5}}>
                            <Text style={styles.label}>Pilih Tahap</Text>
                            <Text style={styles.labelIsi}>Tahap saat ini : {tahap}</Text>
                            <DropDownPicker
                                items={[
                                {label: 'Tahap 1', value: 'Tahap 1'},
                                {label: 'Tahap 2', value: 'Tahap 2'},
                                {label: 'Tahap 3', value: 'Tahap 3'},
                                ]}
                            
                                containerStyle={{ height: 40 }}
                                style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                                dropDownStyle={{ backgroundColor: '#fafafa' }}
                                dropDownMaxHeight={300}
                                labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                onChangeItem={(value) => setTahap(value.value)}
                            />
                            <View></View>
                
                    </View>
                    
                </View>
                <View>
                    <Text style={styles.label}>Penanggung Jawab</Text>
                    <Text style={styles.labelIsi}>Penanggung jawab saat ini : {penanggungJawab}</Text>
                        <DropDownPicker
                            items={listPenanggungJawab}
                        
                            containerStyle={{ height: 40 }}
                            style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            dropDownMaxHeight={300}
                            labelStyle={{ fontSize: 13, fontFamily: 'Poppins', color: '#394761' }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            onChangeItem={(value) => setIdPenanggungJawab(value.value)}
                        />
                        <View></View>
                </View>
                <View>
                    <Text style={styles.label}>Nama Kegiatan</Text>
                    <TextInput placeholder="Nama Kegiatan" style={styles.form} value={namaKegiatan} onChangeText={(value) => setNamaKegiatan(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Jumlah Anggran (Rp)</Text>
                    <TextInput placeholder="Jumlah Anggaran (Rp)" style={styles.form} value={jumlahAnggaran.toString()} keyboardType="numeric" onChangeText={(value) => setJumlahAnggaran(value)} />
                </View>
                
               <TouchableOpacity style={{width: '100%'}} onPress={() => updateKegiatan()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
               </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    labelIsi : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
