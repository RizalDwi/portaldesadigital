import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, Modal, TouchableHighlight, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import Textarea from 'react-native-textarea';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'


export default function index({route}) {

    const [modalVisible, setModalVisible] = useState(false);
    const [judul, setJudul] = useState('')
    const [isi, setIsi] = useState('')
    const [foto, setFoto] = useState('')
    const [loading, setLoading] = useState(false)

    const SelectImageFromGaleery = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 300,
            cropping: true
          }).then(image => {
            setModalVisible(!modalVisible);
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    const selectImageFromCamera = () => {
        setModalVisible(!modalVisible);
        ImagePicker.openCamera({
            width: 400,
            height: 300,
            cropping: true,
          }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => {
                    setFoto(base64String)
                }
                    )
                .catch(err => console.log(err) );
          });
    }

    async function addInformasi() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    judul_informasi: judul,
                    deskripsi: isi,
                    foto: foto,
                    id_desa: route.params.idDesa
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/tambah_informasi', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setJudul('')
                    setIsi('')
                    setFoto('')
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

      if(loading){
          return <LoadingScreen />
      }

    return (
        
        <View style={{flex: 1, padding: 20}}>

        <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centeredView}>
                <View style={styles.modalView}>

                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            selectImageFromCamera();
                        }}>
                            <Text style={styles.textStyle}>Ambil Dari Camera</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            SelectImageFromGaleery();
                        }}>
                            <Text style={styles.textStyle}>Ambil Dari Galeri</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        style={{ ...styles.openButton, backgroundColor: "#2196F3", width: '90%', marginVertical: 5 }}
                        onPress={() => {
                            setModalVisible(!modalVisible);
                        }}
                        >
                        <Text style={styles.textStyle}>Batal</Text>
                    </TouchableHighlight>
                </View>
                </View>
            </Modal>
            <ScrollView>
            <View>
                <Text style={styles.label}>Gambar Informasi</Text>
                <View style={{flex: 1, alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => {
                                                    setModalVisible(true);
                                                    }}>
                        <View style={{width: 190, height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center', borderRadius: 25}}>
                            <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 15, textAlign: 'center'}}>Upload Gambar</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
            <View style={{marginVertical: 10, paddingHorizontal: '10%', alignItems: 'center'}}>
                <Image source={foto === '' ? require('./../../../../assets/icons/thumnail.png') : {uri: `data:image/png;base64,${foto}`}} style={{width: 400, height: 300}} />
            </View>
            

            <View>
                <Text style={styles.label}>Judul Informasi</Text>
                <TextInput placeholder="Nama Produk" style={styles.form} onChangeText={(value) => setJudul(value)} />
            </View>
            <View>
                <Text style={styles.label}>Deskripsi</Text>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Tulis Berita'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        onChangeText={(value) => setIsi(value)}
                    />
                </View>
            </View>
            <TouchableOpacity style={{width: '100%'}} onPress={() => addInformasi()}>
                <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                    <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Tambahkan</Text>
                </View>
            </TouchableOpacity>
            </ScrollView>
            
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      centeredView: {
        flex: 1,
        marginTop: 22
      },
      modalView: {
        width: '100%',
        height: 200,
        position: 'absolute',
        bottom: 0,
        backgroundColor: "white",
        padding: 35,
        alignItems: "center",
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        textAlign: "center",
        fontFamily: 'Poppins-Bold'
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
})
