import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import Textarea from 'react-native-textarea';
import TanggalAkhir from './../../../atom/DatePickerTanggalAkhir'
import TanggalAwal from './../../../atom/DatePickerTanggalAwal'
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {
    
    const [namaProgram, setNamaProgram] = useState('')
    const [keterangan, setKeterangan] = useState('')
    const [tanggalMulai, setTanggalMulai] = useState(new Date())
    const [tanggalAkhir, setTanggalAkhir] = useState(new Date())
    const [loading, setLoading] = useState(true)

    async function editProgram() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    nama_program: namaProgram,
                    keterangan: keterangan,
                    tanggal_mulai: tanggalMulai,
                    tanggal_berakhir: tanggalAkhir,
                    id: route.params.id
                }
                setLoading(true)
                Axios.post('https://shielded-eyrie-91877.herokuapp.com/update_program', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    setLoading(false)
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    setLoading(false)
                    console.log(data)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function getData() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: route.params.id
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_program_satuan', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setNamaProgram(res.data.data[0].nama_program)
                    setKeterangan(res.data.data[0].keterangan)
                    setTanggalMulai(new Date(res.data.data[0].tanggal_mulai))
                    setTanggalAkhir(new Date(res.data.data[0].tanggal_berakhir))
                    setLoading(false)
                
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    useEffect(() => {
        
        getData()
      
      }, [])

      const showToast = () => {
        ToastAndroid.show("Data berhasil diupdate", ToastAndroid.SHORT);
      };

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                
                <View>
                    <Text style={styles.label}>Nama Program</Text>
                    <TextInput placeholder="Nama Program" style={styles.form} value={namaProgram} onChangeText={(value) => setNamaProgram(value)} />
                </View>
                <View>
                    <Text style={styles.label}>Keterangan</Text>
                    <View style={styles.container}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            placeholder={'Keterangan'}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            value={keterangan}
                            onChangeText = {(value) => setKeterangan(value)}
                        />
                    </View>
                </View>
                <TanggalAwal tanggal={tanggalMulai} setTanggal = {setTanggalMulai} />
                <TanggalAkhir tanggal={tanggalAkhir} setTanggal = {setTanggalAkhir} />
                <TouchableOpacity onPress={() => editProgram()} style={{width: '100%'}}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>       
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      formTanggal: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC',
            marginLeft: 5
        }
})
