import React, {useContext} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native'
import { RootContext } from './../../../../screens/admin/EditLuasDesa'

export default function index() {
    const state = useContext(RootContext)

    return (
        <View style={{padding: 20, flex: 1}}>
                <View>
                    <Text style={styles.label}>Nama Wilayah</Text>
                    <TextInput placeholder="Nama Wilayah" style={styles.form} value={state.namaWilayah} onChangeText={(value) =>  state.handleChangeInputNamaWilayah(value)}/>
                </View>
                <View>
                    <Text style={styles.label}>Luas Wilayah</Text>
                    <TextInput placeholder="Luas Wilayah" keyboardType="numeric" style={styles.form} value={state.luas} onChangeText={(value) =>  state.handleChangeInputLuas(value)}/>
                </View>
                
               
                <TouchableOpacity style={{width: '100%'}} onPress={() => state.updateLuasDesa()}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }
})
