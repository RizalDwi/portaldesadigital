import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Textarea from 'react-native-textarea';
import DateTimePicker from '@react-native-community/datetimepicker';
import LoadingScreen from './../../../../screens/LoadingScreen'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'

export default function index({route}) {
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [listUser, setListUser] = useState([])
    const [loading, setLoading] = useState(true)
    const [loading2, setLoading2] = useState(false)
    const [nik, setNik] = useState('')
    const [telp, setTelp] = useState('')
    const [alamat, setAlamat] = useState('')
    const [idWarga, setIdWarga] = useState('')

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
      };
    
      const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
      };
    
      const showDatepicker = () => {
        showMode('date');
      };

      const getUser = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    id_desa: route.params.idDesa,
                    id_program: route.params.id
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_warga_desa', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Form Data ->", data)
                    console.log(res)
                    setListUser(res.data.data)
                    setLoading(false)
                })
                .catch((err) => {
                    //alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const getDetailUser = async(id) => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    id: id,
                }

                setLoading2(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/detail_warga', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log("Form Data ->", data)
                    console.log(res)
                    setIdWarga(id)
                    setNik(res.data.data[0].nik)
                    setTelp(res.data.data[0].no_telp)
                    setAlamat(res.data.data[0].alamat)
                    setDate(new Date(res.data.data[0].tanggal_lahir))
                    setLoading2(false)
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading2(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const addPeserta = async() => {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){
                let data = {
                    id_program: route.params.id,
                    id_warga: idWarga
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/add_peserta', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setIdWarga('')
                    setNik('')
                    setTelp('')
                    setAlamat('')
                    setDate(new Date())
                    setListUser([])
                    getUser()
                    showToast()
                })
                .catch((err) => {
                    alert(err)
                    console.log("Form Data ->", data)
                    console.log(err)
                    setLoading(false)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const showToast = () => {
        ToastAndroid.show("Data berhasil ditambahkan", ToastAndroid.SHORT);
      };

    useEffect(() => {
        getUser()
    }, [])
    
    if(loading){
        return <LoadingScreen />
    }
    return (
        <View style={{flex: 1, padding: 20}}>
            <ScrollView>
                <View>
                    <Text style={styles.label}>No. KK / Nama</Text>
                    <DropDownPicker
                        items={listUser}
                    
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#F7F9FC', orderWidth: 1, borderColor: '#A8C4E5' }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        dropDownMaxHeight={300}
                        labelStyle={{ fontSize: 18, fontFamily: 'Poppins', color: '#394761' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        onChangeItem={(value) => getDetailUser(value.value)}
                    />
                    <View></View>
            
                </View>
                <View>
                    <Text style={styles.label}>NIK</Text>
                    {(() => {
                            if (loading2){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <TextInput placeholder="NIK" style={styles.form} value={nik} editable={false}/>
                            )
                        })()}
                </View>
                
                <View>
                    <Text style={styles.label}>No. Telp</Text>
                    
                    {(() => {
                            if (loading2){
                                return <Image source={require('./../../../../assets/gif/loading.gif')} style={{width: 40, height: 40}} />
                            }
                            return (
                                <TextInput placeholder="N0. Telp" style={styles.form} value={telp} editable={false} />
                            )
                        })()}
                </View>
                    {show && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    display="default"
                    onChange={onChange}
                    />
                )}
                <View>
                    <Text style={styles.label}>Tanggal Lahir</Text>
                    <View style={{flexDirection: 'row'}}>
                       
                            <View>
                                <Image source={require('./../../../../assets/icons/kalender.png')} style={{width: 40, height: 40, marginVertical: 5}} />
                            </View>
                        {(() => {
                            if (loading2){
                                return <LoadingScreen />
                            }
                            return (
                                <TextInput placeholder="Judul Pesan" style={styles.formTanggal} value={date.toDateString()} editable={false} />
                            )
                        })()}
                    </View>
                </View>
                <View>
                    <Text style={styles.label}>Alamat</Text>
                   
                    {(() => {
                            if (loading2){
                                return <LoadingScreen />
                            }
                            return (
                                <TextInput placeholder="Alamat" style={styles.form} value={alamat} editable={false} />
                            )
                        })()}
                </View>
                
               
               <TouchableOpacity onPress={() => addPeserta()} style={{width: '100%'}}>
                    <View style={{width: '100%', height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                        <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 20, textAlign: 'center'}}>Simpan</Text>
                    </View>
                </TouchableOpacity> 
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    form: { width: '100%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC'
        },
    label : {
        fontFamily: 'Poppins-Bold',
        color: '#394761',
        fontSize: 18
    },
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 200,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
      formTanggal: { width: '50%',
            height: 40,
            borderWidth: 1,
            borderColor: '#A8C4E5',
            paddingHorizontal: 20,
            marginVertical: 5,
            backgroundColor: '#F7F9FC',
            marginLeft: 5
        }
})
