import React from 'react'
import { StyleSheet, Text, View, Image, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Textarea from 'react-native-textarea';

const index = (props) => {

    return (
        <View style={{marginTop: 10}}>
            <View style={{borderColor: '#DFDFDF', borderBottomWidth: 1, marginVertical: 5}}>
            {props.dataBalasan.map((item, index) => (
                    <View key={index}>
                        <View style={{flexDirection: 'row', marginBottom: 10}}>
                        <Image source={require('./../../../assets/images/widang.jpg')} style={{height: 50, width: 50, borderRadius: 25}} />
                        <View style={{marginLeft: 10, justifyContent:'center', flex: 1}}>
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.textName}>{props.namaDesa}</Text>
                                    <View style={{flex:1, justifyContent: 'center'}}>
                                        <Text style={{textAlign: 'right',fontSize: 11, fontFamily: 'Poppins', color: '#989898'}}>2 jam lalu</Text>
                                    </View>
                                </View>
                                <View style={{flex:1}}>
                                    <Text style={styles.textChat}>{item.isi_balasan}</Text>
                                </View>
                        </View>
                    </View>
                    </View>
                ))}
            
            </View>
            
            <View>
                <Text style={styles.label}>Balas</Text>
                <View style={styles.container}>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        placeholder={'Isi Pesan'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                        onChangeText={(value) => props.handleBalasan(value)}
                    />
                </View>
            </View>
            <TouchableOpacity onPress={() => props.sendMail()}>
                <View style={{height: 40, backgroundColor: '#2286FF', marginTop: 10, justifyContent: 'center'}}>
                    <Text style={{fontFamily: 'Poppins-Bold', color: '#FFFFFF', fontSize: 18, textAlign: 'center'}}>Kirim Pesan</Text>
                </View>
            </TouchableOpacity>
            
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    textName: {fontSize: 16, fontFamily: 'Poppins', color: '#1B1B1B'},
    textChat: {fontSize: 11, fontFamily: 'Poppins', color: '#989898'},
    container: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      },
      textareaContainer: {
        height: 150,
        width: '100%',
        padding: 5,
        backgroundColor: '#F7F9FC',
        borderWidth: 1,
        borderColor: '#A8C4E5'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 130,
        fontSize: 14,
        color: '#333',
      },
})