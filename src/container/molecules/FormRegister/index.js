import React, { useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'
import LoadingScreen from './../../../screens/LoadingScreen'

export default function index(props) {

    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [loading, setLoading] = useState(false)

    const onRegisterPress = () => {
        let data = {
            name : username,
            email : email,
            password: password
        }
        setLoading(true)
        Axios.post('https://shielded-eyrie-91877.herokuapp.com/register', data, {
            timeout: 20000
        })
        .then((res) => {
            setLoading(false)
            showToast()

            props.navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }]
            })
        })
        .catch((err) => {
            alert(err)
        })
    }

    const showToast = () => {
        ToastAndroid.show("Register berhasil, silahkan login", ToastAndroid.SHORT);
      };

      if(loading){
          return <LoadingScreen />
      }

    return (
        <View style={{width: '100%'}}>
            <TextInput value={username} onChangeText={(username) => setUsername(username)} placeholder="Username" style={{width: '100%', height: 60, borderWidth: 1, borderColor: '#E2E6EA', paddingHorizontal: 20, marginVertical: 5}} />
            <TextInput value={email} onChangeText={(email) => setEmail(email)} placeholder="Email" style={{width: '100%', height: 60, borderWidth: 1, borderColor: '#E2E6EA', paddingHorizontal: 20, marginVertical: 5}} />
            <TextInput value={password} onChangeText={(password) => setPassword(password)} placeholder="Password" style={{width: '100%', height: 60, borderWidth: 1, borderColor: '#E2E6EA', paddingHorizontal: 20, marginVertical: 5}} />

            <TouchableOpacity onPress={() => onRegisterPress()} style={{marginVertical: 10}}>
                <View style={{width: '100%', height: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3C82FF'}}>
                    <Text style={{fontFamily: 'SFProDisplay', color: '#FFFFFF', fontSize: 19}}>Register</Text>
                </View>
            </TouchableOpacity>

            <View style={{marginVertical: 5, flexDirection: 'row', justifyContent:'center'}}>
                <Text style={{fontSize: 16, fontFamily: 'SFProDisplay', color: '#8B959A'}}>Sudah punya akun?</Text>
                <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
                    <Text style={{fontSize: 16, fontFamily: 'SFProDisplay', color: '#3C82FF'}}> Login sekarang</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
