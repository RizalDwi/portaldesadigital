import React, { useEffect } from 'react'
import { View, Text, ScrollView, Image, FlatList, TouchableOpacity } from 'react-native'

export default function index(props) {

    return (
        <View style={{width: '100%', height: 480, padding: 10}}>
            <Text style={{fontSize: 24, fontFamily: 'Rubik-Bold', color: '#696969'}}>
                Pengumuman
            </Text>
            <View style={{marginTop: 10}}>
                <ScrollView horizontal={true}>
                {props.dataPengumuman.map((item, index) => (
                    <View key={index}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('ArtikelPengumuman', {id:item.id})}>
                            <View style={{width:350, height: 400, backgroundColor: '#FFFFFF',padding: 10, marginHorizontal: 5}} elevation={3}>
                                    <View>
                                        <Image source={{uri: `data:image/png;base64,${item.foto}`}} style={{width: '100%', height: '90%'}} />
                                    </View>
                                    <View style={{marginVertical: 10}}>
                                        <Text style={{fontSize: 17, fontFamily: 'SFProDisplay'}}>
                                            {item.judul}
                                        </Text>
                                    </View>
                                </View>
                        </TouchableOpacity>
                    </View>
                ))}
                    
                </ScrollView>
            </View>
            
        </View>
    )
}
