import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, Image } from 'react-native'

export default function index(props) {


    const renderItem = ({ item, index }) => {
        return(
            <View style={{flexDirection: 'row'}}>
                        <View style={styles.rowNomor}>
                            <Text style={styles.labelRow}>{index + 1}</Text>
                        </View>
                        <View style={styles.row}>
                        {(() => {
                            if (item.rules != "Superadmin" && props.rules == "Superadmin"){
                                return (
                                    <View>
                                        <TouchableOpacity onPress={() => props.hapusAdmin(item.id_users)}>
                                            <Image source={require('./../../../../assets/icons/cancel.png')} style={{width: 30, height: 30}} />
                                        </TouchableOpacity>
                                    </View>
                                )
                            }
                            
                        })()}
                            
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelRow}>{item.nama_lengkap}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelRow}>{item.rules}</Text>
                        </View>
                        
                    </View>
        )
    }

    return (
        <View style={{flex: 1, marginTop: 10}}>
                <View style={{width: '100%'}}>
                    <View style={{flexDirection: 'row', backgroundColor: `#${props.warna}`}}>
                        <View style={styles.rowNomor}>
                            <Text style={styles.labelHeader}>No</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Aksi</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Nama</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Rules</Text>
                        </View>
                      
                    </View>
                    <FlatList data={props.dataWarga} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
                    
                </View>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#FFFFFF',
        fontSize: 10,
        textAlign: 'center'
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 10,
        textAlign: 'center'
    },
    row: {
        borderColor: '#E5E5E5', borderWidth: 2, width:90 , alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowNomor: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 50, alignItems: 'center', justifyContent: 'center', padding: 10
    }
})

