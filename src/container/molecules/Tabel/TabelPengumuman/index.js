import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image, TouchableOpacity, Alert, ToastAndroid, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../../screens/LoadingScreen'

export default function index({navigation, route}) {

    const [dataPengumuman, setDataPengumuman] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);

    async function getDataPengumuman() {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_pengumuman', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataPengumuman(res.data.data)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function hapusPengumuman(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_pengumuman', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataPengumuman()
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const alertDelete = (id) =>
        Alert.alert(
        "Hapus Pengumuman",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => hapusPengumuman(id) }
        ],
        { cancelable: false }
        );
    
        const showToast = () => {
            ToastAndroid.show("Data berhasil dihapus", ToastAndroid.SHORT);
          };

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            getDataPengumuman()
        });
        const interval = setInterval(() => {
            setCount((count) => count + 1);
          }, 1000);
        const unsubscribe = navigation.addListener('focus', () => {
            setCount(0);
            InteractionManager.runAfterInteractions(() => {
                getDataPengumuman()
            });
          });
      
          return () => {
            // Clear setInterval in case of screen unmount
            clearTimeout(interval);
            // Unsubscribe for the focus Listener
            unsubscribe;
          };
    
        
      }, [navigation])

    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
            <View style={{flex:1}}>
                <View style={styles.row}>
                    <View style={styles.coloumnLeft}>
                        <Text style={styles.labelHeader}>Judul</Text>
                    </View>
                    <View style={styles.coloumnRight}>
                        <Text style={styles.labelRow}>{item.judul}</Text>
                    </View>
                </View>
               
            </View>
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                    <TouchableOpacity onPress={() => navigation.navigate('FormEditPengumuman', {id: item.id}) }>
                        <View style={{backgroundColor: '#F19000', width: 50, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                            <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Edit</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => alertDelete(item.id)}>
                        <View style={{backgroundColor: '#E03535', width: 60, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                            <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Hapus</Text>
                        </View>
                    </TouchableOpacity>
                    
                </View>
        </View>
        )
    }
    if (loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 10}}>
            <ScrollView>
                    <FlatList data={dataPengumuman} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10}
})

