import React, {useState, useContext, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image, TouchableOpacity } from 'react-native'
import { RootContext } from './../../../../screens/admin/ProfilDesa'

export default function index(props) {
    const state = useContext(RootContext)


    useEffect(() => {
        props.setLuas(luas)  
        }, [luas])

    var luas = 0

    const renderItem = ({ item, index }) => {
        luas = luas + parseInt(item.luas)
        return(
            <View style={styles.cardContainer}>
                    <View style={{flex:1}}>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nama Daerah</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.nama_tempat}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Luas Daerah</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.luas}</Text>
                            </View>
                        </View>
                       
                    </View>
                        <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('EditLuasDesa', {itemId: item.id}) }>
                                <View style={{backgroundColor: '#F19000', width: 50, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Edit</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => alertDelete(item.id)}>
                                <View style={{backgroundColor: '#E03535', width: 60, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Hapus</Text>
                                </View>
                            </TouchableOpacity>
                            
                        </View>
                </View>
        )
    }

    return (
        <View style={{flex: 1, marginTop: 10}}>
            <ScrollView>
                    <FlatList data={state.luasDesa} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10}

})

