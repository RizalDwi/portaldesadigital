import React, {useState, useContext} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image, TouchableOpacity } from 'react-native'
import { RootContext } from './../../../../screens/admin/ProfilDesa'

export default function index(props) {
    const state = useContext(RootContext)    

    const renderItem = ({ item, index }) => {
        return(
            <View style={{flexDirection: 'row'}}>
                        <View style={styles.rowNomor}>
                            <Text style={styles.labelRow}>{index + 1}</Text>
                        </View>
                        <View style={styles.rowAksi}>
                            <View style={styles.button}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('EditTopografi', {itemId: item.id}) }>
                                    <Image source={require('./../../../../assets/icons/pencil_edit.png')} style={{width: 40, height: 40}} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.button}>
                                <TouchableOpacity onPress={() => state.alertDeleteKategoriTopografi(item.id)}>
                                    <Image source={require('./../../../../assets/icons/cancel.png')} style={{width: 40, height: 40}} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.button}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('KelompokTopografi', {itemId: item.id}) }>
                                    <Image source={require('./../../../../assets/icons/more_detail.png')} style={{width: 40, height: 40}} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelRow}>{item.nama_kategori}</Text>
                        </View>
                    </View>
        )
    }

    return (
        <View style={{flex: 1, marginTop: 10}}>
            <ScrollView horizontal={true}>
                <View style={{width: '100%'}}>
                    <View style={{flexDirection: 'row', backgroundColor: `#${props.warna}`}}>
                        <View style={styles.rowNomor}>
                            <Text style={styles.labelHeader}>No</Text>
                        </View>
                        <View style={styles.rowAksi}>
                            <Text style={styles.labelHeader}>Aksi</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.labelHeader}>Nama Kategori</Text>
                        </View>
                       
                      
                    </View>
                    <FlatList data={state.kategoriTopografi} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
                    
                </View>
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center'
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        textAlign: 'center'
    },
    row: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 200, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowNomor: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 50, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    
    rowStatus: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 70, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    rowAksi: {
        borderColor: '#E5E5E5', borderWidth: 2, width: 210, alignItems: 'center', justifyContent: 'center', padding: 5, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5}
})

