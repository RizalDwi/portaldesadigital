import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, ToastAndroid, Alert, Image, InteractionManager } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import LoadingScreen from './../../../../screens/LoadingScreen'

export default function index({navigation, route}) {

    const [dataIventaris, setDataIventaris] = useState([])
    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0);
    const [warna, setWarna] = useState('')

    async function getDataIventaris() {
        setWarna(await Asynstorage.getItem('warna_banner_admin'))
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id_desa: route.params.idDesa,
                }

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/get_all_iventaris_tanah', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    setDataIventaris(res.data.data)
                    setLoading(false)
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    async function hapusDataIventaris(id) {
        var token = await Asynstorage.getItem('api_token')
        try {
            await Asynstorage.getItem('api_token', (error, result) =>{

              if (result){

                let data = {
                    id: id,
                }

                setLoading(true)

                Axios.post('https://shielded-eyrie-91877.herokuapp.com/hapus_iventaris_tanah', data, {
                    timeout: 20000,
                    headers: {
                        'Authorization' : 'Bearer ' + token
                    }
                })
                .then((res) => {
                    console.log(res)
                    getDataIventaris()
                    setLoading(false)
                    showToast()
                    
                })
                .catch((err) => {
                    alert(err)
                })
              }
            })
  
        } catch(err) {
          console.log("Logout -> error : ",err)
        }
    }

    const alertDelete = (id) =>
        Alert.alert(
        "Hapus Iventaris Tanah",
        "Anda yakin ingin menghapus data ini?",
        [
            {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => hapusDataIventaris(id) }
        ],
        { cancelable: false }
        );
    
        const showToast = () => {
            ToastAndroid.show("Data berhasil dihapus", ToastAndroid.SHORT);
          };

          useEffect(() => {
            InteractionManager.runAfterInteractions(() => {
                getDataIventaris()
              });
            
          }, [])

    const renderItem = ({ item, index }) => {
        return(
            <View style={styles.cardContainer}>
                    <View style={{flex:1}}>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nama Barang</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.nama_barang}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Kode</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.kode_barang}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nomor Registrasi</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.nomor_register}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Luas</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.luas_tanah} M2</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tahun Pengadaan</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.tahun_pengadaan}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Lokasi</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.letak_atau_alamat}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Hak Tanah</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.hak_tanah}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Tanggal Sertifikat</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.tanggal_sertifikat}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Nomor Sertifikat</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.nomor_sertifikat}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Penggunaan</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.penggunaan}</Text>
                            </View>
                        </View>
                        
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Sumber Dana</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.asal_usul_barang}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Harga</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.harga}</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.coloumnLeft}>
                                <Text style={styles.labelHeader}>Keterangan</Text>
                            </View>
                            <View style={styles.coloumnRight}>
                                <Text style={styles.labelRow}>{item.keterangan}</Text>
                            </View>
                        </View>
                    </View>
                        <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', height: 50}}>
                            <TouchableOpacity onPress={() => navigation.navigate('FormEditIventarisTanah', {id: item.id}) }>
                                <View style={{backgroundColor: '#F19000', width: 50, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Edit</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => alertDelete(item.id)}>
                                <View style={{backgroundColor: '#E03535', width: 60, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 5}}>
                                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF'}}>Hapus</Text>
                                </View>
                            </TouchableOpacity>
                            
                        </View>
                </View>
          
        )
    }

    if(loading){
        return <LoadingScreen />
    }

    return (
        <View style={{flex: 1, padding: 20}}>
            <TouchableOpacity onPress={() => navigation.navigate('FormIventarisTanah', {idDesa: route.params.idDesa})}>
                <View style={{marginVertical: 10, backgroundColor: `#${warna}`, height: 30, width: 200, alignItems: 'center', justifyContent: 'center', borderRadius: 15}}>
                    <Text style={{fontFamily: 'Rubik-Bold', color: '#FFFFFF', fontSize: 18}}>Tambah Iventaris</Text>
                </View>
            </TouchableOpacity>
            <ScrollView>
                    <FlatList data={dataIventaris} renderItem={renderItem} keyExtractor = { (item, index) => index.toString() } />
            </ScrollView>
        </View>
       
    )
}


const styles = StyleSheet.create({
    labelHeader : {
        fontFamily: 'Poppins-Bold',
        color: '#000000',
        fontSize: 14,
        marginLeft: 10
    },
    labelRow : {
        fontFamily: 'Poppins',
        color: '#394761',
        fontSize: 14,
        marginLeft: 10
    },
    row: {
        flex:1, flexDirection: 'row'
    },
    button: {paddingHorizontal: 5},
    coloumnLeft: {flex: 1},
    coloumnRight: {flex: 1},
    cardContainer: {padding: 10, flex: 1, backgroundColor: '#B7DBFF', borderRadius: 10, marginTop: 10}
})

