import React, { useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, Alert } from 'react-native'
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'
import LoadingScreen from './../../../screens/LoadingScreen'
import NotifService from './../../../config/Notification/NotifService';

export default function index(props) {
    const [registerToken, setRegisterToken] = useState('');
    const [fcmRegistered, setFcmRegistered] = useState(false);

    const onRegister = (token) => {
        setRegisterToken(token.token);
        setFcmRegistered(true);
    };

    const onNotif = (notif) => {
        Alert.alert(notif.title, notif.message);
    };

    const notif = new NotifService(onRegister, onNotif);
    const handlePerm = (perms) => {
        Alert.alert('Permissions', JSON.stringify(perms));
    };

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [loading, setLoading] = useState(false)

    const onLoginPress = () => {
        let data = {
            email : email,
            password: password,
            token_device: registerToken
        }
        setLoading(true)
        Axios.post('https://shielded-eyrie-91877.herokuapp.com/login', data, {
            timeout: 20000
        })
        .then((res) => {
            if(res.data.data.user.foto){
                Asynstorage.setItem('foto_user', res.data.data.user.foto)
            } else {
                Asynstorage.setItem('foto_user', '')
            }
            console.log("user", res.data.data.user)
            Asynstorage.setItem('api_token', res.data.data.api_token)
            Asynstorage.setItem('id_user', JSON.stringify(res.data.data.user.id))
            Asynstorage.setItem('username', res.data.data.user.name)
            Asynstorage.setItem('email', res.data.data.user.email)
            if (res.data.data.data_admin.length > 0){
                Asynstorage.setItem('id_desa_admin', JSON.stringify(res.data.data.data_admin[0].id_desa))
                Asynstorage.setItem('rules', res.data.data.data_admin[0].rules)
                Asynstorage.setItem('warna_banner_admin', res.data.data.data_admin[0].warna_banner)
                Asynstorage.setItem('nama_desa_admin', res.data.data.data_admin[0].nama_desa)
                if(res.data.data.data_admin[0].foto){
                    Asynstorage.setItem('foto_desa_admin', res.data.data.data_admin[0].foto)
                } else {
                    Asynstorage.setItem('foto_desa_admin', '')
                }
            }
            Asynstorage.setItem('admin', res.data.admin)

            props.navigation.reset({
                index: 0,
                routes: [{ name: 'main' }]
            })
        })
        .catch((err) => {
            setLoading(false)
            console.log(err)
        })
    }

    if(loading){
        return <LoadingScreen />
    } else {
        return (
            <View style={{width: '100%'}}>
                <TextInput value={email} onChangeText={(email) => setEmail(email)} placeholder="Email" style={{width: '100%', height: 60, borderWidth: 1, borderColor: '#E2E6EA', paddingHorizontal: 20, marginVertical: 5}} />
                <TextInput value={password} onChangeText={(password) => setPassword(password)} placeholder="Password" style={{width: '100%', height: 60, borderWidth: 1, borderColor: '#E2E6EA', paddingHorizontal: 20, marginVertical: 5}} />
    
                <TouchableOpacity onPress={() => onLoginPress()} style={{marginVertical: 10}}>
                    <View style={{width: '100%', height: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3C82FF'}}>
                        <Text style={{fontFamily: 'SFProDisplay', color: '#FFFFFF', fontSize: 19}}>Login</Text>
                    </View>
                </TouchableOpacity>
    
                <View style={{marginVertical: 5, flexDirection: 'row', justifyContent:'center'}}>
                    <Text style={{fontSize: 16, fontFamily: 'SFProDisplay', color: '#8B959A'}}>Belum punya akun?</Text>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Register')}>
                        <Text style={{fontSize: 16, fontFamily: 'SFProDisplay', color: '#3C82FF'}}> Daftar di sini sekarang</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
