import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import Axios from 'axios'
import RNFetchBlob from 'rn-fetch-blob'

export default function index() {
    function Downnload() {
        const { config, fs } = RNFetchBlob;
        const downloads = fs.dirs.DownloadDir;
        return config({
            // add this option that makes response data to be stored as a file,
            // this is much more performant.
            fileCache : true,
            addAndroidDownloads : {
            useDownloadManager : true,
            notification : false,
            path:  downloads + '/fileNew.pdf',
            }
        })
        .fetch('GET', 'https://shielded-eyrie-91877.herokuapp.com/cetak');
        
    }

    return (
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
           <TouchableOpacity onPress={() => Downnload()}>
               <View style={{backgroundColor: 'pink', width: 100, height: 30}}></View>
           </TouchableOpacity>
        </View>
    )
}
