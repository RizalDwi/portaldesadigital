import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Konten from './src/screens/LoadingScreen'
import HomeDesa from './src/screens/desa/HomeDesa'
import Login from './src/screens/Login'
import Register from './src/screens/Register'
import Routes from './src/config/Routes'
import Input from './src/screens/Input'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Chart from './chart.js'
import Download from './download.js'

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 200,
    width: 200,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
 });

export default function App() {
  return (
    <View style={{flex: 1}}>
     <Routes />
   </View>
  )
}
